from django.contrib import admin
from src.payment.models import Payment, PaymentMethod


class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        "reference_no",
        "phone_number",
        "transaction_id",
        "status",
        "id",
        "order_id",
        "provider",
        "company_id",
    )


class PaymentMethodAdmin(admin.ModelAdmin):
    list_display = ("name", "label")


admin.site.register(Payment, PaymentAdmin)
admin.site.register(PaymentMethod, PaymentMethodAdmin)
