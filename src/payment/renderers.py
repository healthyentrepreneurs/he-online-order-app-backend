from io import StringIO

from django.utils.xmlutils import SimplerXMLGenerator
from rest_framework_xml.parsers import XMLParser
from rest_framework_xml.renderers import XMLRenderer


class CustomXMLParser(XMLParser):
    def parse(self, stream, media_type=None, parser_context=None):
        if isinstance(stream, (str)):
            stream = StringIO(stream)
        return super().parse(stream, media_type, parser_context)


class CustomXMLRenderer(XMLRenderer):
    root_tag_name = "COMMAND"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        if data is None:
            return ""

        stream = StringIO()

        xml = SimplerXMLGenerator(stream, self.charset)
        xml.startElement(self.root_tag_name, {})
        self._to_xml(xml, data)
        xml.endElement(self.root_tag_name)
        xml.endDocument()
        return stream.getvalue()
