import random
import logging
import string
from datetime import timedelta

from celery import shared_task

from django.conf import settings
from django.utils import timezone
from django_q.models import Schedule, Task
from django_q.tasks import schedule
from src.djangodoo.utils import switch_country
from src.utils.tasks import get_transaction_status


logger = logging.getLogger(__name__)


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return "".join(random.choice(chars) for _ in range(size))


def next_reference_no(instance):
    new_reference_no = random_string_generator()

    Klass = instance.__class__

    if Klass.objects.filter(reference_no=new_reference_no).exists():
        return next_reference_no(instance)
    return new_reference_no.capitalize()


def check_transaction_status_of_pending_payments():
    from src.payment.aggregator.momo_api import MomoApi
    from src.payment.models import Payment

    client = MomoApi()
    momo_requests = Payment.objects.filter(status="received", provider="mtn")
    for momo_request in momo_requests:
        response = client.get_transaction_status(momo_request.reference_no)
        if "status" in response:
            req = Payment.objects.filter(id=momo_request.id)
            req.update(
                status=response["status"],
                transaction_id=response["financialTransactionId"],
                data=response,
            )


def create_payment_task(ref_no, seconds=20, repeats=8, **kwargs):
    cron = "* * * * * */{seconds}".format(seconds=int(seconds))
    logger.info(
        f"\n\n\n\n\n{ref_no}--inside--------create_payment_task-------kwargs---\n\n\n{kwargs}\n\n\n\n"
    )

    return schedule(
        "src.utils.tasks.get_transaction_status",
        ref_no,
        name=ref_no,
        schedule_type=Schedule.CRON,
        cron=cron,
        repeats=repeats,
        **kwargs,
    )


def check_payment_created_in_odoo(instance, client):
    payment = None
    if instance:
        arguments = {
            "limit": None,
            "offset": 0,
            "order": None,
            "fields": ["ref"],
        }
        if instance.transaction_id:
            domain = [["ref", "=", instance.transaction_id]]
            payment = client.search_read("account.payment", tuple(domain), **arguments)
    return payment if payment else None


@shared_task(bind=True, name="check_payment_status")
def check_payment_status(
    self, payment_method, reference_no, merchant_reference=None, code=None, count=0
):
    from src.payment.models import Payment

    instance = Payment.objects.filter(reference_no=reference_no).first()
    client = None

    if instance:
        client = switch_country(instance.company_id.odoo_id)

    payment_in_odoo = check_payment_created_in_odoo(instance, client)
    if instance and instance.processed is False and payment_in_odoo is None:
        kwargs = dict(merchant_reference=merchant_reference, code=code)
        print(
            f"\n\n\n\n---after 30 seconds checking payment status here------>>>{[payment_method, reference_no, kwargs]}"
        )

        print(f"\\n\n\n\n\n before request ------{reference_no} ----{payment_method}")
        trans = get_transaction_status(reference_no, **kwargs)

        count = int(count)

        count += 1
        kwargs.update(count=count)
        print(f"\n\n\n\n\n after request --------{payment_method}")
        if (
            isinstance(trans, Payment)
            and trans.status.lower() in ["pending", "received"]
            and count <= 3
        ):
            check_payment_status.apply_async(
                (payment_method, reference_no),
                dict(kwargs),
                countdown=30,
            )
    else:
        print(
            f"\n\n\n\n\n Payment already processed: reference_no: {reference_no} amount: {instance.amount} transaction_id: {instance.transaction_id} phone_number: {instance.phone_number}"
        )


def reconcile_received_payments():
    from src.payment.models import Payment

    payments = Payment.objects.filter(status="received").all()

    for payment in payments:
        print(
            "\n\n\n\n\n-----prcoessing payment ----",
            payment.reference_no,
            payment.status,
            payment.amount,
        )
        trans = get_transaction_status(payment.reference_no)
        if isinstance(trans, Payment):
            print(
                "\n\n\n\n\n----after -prcoessing payment ----",
                trans.reference_no,
                trans.status,
                trans.amount,
            )
