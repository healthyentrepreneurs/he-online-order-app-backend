from uuid import uuid4
import logging

from django.conf import settings
from rest_framework import status
from django.contrib.auth import get_user_model
from django.db.utils import IntegrityError
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound, ParseError, ValidationError
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework.settings import api_settings
from ..core.pagination import PostLimitOffsetPagination
from src.djangodoo.authentication import OdooAuthentication, OdooSessionAuthentication
from src.payment.models import Payment, PaymentMethod
from .aggregator import AirtelOpenApi, MpesaApi
from .renderers import CustomXMLParser, CustomXMLRenderer
from .serializers import (
    AirtelCallbackSerializer,
    MomoCallbackSerializer,
    MpesaCallbackSerializer,
    PaymentMethodSerializer,
    PaymentSerializer,
    TransactionSerializer,
    PaymentsOverviewSerializer,
)
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from src.utils.helpers import Helpers
from src.orders.models import Orders
from django.utils.translation import gettext as _

User = get_user_model()


logger = logging.getLogger(__name__)


class PaymentViewset(viewsets.GenericViewSet, mixins.CreateModelMixin):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    authentication_classes = [OdooAuthentication, OdooSessionAuthentication]

    def get_odoo_client(self):
        return (
            self.request.user.odoo_client
            or settings.odoo[self.request.user.company_id.odoo_id]
        )

    def create(self, request, *args, **kwargs):
        data = request.data
        order_id = data.get("order_id", None)
        invoice_id = data.get("invoice_id", None)
        ordering_for = None
        if request.user.contact_type == "employee":
            if request.user.ordering_for:
                ordering_for = User.objects.filter(
                    odoo_id=request.user.ordering_for
                ).first()
        if not any([order_id, invoice_id]):
            return Response(
                {"error": _("order_id or invoice_id reqired in payload")},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if order_id and request.user.contact_type != "employee":
            route_id = Orders.get_order_route(order_id, client=self.get_odoo_client())
            print(f"\n\n\n\n\n--checking order {order_id} route:---> {route_id}")
            if isinstance(route_id, dict) and "error" in route_id:
                return Response(
                    route_id,
                    status=status.HTTP_400_BAD_REQUEST,
                )
            if route_id is False:
                return Response(
                    {
                        "error": _(
                            "No delivery planned for your cluster yet. Call HE Customer Service for assistance."
                        )
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )

        return super().create(request, *args, **kwargs)

    @action(
        methods=["get"],
        detail=True,
        serializer_class=TransactionSerializer,
        authentication_classes=api_settings.DEFAULT_AUTHENTICATION_CLASSES,
    )
    def transaction_status(self, request, pk=None):
        transaction = self.get_object()
        serializer = self.get_serializer_class()(transaction)
        return Response(serializer.data)

    @action(
        methods=["post"],
        detail=False,
        permission_classes=[AllowAny],
        serializer_class=AirtelCallbackSerializer,
    )
    def airtel_callback(self, request):
        data = request.data
        print("-------------------airtel call back", data)
        serializer = self.get_serializer_class()(data=data)
        serializer.is_valid(raise_exception=True)
        serialized_data = serializer.validated_data

        # find order id of saved transaction from ref
        ref_id = serialized_data.get("transaction").get("id")
        status_ = serialized_data.get("transaction").get("status")
        trans = self.get_transaction(ref_id, **serialized_data)
        print("-----after get_transaction--------------airtel call back got here", data)
        try:
            serializer.save(db_record=trans)
        except IntegrityError:
            print("-----airtel payment IntegrityError reponse", trans)
            raise ValidationError(_("Transaction Id already used"))
            # raise Response({"data": serializer.data, "errors": f"{e"}, status.HTTP_400_OK)
        except Exception as e:
            print(Helpers.stacktrace(e, "-----airtel payment error reponse"))

            return Response(
                {"data": serializer.data, "errors": f"{e}"}, status.HTTP_400_OK
            )
        response = None
        try:
            response = Response(serializer.data, status.HTTP_200_OK)
            print("-----airtel payment success reponse", data, response)
        except Exception as e:
            print(Helpers.stacktrace(e, "-----airtel payment error 222 reponse"))

        return Response(serializer.data, status.HTTP_200_OK)

    @action(
        methods=["post"],
        detail=False,
        permission_classes=[AllowAny],
        serializer_class=MpesaCallbackSerializer,
    )
    def mpesa_callback(self, request):
        data = request.data
        serializer = self.get_serializer_class()(data=data)
        serializer.is_valid(raise_exception=True)
        serialized_data = serializer.validated_data
        body = serialized_data["Body"]["stkCallback"]
        # find order id of saved transaction from ref
        trans = self.get_transaction(body.get("CheckoutRequestID"), **serialized_data)
        success_callback = body.get("CallbackMetadata")
        result_code = body.get("ResultCode")
        trans_id = None
        if result_code != 0:
            order_status = "failed"
        elif success_callback and result_code == 0:
            order_status = "successful"
            trans_id = MpesaApi.get_transaction_id(success_callback, trans_id)
        else:
            order_status = "received"
        try:
            serializer.save(trans_id=trans_id, transaction=trans, status=order_status)
        except IntegrityError:
            raise NotFound("Transaction Id already used")
        # send notification
        message = serialized_data.get("ResultDesc")
        return Response(serializer.data, status.HTTP_200_OK)

    def get_transaction(self, reference_no, **kwargs):
        trans = Payment.objects.filter(reference_no=reference_no).first()
        if not trans:
            obj = {"reference_no": reference_no, "data": kwargs}
            payment = Payment.objects.create(**obj)
            payment.save()
            raise NotFound("Transaction not found")
        return trans

    @action(
        methods=["post"],
        detail=False,
        permission_classes=[AllowAny],
        serializer_class=MomoCallbackSerializer,
    )
    def momo_callback(self, request):
        data = request.data
        logger.info(
            f"\n\n\n\n\n----momo_callback-------++++++++-------\n\n\n{data}\n\n\n\n"
        )
        # {'externalId': '3c80cd55-6f9d-4a6e-898c-c5408c1b9a82', 'amount': '500.0', 'currency': 'UGX', 'payer': {'partyIdType': 'MSISDN', 'partyId': '256760395951'}, 'payeeNote': 'Testing', 'status': 'FAILED', 'reason': 'INTERNAL_PROCESSING_ERROR'}

        serializer = self.get_serializer_class()(data=data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        print("\n\n\n--momo-----callback_validated data------>>>", validated_data)
        logger.info(
            f"\n\n\n\n\n----momo-----callback_validated data-------++++++++-------\n\n\n{validated_data}\n\n\n\n"
        )

        # find order id of saved transaction from ref
        trans = self.get_transaction(validated_data.get("externalId"), **validated_data)
        print("\n\n\n--momo-----callback_befroe save transaction------>>>", trans)
        logger.info(
            f"\n\n\n\n\n----momo------momo-----callback_befroe save transaction- data-------++++++++-------\n\n\n{trans}  ---{vars(trans)}\n\n\n\n"
        )

        try:
            serializer.save(transaction=trans)
        except IntegrityError:
            raise NotFound("Transaction Id already used")
        # send notification
        logger.info(
            f"\n\n\n\n\n----success------momo-----after save transaction- data-------++++++++-------\n\n\n{validated_data}  ---\n\n\n\n"
        )

        message = validated_data.get("status")
        return Response(serializer.data, status.HTTP_200_OK)


class PaymentMethodViewSet(viewsets.ModelViewSet):
    http_method_names = ["get"]
    permission_classes = [IsAdminUser]
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentMethodSerializer
    pagination_class = None


class PaymentsOverviewViewSet(viewsets.ModelViewSet):
    http_method_names = ["get"]
    permission_classes = [IsAdminUser]
    queryset = Payment.objects.all()
    serializer_class = PaymentsOverviewSerializer
    pagination_class = PostLimitOffsetPagination
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_fields = {
        "created_at": ["gte", "lte", "exact", "gt", "lt"],
        "updated_at": ["gte", "lte", "exact", "gt", "lt"],
        "provider": ["exact"],
        "invoice_id": ["exact"],
        "order_id": ["exact"],
        "status": ["exact"],
        "transaction_id": ["exact"],
        "phone_number": ["exact"],
    }
    search_fields = [
        "order_id",
        "invoice_id",
        "order_name",
        "status",
        "provider__name",
        "transaction_id",
        "phone_number",
    ]
