import logging
import math
from uuid import uuid4

from django.utils.translation import gettext as _
from phonenumber_field.serializerfields import PhoneNumberField
from phonenumbers import carrier as cr
from rest_framework import serializers
from src.orders.models import Orders
from src.accounting.models import Invoices
from src.payment.aggregator import AirtelOpenApi, MomoApi, MpesaApi, IhelaClient
from src.payment.models import Payment, PaymentMethod, RECEIVED
from src.payment.utils import create_payment_task, check_payment_status
from src.utils.helpers import Helpers


from django.contrib.auth import get_user_model

logger = logging.getLogger(__name__)

User = get_user_model()

prefixes_to_allow = ["+256765", "+256766", "+256745"]

from phonenumbers import PhoneNumber


class CustomPhoneNumber(PhoneNumber):
    def __init__(self, data):
        self.country_code = data.get("country_code")
        self.national_number = data.get("national_number")
        self.raw_input = data.get("raw_input")
        self.italian_leading_zero = data.get("italian_leading_zero")
        self.number_of_leading_zeros = data.get("number_of_leading_zeros")
        self.preferred_domestic_carrier_code = data.get(
            "preferred_domestic_carrier_code"
        )
        self.extension = data.get("extension")
        self.country_code_source = None


class CustomPhoneNumberField(PhoneNumberField):
    def to_internal_value(self, data):
        if [prefix for prefix in prefixes_to_allow if data.startswith(prefix)]:
            return CustomPhoneNumber(
                {
                    "country_code": int(data[1:4]),
                    "national_number": int(data[4:]),
                    "extension": None,
                    "italian_leading_zero": None,
                    "number_of_leading_zeros": None,
                    "raw_input": data,
                    "preferred_domestic_carrier_code": None,
                }
            )

        return super().to_internal_value(data)


class PaymentSerializer(serializers.Serializer):
    order_id = serializers.IntegerField(required=False)
    id = serializers.IntegerField(read_only=True)
    amount = serializers.DecimalField(
        max_digits=10, decimal_places=1, min_value=1, required=False
    )
    phone_number = CustomPhoneNumberField(required=False)
    email = serializers.CharField(required=False)
    status = serializers.CharField(read_only=True)
    partial = serializers.BooleanField(required=False)
    payment_method = serializers.ChoiceField(
        choices=["momo", "airtel", "mpesa", "ihela"]
    )
    invoice_id = serializers.IntegerField(required=False)

    def validate_phone_number(self, value):
        data = self.get_initial()
        payment_method = data.get("payment_method")
        carrier = cr.name_for_number(value, "en")

        carrier = "Ihela" if carrier in ["Leo", "Viettel"] else carrier
        print("carrier+++++++===========--", carrier)

        if value.country_code == 256:
            if value.number_of_leading_zeros is None:
                prefix = str(value.national_number)[:2]
            else:
                prefix = str(value.national_number)[
                    int(value.number_of_leading_zeros) :
                ][:2]
            if len(prefix) == 2 and not str(prefix).startswith("0"):
                prefix = f"0{str(prefix)}"
            print("this here---", prefix)
            crr = {"074": "Airtel", "070": "Airtel", "075": "Airtel", "076": "MTN"}.get(
                prefix
            )
            carrier = crr if crr is not None else carrier

        allowed_method = PaymentMethod.objects.filter(
            carrier=carrier, name=payment_method
        ).first()
        # check if carrier exists and is allowed
        print(
            carrier,
            payment_method,
            PaymentMethod.objects.filter(carrier=carrier, name=payment_method).first(),
            allowed_method,
            "----------------this runs",
        )
        if not allowed_method:
            msg = _("Phone number carrier not allowed  for selected payment method")
            raise serializers.ValidationError(msg)
        return value

    def validate_payment_method(self, value):
        user = self.context.get("request").user
        ordering_for = User.get_user(user.ordering_for)
        user = ordering_for if ordering_for else user
        data = self.get_initial()
        country = user.company_id
        accepted_payment_methods = country.payment_methods.values_list(
            "name", flat=True
        )

        payment_method = data.get("payment_method")
        if payment_method not in accepted_payment_methods:
            msg = _("Selected payment method is not allowed in your country")
            raise serializers.ValidationError(msg)
        return value

    def validate_order_details(self, validated_data):
        user = self.context.get("request").user
        ordering_for = User.get_user(user.ordering_for)
        user = ordering_for if ordering_for else user
        order_id = validated_data.get("order_id")
        invoice_id = validated_data.get("invoice_id")
        partial = validated_data.get("partial", False)
        # check payment table for the order
        existing_payment = (
            Payment.objects.filter(invoice_id=invoice_id, status=RECEIVED).first()
            if invoice_id
            else Payment.objects.filter(order_id=order_id, status=RECEIVED).first()
        )

        if existing_payment:
            msg = _(
                "You still have a pending transaction under this order/invoice ,please try again after this is complete"
            )
            raise serializers.ValidationError({"order_id": msg})

        try:
            order = Orders.odoo_get(
                order_id, client=user.odoo_client, fields=["id", "amount_total", "name"]
            )
            if len(order) == 1:
                order = order[0]
        except Exception as error:
            logger.error(f"{error} odoo fetch failed")
            order = None
        if order:
            amount_total = math.ceil(order.get("amount_total"))
            has_free_products = order.get("has_free_products")
            amount = math.ceil(validated_data.get("amount"))
            if not partial and (amount != amount_total) and not has_free_products:
                msg = _(
                    f"Total amount required required for this order is {amount_total} not {amount}"
                )
                raise serializers.ValidationError({"amount": msg})
            return order
        elif invoice_id:
            try:
                invoice = Invoices.odoo_get(invoice_id, client=user.odoo_client)
            except Exception as error:
                logger.error(f"{error} odoo fetch failed")
                invoice = None
            if invoice:
                return invoice[0]
            msg = _("Invoice not found")
            raise serializers.ValidationError({"invoice_id": msg})

        else:
            msg = _("Order not found")
            raise serializers.ValidationError({"order_id": msg})

    def validate(self, attrs):
        if not attrs.get("phone_number", None) and not attrs.get("email", None):
            raise serializers.ValidationError(
                _("phone_number or email required for payment")
            )
        validated_data = super().validate(attrs)
        order = self.validate_order_details(validated_data)
        if not validated_data.get("phone_number", None):
            validated_data["phone_number"] = validated_data["email"]
        if not attrs.get("invoice_id"):
            validated_data["order"] = order
        else:
            validated_data["invoice"] = order
            validated_data["invoice_id"] = attrs.get("invoice_id")
            print(
                order,
                f"------invoice--------------{validated_data['invoice_id'] }\n\n\n\n\n\n",
            )
        return validated_data

    def create(self, validated_data):
        user = self.context.get("request").user
        ordering_for = User.get_user(user.ordering_for)
        user = ordering_for if ordering_for else user
        payment_method = validated_data.get("payment_method")
        payment_method_obj = PaymentMethod.objects.filter(name=payment_method).first()
        phone_number = validated_data.get("phone_number")
        amount = validated_data.get("amount")
        order = validated_data.get("order", None)
        invoice = validated_data.get("invoice", None)
        order_id = validated_data.get("order_id")
        invoice_id = validated_data.get("invoice_id")
        status = "received"
        seconds = 20
        # get payment provider payment class
        external_id = str(uuid4())
        if payment_method == "momo":
            provider_api = MomoApi()
        elif payment_method == "mpesa":
            provider_api = MpesaApi()
            seconds = 3 * 60
        elif payment_method == "airtel":
            provider_api = AirtelOpenApi()
            external_id = external_id[:24]
        elif payment_method == "ihela":
            provider_api = IhelaClient()
            # external_id = external_id[:24]
        else:
            msg = _("Selected payment method is not allowed in your country")
            raise serializers.ValidationError(msg)

        obj = {}
        if order and not invoice_id:
            order_name = order.get("name")
            obj = {
                "status": status,
                "phone_number": phone_number,
                "order_id": order_id,
                "reference_no": external_id,
                "amount": amount,
                "provider": payment_method_obj,
                "user_id": user,
                "company_id": user.company_id,
                "order_name": order_name,
            }
        else:
            invoice_name = invoice.get("display_name")
            obj = {
                "status": status,
                "phone_number": phone_number,
                "invoice_id": invoice_id,
                "reference_no": external_id,
                "amount": amount,
                "provider": payment_method_obj,
                "user_id": user,
                "company_id": user.company_id,
                "invoice_name": invoice_name,
            }
        payment = Payment.objects.create(**obj)
        payment.save()
        print("-=\n\n\n\n\n\n----phonenumber", phone_number)

        ihela_extra_args = {}

        try:
            response, reference_no = provider_api.request_to_pay(
                phone_number, amount, external_id
            )
            c = response
            print(f"\n\n\n\n\n---{provider_api}---++__+_+_+{c}")
            print(
                f"payment_method------>>>>>{payment_method} : {provider_api} {c.json()}"
            )

            if payment_method == "ihela":
                logger.info(
                    f"\n\n\n\n--payment_method----- ---{payment_method}-------ihela_extra_args before ==={ihela_extra_args}-\n\n\n\n\n"
                )
                status_code = response["response_status"]
                response = response["bill"]
                ihela_extra_args["merchant_reference"] = response["merchant_reference"]
                ihela_extra_args["code"] = response["code"]
                logger.info(
                    f"\n\n\n\n--payment_method----- ---{payment_method}-------ihela_extra_args after ==={ihela_extra_args}-\n\n\n\n\n"
                )
            else:
                status_code = response.status_code
                response = response.json()

        except Exception as error:
            payment.hard_delete()
            print(error, "============error===================>")
            msg = _("Payment failed, please try again later")
            logger.error(error)
            raise serializers.ValidationError(msg) from error
        print(response, "============response===============>")

        if status_code > 202:
            self.raise_for_error(response, payment_method, payment)
        # update reference number if changed
        if reference_no != external_id:
            payment.reference_no = reference_no
            payment.save()

        print(
            f"\n\n\n\nn{reference_no}======beforeeeeee======reference_no=====payment task created==========>\n\n\n\n"
        )
        x = create_payment_task(
            ref_no=reference_no, seconds=seconds, **ihela_extra_args
        )
        print(
            f"\n\n\n\nn{reference_no}======={x} {vars(x)}===before==check_payment_status.apply_async task created==========>\n\n\n\n"
        )
        check_payment_status.apply_async(
            (
                payment_method,
                reference_no,
            ),
            dict(ihela_extra_args),
            countdown=30,
        )
        print(
            f"\n\n\n\nn{reference_no}======={x} {vars(x)}=====check_payment_status.apply_async task created==========>\n\n\n\n"
        )
        validated_data["status"] = status
        validated_data["id"] = payment.pk
        return validated_data

    def raise_for_error(self, response, payment_method, payment):
        message = self.get_error_message(response, payment_method)
        payment.status = "failed"
        payment.message = message
        payment.save()
        msg = _(message or "Unexpected error")
        logger.error(msg)
        raise serializers.ValidationError(msg)

    def get_error_message(self, response, method):
        message = _("Payment request failed, please try again later")
        try:
            json_data = response.json()
            if method == "momo":
                message = json_data.get("message")
            elif method == "mpesa":
                json_data
                message = json_data.get(
                    "ResultDesc", json_data.get("errorMessage", "Unexpected error")
                )
        except Exception as error:
            logger.error("Failed to get error message")
        return message


class MomoCallbackSerializer(serializers.Serializer):
    financialTransactionId = serializers.CharField(required=False)
    externalId = serializers.CharField(required=False)
    amount = serializers.CharField(required=False)
    currency = serializers.CharField(required=False)
    payer = serializers.JSONField(required=False)
    payeeNote = serializers.CharField(required=False)
    status = serializers.CharField(required=False)
    reason = serializers.CharField(required=False)

    def create(self, validated_data):
        try:
            print(
                "\n\n\n\n\n-----momo payment serializer data",
                validated_data,
                "\n\n\n\n",
            )
            logger.info(
                f"\n\n\n\n----------momo payment serializer data #{validated_data}"
            )
            reference_no = validated_data.get("externalId")
            amount = validated_data.get("amount")
            transaction_id = validated_data.get("financialTransactionId")
            currency = validated_data.get("currency")
            data = validated_data.get("payer")
            status = validated_data.get("status")
            db_transaction = validated_data.get("transaction")
            message = validated_data.get("status")
            reason = validated_data.get("reason")

            # check transaction status
            if status.lower() == "successful" and reference_no:
                status = "successful"
                db_transaction.transaction_id = transaction_id
                db_transaction.data = data

            if status.lower() == "failed":
                # status = "failed"
                message = f"transaction could not be processed, check your account and try again\nreason: {reason}"
            db_transaction.status = status
            db_transaction.message = message
            db_transaction.save()
            logger.info(
                f"\n\n\n\n----------momo payment db_transaction#{db_transaction}"
            )
            # db_transaction.message = message
            # return validated_data
        except Exception as e:
            print(Helpers.stacktrace(e, "-----momo payment error calback"))

        return validated_data


class AirtelCallbackTransactionSerializer(serializers.Serializer):
    airtel_money_id = serializers.CharField(required=False)
    id = serializers.CharField(required=False)
    message = serializers.CharField(required=False)
    status_code = serializers.CharField()


class AirtelCallbackSerializer(serializers.Serializer):
    transaction = AirtelCallbackTransactionSerializer()

    def create(self, validated_data):
        try:
            db_record = validated_data.get("db_record")
            transaction = validated_data.get("transaction")

            transaction_id = transaction.get("airtel_money_id")

            currency = validated_data.get("currency")
            data = validated_data.get("payer")
            status = transaction.get("status_code")

            db_record.data = data
            # check transaction status
            message = None
            if status.lower() == "ts" and transaction_id:
                status = "successful"
                db_record.transaction_id = transaction_id
                db_record.data = data
            else:
                message = transaction.get("message", status + "failed try again")
                status = "failed"
                db_record.transaction_id = transaction_id
                db_record.data = data
            db_record.status = status
            if message is not None:
                db_record.message = message
            db_record.save()
        except Exception as e:
            print(Helpers.stacktrace(e, "-----airtel payment error calback"))

        return validated_data


class PaymentsOverviewSerializer(PaymentSerializer):
    payment_type = serializers.SerializerMethodField()
    invoice_name = serializers.CharField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    transaction_id = serializers.CharField(read_only=True)
    user = serializers.SerializerMethodField()
    payment_method = serializers.SerializerMethodField()

    def get_payment_method(self, obj):
        try:
            return PaymentMethod.objects.get(pk=obj.provider_id).name
        except PaymentMethod.DoesNotExist:
            return "Unknown"

    def get_user(self, obj):
        try:
            user = User.objects.get(pk=obj.user_id_id)
            return f"{user.first_name} {user.last_name}"
        except User.DoesNotExist:
            return "User not found."

    def get_payment_type(self, obj):
        app = "App Order Payment"
        credit = "Credit Repayment"
        return credit if obj.invoice_id else app


class MpesaSuccessCallbackSerializer(serializers.Serializer):
    Name = serializers.ChoiceField(
        choices=(
            "Amount",
            "MpesaReceiptNumber",
            "Balance",
            "TransactionDate",
            "PhoneNumber",
        )
    )
    Value = serializers.CharField(required=False)


class Item(serializers.Serializer):
    Item = MpesaSuccessCallbackSerializer(many=True)

    def to_representation(self, data):
        rep = super().to_representation(data)
        items = rep.get("Item")
        if items:
            rep["Item"] = {item["Name"]: item.get("Value", None) for item in items}
        return rep


class MpesaDefaultCallbackSerializer(serializers.Serializer):
    MerchantRequestID = serializers.CharField()
    CheckoutRequestID = serializers.CharField()
    ResultCode = serializers.IntegerField()
    ResultDesc = serializers.CharField()
    CallbackMetadata = Item(required=False)


class MpesaCallbackBodySerializer(serializers.Serializer):
    stkCallback = MpesaDefaultCallbackSerializer()


class MpesaCallbackSerializer(serializers.Serializer):
    Body = MpesaCallbackBodySerializer()

    def create(self, validated_data):
        data = validated_data["Body"].get("stkCallback")
        status = validated_data.pop("status")
        trans_id = validated_data.pop("trans_id")
        db_transaction = validated_data.pop("transaction")
        merchant_id = data.get("MerchantRequestID")
        message = data.get("ResultDesc")
        if trans_id:
            db_transaction.transaction_id = trans_id
        db_transaction.status = status
        db_transaction.message = message
        db_transaction.data = validated_data
        db_transaction.save()
        return validated_data


class PaymentMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentMethod
        fields = ("id", "label", "name", "carrier")


class TransactionSerializer(serializers.ModelSerializer):
    payment_method = PaymentMethodSerializer(source="provider")

    class Meta:
        model = Payment
        exclude = (
            "data",
            "reference_no",
            "provider",
            "company_id",
            "user_id",
        )
