import json
import os
from uuid import uuid4

import requests
from django.core.management.base import BaseCommand, CommandError
from src.payment.aggregator.mpesa_api import MpesaApi
from src.payment.utils import next_reference_no
from src.payment.models import Payment


class Command(BaseCommand):
    help = "Request to pay mpesa Api"

    def add_arguments(self, parser):
        parser.add_argument("--ref")

    def handle(self, *args, **options):
        mpesa = MpesaApi()
        if options["ref"]:
            ref = options["ref"]
        else:
            external_id = f"HEALTHENT-{next_reference_no(Payment())}"
            res, ref = mpesa.request_to_pay("+254722002100", 500, external_id)
            if res.status_code > 202:
                text = [f"{key}:{value}" for key, value in res.json().items()]
                return self.stdout.write(self.style.ERROR(" ".join(text)))
            text = [f"{key}:{value}" for key, value in res.json().items()]
            self.stdout.write(self.style.SUCCESS(" ".join(text)))
            self.stdout.write(self.style.SUCCESS("\n"))
        res = mpesa.get_transaction_status(ref)
        text = [f"{key}:{value}" for key, value in res.items()]
        self.stdout.write(self.style.SUCCESS(" ".join(text)))
        self.stdout.write(self.style.SUCCESS(ref))
