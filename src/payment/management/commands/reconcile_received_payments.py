from django.core.management.base import BaseCommand, CommandError
from src.payment.utils import reconcile_received_payments


class Command(BaseCommand):
    help = "Reconcile all received status payments"

    def handle(self, *args, **options):
        reconcile_received_payments()
