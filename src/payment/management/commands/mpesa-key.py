import os
from uuid import uuid4

import requests
from django.core.management.base import BaseCommand, CommandError
from src.payment.aggregator.mpesa_api import MpesaApi


class Command(BaseCommand):
    help = "Create Momo Api User"

    def add_arguments(self, parser):
        parser.add_argument("--uuid")

    def handle(self, *args, **options):
        mpesa = MpesaApi()
        another = mpesa.sign_certificate()
        output = mpesa.get_api_token().json()
        if output is None:
            raise CommandError("API User creation failed")
        self.stdout.write(
            self.style.SUCCESS(
                f"expires_in: {output['expires_in']}  access_token: {output['access_token']}"
            )
        )
        self.stdout.write(self.style.SUCCESS(f"expires_in: {another}"))
