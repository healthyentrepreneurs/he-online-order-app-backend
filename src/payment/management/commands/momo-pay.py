import json
import os
from uuid import uuid4

import requests
from django.core.management.base import BaseCommand, CommandError
from src.payment.aggregator.momo_api import MomoApi


class Command(BaseCommand):
    help = "Request to pay Momo Api"

    def add_arguments(self, parser):
        parser.add_argument("--ref")

    def handle(self, *args, **options):
        momo = MomoApi()
        if options["ref"]:
            ref = options["ref"]
        else:
            external_id = str(uuid4())
            phone = "0781734373"
            response, ref = momo.request_to_pay(phone, 60000, external_id, "uganda")
            if response.status_code > 202:
                try:
                    text = [f"{key}:{value}" for key, value in response.json().items()]
                    self.stdout.write(self.style.ERROR(" ".join(text)))
                    self.stdout.write(self.style.ERROR("\n"))
                except Exception:
                    pass
                raise CommandError("API payment creation failed", response.status_code)
        res = momo.get_transaction_status(ref)
        text = [f"{key}:{value}" for key, value in res.items()]
        self.stdout.write(self.style.SUCCESS(" ".join(text)))
        self.stdout.write(self.style.SUCCESS(ref))
