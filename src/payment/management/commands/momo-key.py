import os
from uuid import uuid4

import requests
from django.core.management.base import BaseCommand, CommandError
from src.payment.aggregator.momo_api import MomoApi


class Command(BaseCommand):
    help = "Create Momo Api User"

    def add_arguments(self, parser):
        parser.add_argument("--uuid")

    def handle(self, *args, **options):
        momo = MomoApi()
        if options["uuid"]:
            id = options["uuid"]
        else:
            id = momo.create_api_user()
            if id is None:
                raise CommandError("API User creation failed")
        output = momo.get_api_user(id)
        if output is None:
            raise CommandError("API User creation failed")
        self.stdout.write(
            self.style.SUCCESS(f"id: {output['id']}  apiKey: {output['apiKey']}")
        )
