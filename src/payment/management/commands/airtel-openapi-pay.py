import json
import os
from uuid import uuid4

import requests
from django.core.management.base import BaseCommand, CommandError
from src.payment.aggregator.airtel_openapi import AirtelOpenApi


class Command(BaseCommand):
    help = "Request to pay airtel_openapi Api"

    def add_arguments(self, parser):
        parser.add_argument("--ref")

    def handle(self, *args, **options):
        airtel_openapi = AirtelOpenApi()
        if options["ref"]:
            ref = options["ref"]
        else:
            external_id = str(uuid4())[:25]
            phone = "752332136"
            response, ref = airtel_openapi.request_to_pay(
                phone, 600, external_id, "uganda"
            )
            if response.status_code > 202:
                try:
                    text = [f"{key}:{value}" for key, value in response.json().items()]
                    self.stdout.write(self.style.ERROR(" ".join(text)))
                    self.stdout.write(self.style.ERROR("\n"))
                except Exception:
                    pass
                raise CommandError("API payment creation failed", response.status_code)
        res = airtel_openapi.get_transaction_status(ref)
        text = [f"{key}:{value}" for key, value in res.items()]
        self.stdout.write(self.style.SUCCESS(" ".join(text)))
        self.stdout.write(self.style.SUCCESS(ref))
