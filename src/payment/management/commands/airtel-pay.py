import json
import os
from uuid import uuid4

import requests
from django.core.management.base import BaseCommand, CommandError
from src.payment.aggregator.airtel_api import AirtelApi


class Command(BaseCommand):
    help = "Request to pay Airtel Api"

    def add_arguments(self, parser):
        parser.add_argument("--ref")

    def handle(self, *args, **options):
        airtel = AirtelApi()
        if options["ref"]:
            ref = options["ref"]
        else:
            external_id = str(uuid4())
            phone = "753083733"
            response, ref = airtel.request_to_pay(phone, 500, external_id)
            if response.status_code > 202:
                try:
                    text = [f"{key}:{value}" for key, value in response.json().items()]
                    self.stdout.write(self.style.ERROR(" ".join(text)))
                    self.stdout.write(self.style.ERROR("\n"))
                except Exception:
                    pass
                raise CommandError("Request failed", response.status_code)
        res = airtel.get_transaction_status(ref)
        text = [f"{key}:{value}" for key, value in res.items()]
        self.stdout.write(self.style.SUCCESS(" ".join(text)))
        self.stdout.write(self.style.SUCCESS(ref))
