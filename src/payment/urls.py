from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import PaymentViewset, PaymentMethodViewSet, PaymentsOverviewViewSet

router = routers.DefaultRouter()
router.register(r"", PaymentViewset)
router.register(r"payment_methods", PaymentMethodViewSet)
router.register(r"overview", PaymentsOverviewViewSet)

urlpatterns = [
    path(r"", include(router.urls)),
]
