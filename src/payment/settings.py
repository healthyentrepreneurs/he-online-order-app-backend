import datetime

from django.conf import settings
from rest_framework.settings import APISettings

MOMO_API_SETTINGS = getattr(settings, "MOMO_API", None)

MOMO_API_DEFAULTS = {
    "CALLBACK_HOST": None,
    "PRIMARY_SUBSCRIPTION_KEY": None,
    "API_USER_ID": None,
    "API_SECRET_KEY": None,
    "ENVIRONMENT": "sandbox",
    "BASE_URL": "https://ericssonbasicapi2.azure-api.net",
    "PAYEE_NOTE": "Testing",
    "PAYER_MESSAGE": "Healthy entrepreneurs order payment",
}

MPESA_API_SETTINGS = getattr(settings, "MPESA_API", None)

MPESA_DEFAULTS = {
    "CONSUMER_KEY": None,
    "CALLBACK_URL": "https://heorderapp.online",
    "CONSUMER_SECRET": None,
    "BASE_URL": "https://sandbox.safaricom.co.ke",
    "COMPANY_CODE": "174379",
    "TILL_NUMBER": "700952",
    "LIPA_PASS_KEY": None,
    "PAYER_MESSAGE": "Healthy entrepreneurs order payment",
    "ACCOUNT_REFERENCE": "HEALTHYENT",
}

AIRTEL_API_SETTINGS = getattr(settings, "AIRTEL_API", None)

AIRTEL_DEFAULTS = {
    "BASE_URL": "https://appgw.airtel.ug:8143/services/CAPI",
    "COMPANY_CODE": "100089954",
    "CALLBACK_URL": None,
    "USERNAME": "HEALTHENT",
    "PASSWORD": "HEALTHENT@1234",
    "INTERFACE_ID": "HEALTHENT",
    "MEMO": "Healthy entrepreneurs order payment",
    "CERT_PATH": None,
    "CERT_KEY_PATH": None,
    "ISSUER": "HEALTH",
    "SUBJECT": "HEALTH",
    "KEY": None,
    # url='http://172.27.77.135:9192/services/CAPI'
}


AIRTEL_OPENAPI_SETTINGS = getattr(settings, "AIRTEL_OPENAPI", None)

AIRTEL_OPENAPI_DEFAULTS = {
    "BASE_URL": "https://openapiuat.airtel.africa",
    "CALLBACK_URL": None,
    "API_USER_ID": None,
    "API_SECRET_KEY": None,
    "ENVIRONMENT": "TEST",
    "PAYER_MESSAGE": "Healthy entrepreneurs",
}

IHELA_API_SETTINGS = getattr(settings, "IHELA_API", None)

IHELA_API_DEFAULTS = {
    "BASE_URL": "https://testgate.ihela.online",
    "CLIENT_ID": None,
    "CLIENT_SECRET": None,
    "PAYER_MESSAGE": "Healthy entrepreneurs",
    "PROD_ENV": False,
}


momo_api_settings = APISettings(MOMO_API_SETTINGS, MOMO_API_DEFAULTS)
mpesa_api_settings = APISettings(MPESA_API_SETTINGS, MPESA_DEFAULTS)
airtel_api_settings = APISettings(AIRTEL_API_SETTINGS, AIRTEL_DEFAULTS)
airtel_openapi_settings = APISettings(AIRTEL_OPENAPI_SETTINGS, AIRTEL_OPENAPI_DEFAULTS)
ihela_api_settings = APISettings(IHELA_API_SETTINGS, IHELA_API_DEFAULTS)
