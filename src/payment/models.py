import logging

from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models.signals import post_init, post_save, pre_save
from src.core.models import SoftDeletionModel, TimestampedModel
from src.orders.models import Orders
from src.utils.fields import CustomPhoneNumberField
from django.conf import settings
from src.djangodoo.utils import switch_country
from django.utils.translation import gettext as _
from django.db.models import Q

from src.utils.helpers import Helpers


logger = logging.getLogger(__name__)


PENDING = "PENDING"
SUCCESSFUL = "SUCCESSFUL"
FAILED = "FAILED"
RECEIVED = "RECEIVED"

STATUS_CHOICES = (
    ("pending", PENDING),
    ("successful", SUCCESSFUL),
    ("failed", FAILED),
    ("received", RECEIVED),
)


class PaymentMethod(TimestampedModel):
    label = models.CharField(max_length=200, null=True)
    name = models.CharField(max_length=200, null=True, unique=True)
    carrier = models.CharField(max_length=200, null=True)
    journal_id = models.IntegerField(
        default=20, help_text="Journal for creating orders"
    )

    def __str__(self):
        return self.label


class Payment(TimestampedModel, SoftDeletionModel):
    phone_number = CustomPhoneNumberField(null=True, blank=True)
    transaction_id = models.CharField(max_length=200, null=True)
    order_id = models.CharField(max_length=200, null=True)
    invoice_id = models.CharField(max_length=200, null=True)
    order_name = models.CharField(max_length=200, null=True)
    invoice_name = models.CharField(max_length=200, null=True)
    reference_no = models.CharField(max_length=200, null=True, unique=True)
    reason = models.CharField(max_length=200, null=True)
    amount = models.IntegerField(null=True)
    status = models.CharField(max_length=200, default="pending", choices=STATUS_CHOICES)
    data = JSONField(null=True, blank=True)
    message = models.CharField(max_length=200, null=True, blank=True)
    provider = models.ForeignKey(
        PaymentMethod, on_delete=models.DO_NOTHING, parent_link=True, null=True
    )
    user_id = models.ForeignKey(
        "user.User", on_delete=models.DO_NOTHING, parent_link=True, null=True
    )
    company_id = models.ForeignKey(
        "user.Country", on_delete=models.DO_NOTHING, parent_link=True, null=True
    )
    previous_status = None
    partial = models.BooleanField(default=False)
    processed = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["transaction_id", "provider", "user_id"],
                name="unique_received_transactions",
                condition=Q(status="received"),
            )
        ]
        ordering = ["-updated_at"]

    @staticmethod
    def post_save(sender, **kwargs):
        instance = kwargs.get("instance")
        # created = kwargs.get("created")
        invoice_payment = instance.invoice_name
        if (
            instance.previous_status != instance.status
            and instance.processed is False
            and instance.status.lower()
            in [
                "successful",
            ]
        ):
            # remove schedule if any
            Payment.clear_schedules(instance)
            if instance.status.lower() == "failed":
                return
            # transition order
            try:
                if invoice_payment:
                    Payment.process_invoice(instance)
                else:
                    Payment.process_order(instance)
            except Exception as error:
                print(error)
                logger.exception(f"Order/Invoice approval failed  {vars(instance)}")

    @staticmethod
    def clear_schedules(instance):
        from django_q.models import Schedule

        if schedule := Schedule.objects.filter(name=instance.reference_no).first():
            schedule.delete()

    @staticmethod
    def process_order(instance):
        from src.payment.serializers import TransactionSerializer

        from src.utils.tasks import send_notification
        from src.payment.utils import check_payment_created_in_odoo

        client = switch_country(instance.company_id.odoo_id)

        payment_in_odoo = Helpers.call_with_retries(
            lambda: check_payment_created_in_odoo(instance, client)
        )
        print(f"----------payment_in_odoo----{payment_in_odoo}------", instance.status)
        if (
            instance.status.lower() == "successful"
            and instance.processed is False
            and payment_in_odoo is None
        ):
            instance.processed = True
            instance.save()

            # confirm order

            order_id = int(instance.order_id)
            confirmation = Orders.confirm_order(order_id, client=client)
            print(
                f"\n\n\n\n-------Order confirmation-----Orders.confirm_order-----{order_id}------",
                confirmation,
            )
            # create invoice
            Orders.create_invoice(order_id, client=client)
            # confirm invoice
            invoices = Orders.confirm_invoices(order_id, client=client)
            print(
                f"\n\n\n\n-------invoices confirmation-----Orders.confirm_invoices-----{order_id}------",
                invoices,
            )
            # register payment
            payments = Orders.register_payment(invoices, instance, client=client)
            instance.save()
            route_id = Orders.get_order_route(order_id, client=client)
            route = route_id

            transaction_data = TransactionSerializer(instance).data

            route = (
                Orders.get_delivery_info(route_id[0], client=client)
                if isinstance(route_id, list)
                else dict(route=route)
            )

            transaction_data["delivery_start"] = route.get("date")
            transaction_data["delivery_end"] = route.get("end_date")

            send_notification(
                instance.user_id,
                _(f"Order {instance.order_id} status update"),
                "Confirmed",
                **transaction_data,
            )

        elif instance.status == "failed":
            logger.error("Payment processing failed")
            send_notification(
                instance.user_id,
                _(f"Order {instance.order_id} status update"),
                "Failed",
                **TransactionSerializer(instance).data,
            )

    @staticmethod
    def process_invoice(instance):
        from src.payment.serializers import TransactionSerializer
        from src.accounting.models import Invoices

        from src.utils.tasks import send_notification
        from src.payment.utils import check_payment_created_in_odoo

        client = switch_country(instance.company_id.odoo_id)  # settings.odoo
        payment_in_odoo = Helpers.call_with_retries(
            lambda: check_payment_created_in_odoo(instance, client)
        )

        print(
            f"---------payment in odoo? {payment_in_odoo}-----------", instance.status
        )
        if (
            instance.status.lower() == "successful"
            and instance.processed is False
            and payment_in_odoo is None
        ):
            instance.processed = True
            instance.save()
            # confirm order

            invoice_id = int(instance.invoice_id)

            payments = Invoices.register_payment(invoice_id, instance, client=client)
            instance.save()

            send_notification(
                instance.user_id,
                _(f"Invoice {instance.invoice_id} status update"),
                "Confirmed",
                **TransactionSerializer(instance).data,
            )

        elif instance.status == "failed":
            logger.error("Payment processing failed")
            send_notification(
                instance.user_id,
                _(f"Invoice {instance.invoice_id} status update"),
                "Failed",
                **TransactionSerializer(instance).data,
            )

    @staticmethod
    def remember_status(sender, **kwargs):
        instance = kwargs.get("instance")
        instance.previous_status = instance.status


def pre_save_reference_no(sender, instance: Payment, *args, **kwargs):
    from src.payment.utils import next_reference_no

    if not instance.reference_no:
        instance.reference_no = f"HEALTHENTCOUT-{next_reference_no(instance)}"


pre_save.connect(pre_save_reference_no, sender=Payment)
post_save.connect(Payment.post_save, sender=Payment)
post_init.connect(Payment.remember_status, sender=Payment)
