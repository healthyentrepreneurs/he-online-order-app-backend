import json
import os
from json.decoder import JSONDecodeError
from uuid import uuid4
from urllib.parse import urlsplit

import requests
from django.conf import settings
from requests import Request, Session
from requests._internal_utils import to_native_string
from requests.auth import AuthBase, HTTPBasicAuth
from src.payment.settings import momo_api_settings as momo

from .utils import format_phone_number, requests_retry_session
import logging

logger = logging.getLogger(__name__)


class MoMoAuth(AuthBase):
    """Attaches Authentication to the given Request object."""

    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["Authorization"] = "Bearer " + to_native_string(self.token)
        return r


def json_patch(self, **kwargs):
    def inner_json():
        return format_request_statuses(self.status_code)

    return inner_json


def format_request_statuses(status_code):
    status_message_map = {
        202: "Request has been accepted",
        400: "Invalid data was sent in the request",
        409: "Duplicated reference id. Creation of resource failed.",
        500: "An internal error occurred while processing.",
    }
    message = status_message_map.get(status_code, "Unexpected error occured")
    return {"message": message}


class MomoApi:
    environments = {
        "uganda": {"code": "mtnuganda", "currency": "UGX"},
        "ghana": {"code": "mtnghana", "currency": "UGX"},
        "ivorycoast": {"code": "mtnivorycoast", "currency": "UGX"},
        "zambia": {"code": "mtnzambia", "currency": "UGX"},
        "cameroon": {"code": "mtncameroon", "currency": "UGX"},
        "benin": {"code": "mtnbenin", "currency": "UGX"},
        "congo": {"code": "mtncongo", "currency": "UGX"},
        "swaziland": {"code": "mtnswaziland", "currency": "UGX"},
        "guineaconakry": {"code": "mtnguineaconakry", "currency": "UGX"},
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._session = Session()

    def request(self, method, url, headers, post_data=None):
        self.auth_token = self.get_api_token().json()["access_token"]
        request = Request(
            method,
            url,
            data=json.dumps(post_data),
            headers=headers,
            auth=MoMoAuth(self.auth_token),
        )

        prepped = self._session.prepare_request(request)

        resp = requests_retry_session(sesssion=self._session).send(
            prepped, verify=False
        )
        return resp

    def get_api_token(self):
        url = f"{momo.BASE_URL}/collection/token/"
        print(url, "------------\n\n\n")
        data = json.dumps({})
        headers = {
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": momo.PRIMARY_SUBSCRIPTION_KEY,
        }
        response = requests.post(
            url,
            auth=HTTPBasicAuth(momo.API_USER_ID, momo.API_SECRET_KEY),
            data=data,
            headers=headers,
        )
        return response

    def request_to_pay(
        self,
        phone_number,
        amount,
        external_id,
        country="uganda",
        **kwargs,
    ):
        callback = f"{momo.CALLBACK_HOST}/api/payments/momo_callback/"
        environment = self.get_environment(country=country)
        data = {
            "payer": {
                "partyIdType": "MSISDN",
                "partyId": format_phone_number(phone_number),
            },
            "payeeNote": momo.PAYEE_NOTE,
            "payerMessage": momo.PAYER_MESSAGE,
            "externalId": external_id,
            "currency": environment.get("currency"),
            "amount": str(amount),
        }
        headers = {
            "X-Target-Environment": environment.get("code"),
            "Content-Type": "application/json",
            "X-Reference-Id": external_id,
            "Ocp-Apim-Subscription-Key": momo.PRIMARY_SUBSCRIPTION_KEY,
        }
        if momo.CALLBACK_HOST:
            headers["X-Callback-Url"] = callback
        url = "{0}/collection/v1_0/requesttopay".format(momo.BASE_URL)
        response = self.request("POST", url, headers, data)
        logger.info(
            f"\n\n\n\n\n{headers}-------------++++++++-------\n\n\n{data}\n\n\n\n"
        )
        response.json = json_patch(response)
        logger.info(
            f"\n\n\n\n\n{response}----------response, external_id---+++-------\n\n\n{external_id}\n\n\n\n"
        )
        return response, external_id

    def get_transaction_status(self, transaction_id):
        logger.info(
            f"\n\n\n\n-----getting momo transaction status for --before api calllll--> {transaction_id}"
        )
        url = f"/collection/v1_0/requesttopay/{transaction_id}"
        environment = self.get_environment()
        headers = {
            "X-Target-Environment": environment.get("code"),
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": momo.PRIMARY_SUBSCRIPTION_KEY,
        }
        res = self.request("GET", "{0}{1}".format(momo.BASE_URL, url), headers)
        logger.info(
            f"\n\n\n\n-----getting momo transaction status for ---after-> {transaction_id}-----{res}"
        )
        c = res
        logger.info(
            f"\n\n\n\n-----getting momo transaction status for ----> {c.json()}"
        )
        return res.json()

    def get_environment(self, country="uganda"):
        return self.environments.get(country, momo.ENVIRONMENT)

    def get_host(self, url):
        return urlsplit(url).netloc

    def create_api_user(self):
        """Create momo api user"""

        url = "/v1_0/apiuser"
        data = {"providerCallbackHost": self.get_host(momo.CALLBACK_HOST)}
        id = str(uuid4())
        headers = {
            "Ocp-Apim-Subscription-Key": momo.PRIMARY_SUBSCRIPTION_KEY,
            "X-Reference-Id": id,
        }
        response = requests.post(
            "{0}{1}".format(momo.BASE_URL, url), json=data, headers=headers
        )
        if response.status_code != 201:
            return None
        return id

    def get_api_user(self, id):
        url = f"/v1_0/apiuser/{id}/apikey"
        headers = {
            "Ocp-Apim-Subscription-Key": momo.PRIMARY_SUBSCRIPTION_KEY,
        }
        res = requests.post("{0}{1}".format(momo.BASE_URL, url), headers=headers)
        if res.status_code != 201:
            return None

        data = res.json()
        return {"id": id, "apiKey": data["apiKey"]}

    def get_status(self, ref_no, **kwargs):
        print(
            f"\n\n\n\n\nthis is called_____momo get_status ->>>>>>>------------{ref_no, kwargs}----------->>>>>>>>>\n\n\n\n\n"
        )
        logger.info(
            f"\n\n\n\n-----getting momo get_status for ----> {ref_no} and why jwrrfgs {kwargs}"
        )
        response = self.get_transaction_status(ref_no)
        logger.info(
            f"\n\n\n\n---after--getting momo get_status for --{response}--> {ref_no}"
        )
        print(
            f"\n\n\n\n\nthis is called_____momo after get_status ->>>>>>>------------{response}----------->>>>>>>>>\n\n\n\n\n"
        )

        tran_id = response.get("financialTransactionId")
        amount = response.get("amount")
        status = response.get("status")
        message = "transaction could not be processed, check your account and try again"

        if status == "SUCCESSFUL" and tran_id:
            message = "transaction successful"
            return response, "successful", tran_id, message
        elif status == "PENDING":
            return response, "pending", None, "pending"
        elif status == "FAILED":
            return response, "failed", None, "expired"
        return response, "failed", None, message
