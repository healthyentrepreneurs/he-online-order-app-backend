import json
import os
from json.decoder import JSONDecodeError
from uuid import uuid4
from urllib.parse import urlsplit

import requests
from django.conf import settings
from requests import Request, Session
from requests._internal_utils import to_native_string
from requests.auth import AuthBase, HTTPBasicAuth
from src.payment.settings import airtel_openapi_settings as airtel_openapi

from .utils import format_phone_number, requests_retry_session


class AirtelOpenApiAuth(AuthBase):
    """Attaches Authentication to the given Request object."""

    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["Authorization"] = "Bearer " + to_native_string(self.token)
        return r


class AirtelOpenApi:
    operator = "Airtel"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._session = Session()

    def request(self, method, url, headers, post_data=None):
        self.auth_token = self.get_api_token().json()["access_token"]
        request = Request(
            method,
            url,
            data=json.dumps(post_data),
            headers=headers,
            auth=AirtelOpenApiAuth(self.auth_token),
        )

        prepped = self._session.prepare_request(request)

        resp = requests_retry_session(sesssion=self._session).send(
            prepped, verify=False
        )
        return resp

    def get_api_token(self):
        url = "/merchant/v1/payments/oauth2/token"
        data = json.dumps(
            {
                "client_id": airtel_openapi.API_USER_ID,
                "client_secret": airtel_openapi.API_SECRET_KEY,
                "grant_type": "client_credentials",
            }
        )
        headers = {
            "Content-Type": "application/json",
            "Accept": "*/*",
        }
        response = requests.post(
            "{0}{1}".format(airtel_openapi.BASE_URL, url),
            data=data,
            headers=headers,
        )
        return response

    def request_to_pay(
        self,
        phone_number,
        amount,
        external_id,
        country="uganda",
        **kwargs,
    ):
        print(
            "\n\n\n\n",
            phone_number,
            "-----------phone_number----airetle request to pay--------\n\n\n",
        )

        phone_number = format_phone_number(phone_number, self.operator, code=False)
        data = {
            "reference": airtel_openapi.PAYER_MESSAGE,
            "subscriber": {"country": "UG", "currency": "UGX", "msisdn": phone_number},
            "transaction": {
                "amount": int(amount),
                "country": "UG",
                "currency": "UGX",
                "id": external_id,
            },
        }
        headers = {
            "Content-Type": "application/json",
            "Accept": "*/*",
            "X-Country": "UG",
            "X-Currency": "UGX",
        }
        url = "{0}/merchant/v1/payments/".format(airtel_openapi.BASE_URL)
        response = self.request("POST", url, headers, data)
        print(
            f"\n\n\n\n\n\n logging here--url----{url}- resposners---{response}-----\n\n\n\n"
        )
        response.raise_for_status()
        return response, external_id

    def get_transaction_status(self, external_id):
        url = f"/standard/v1/payments/{external_id}"
        headers = {
            "Accept": "*/*",
            "X-Country": "UG",
            "X-Currency": "UGX",
        }
        res = self.request(
            "GET", "{0}{1}".format(airtel_openapi.BASE_URL, url), headers
        )
        return res.json()

    def get_status(self, ref_no, **kwargs):
        response = self.get_transaction_status(ref_no)

        data = response.get("data")
        transaction = data.get("transaction")
        status = transaction.get("status")

        message = transaction.get("message")

        print(
            "\n\n\n\n",
            status,
            data,
            "-----------status----airetle trans status--------\n\n\n",
        )

        if status == "TS":
            tran_id = transaction.get("airtel_money_id")
            return data, "successful", tran_id, message
        elif status == "TIP":
            return data, "pending", None, "pending"
        elif status == "TF":
            return data, "failed", None, message
        return data, "failed", None, message
