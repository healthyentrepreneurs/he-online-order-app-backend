import phonenumbers
import requests
from phonenumbers import carrier
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from rest_framework.serializers import ValidationError
from phonenumber_field.phonenumber import PhoneNumber
from django.utils.translation import gettext as _


def requests_retry_session(
    retries=3, backoff_factor=0.3, status_forcelist=(502, 504), session=None, **kwargs
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    return session


def format_phone_number(number, operator="MTN", code=True):
    from src.payment.serializers import CustomPhoneNumber

    obj = number
    if not isinstance(number, (PhoneNumber, CustomPhoneNumber)):
        obj = phonenumbers.parse(number, "UG")
        if not phonenumbers.is_valid_number(obj):
            raise ValidationError(_("Invalid Phone number {0}".format(number)))
        if carrier.name_for_number(obj, "en") != operator:
            raise ValidationError(
                _("{0}: Only {1} is supported at the moment".format(number, operator))
            )
    if code:
        return "{1}{0}".format(obj.national_number, obj.country_code)
    return "{0}".format(obj.national_number)


def validate_number(number):
    number_types = (int, float)
    if not type(number) in number_types:
        raise ValidationError(_("{0}: Must be a number".format(number)))
    return number


def validate_string(_string):
    if not isinstance(_string, str):
        raise ValidationError(_("{0}: Must be a string".format(_string)))
    return _string


def validate_uuid(_string):
    try:
        UUID(_string, version=4)
    except ValueError:
        raise ValidationError(_("{0}: Must be a valid uuid4 string".format(_string)))
    return _string
