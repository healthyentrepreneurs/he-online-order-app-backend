import os
import tempfile

import requests
from django.conf import settings


class HTTPAdapter(requests.adapters.HTTPAdapter):
    """Handle a variety of cert types"""

    def cert_verify(self, conn, url, verify, cert):
        if cert:
            if isinstance(cert, tuple) and len(cert) == 2:
                cert_file = tempfile.NamedTemporaryFile(delete=False)
                cert_file.write(cert[0].encode("utf-8"))
                cert_file.close()

                key_file = tempfile.NamedTemporaryFile(delete=False)
                key_file.write(cert[1].encode("utf-8"))
                key_file.close()
                cert = (cert_file.name, key_file.name)
        super().cert_verify(conn, url, verify, cert)


def patch_requests(adapter=True):
    if os.environ.get("DJANGO_SETTINGS_MODULE") == "config.settings.staging":
        requests.sessions.HTTPAdapter = HTTPAdapter
