from src.payment.settings import ihela_api_settings as ihela_api

from ihela_client import MerchantClient

from .utils import format_phone_number, requests_retry_session


class IhelaClient:
    operator = "Ihela"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.client = MerchantClient(
            ihela_api.CLIENT_ID, ihela_api.CLIENT_SECRET, prod=ihela_api.PROD_ENV
        )

    def request_to_pay(
        self,
        phone_number,
        amount,
        external_id,
        country="burundi",
        **kwargs,
    ):
        # phone_number = format_phone_number(phone_number, self.operator, code=False)
        if str(phone_number)[:4] == "+257":
            phone_number = str(phone_number)[4:]

        print(
            f"befroe init bbill---yesy coz I tested this iyii--->>>>>{phone_number} : {amount}\n\n\n\n\n"
        )
        response = self.client.init_bill(
            amount, phone_number, ihela_api.PAYER_MESSAGE, external_id
        )
        print(f"after init bill------>>>>>{phone_number} : {response}\n\n\n\n\n")
        return response, external_id

    def get_transaction_status(self, ref, merchant_reference=None, code=None):
        print(
            merchant_reference,
            code,
            "------merchant_reference------ihela--code\n\n\n\n\n",
        )
        response = self.client.verify_bill(merchant_reference, code)
        bill = response.get("bill", {})

        return bill

    def get_status(self, ref_no, **kwargs):
        print(kwargs, "-----++++====kwargs here===\n\n\n\n")

        response = self.get_transaction_status(ref_no, **kwargs)

        reference = response.get("reference")
        status = response.get("status")

        if status == "Paid":
            return response, "successful", reference, "success"
        elif status == "Pending":
            return response, "pending", None, "pending"
        elif status == "Expired":
            return response, "failed", None, "expired"
        return response, "failed", None, "failed"
