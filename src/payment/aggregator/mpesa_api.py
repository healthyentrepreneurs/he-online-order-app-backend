import json
from base64 import b64encode
from datetime import datetime
from uuid import uuid4

import requests
from django.conf import settings
from requests import Request, Session
from requests._internal_utils import to_native_string
from requests.auth import AuthBase, HTTPBasicAuth
from src.payment.settings import mpesa_api_settings as mpesa

from .utils import format_phone_number, requests_retry_session


class MpesaAuth(AuthBase):
    """Attaches Authentication to the given Request object."""

    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["Authorization"] = "Bearer " + to_native_string(self.token)
        return r


class MpesaApi:
    operator = "Safaricom"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._session = Session()

    def request(self, method, url, headers, post_data=None):
        self.auth_token = self.get_api_token().json()["access_token"]
        request = Request(
            method,
            url,
            data=json.dumps(post_data),
            headers=headers,
            auth=MpesaAuth(self.auth_token),
        )

        prepped = self._session.prepare_request(request)

        resp = requests_retry_session(sesssion=self._session).send(
            prepped, verify=False
        )
        return resp

    def get_api_token(self):
        url = "/oauth/v1/generate?grant_type=client_credentials"
        headers = {
            "Content-Type": "application/json",
        }

        response = requests.get(
            "{0}{1}".format(mpesa.BASE_URL, url),
            auth=HTTPBasicAuth(mpesa.CONSUMER_KEY, mpesa.CONSUMER_SECRET),
        )
        return response

    def generate_timed_password(self):
        trans_time = datetime.now().strftime("%Y%m%d%H%M%S")
        data_to_encode = mpesa.COMPANY_CODE + mpesa.LIPA_PASS_KEY + trans_time
        online_password = b64encode(data_to_encode.encode())
        decode_password = online_password.decode("utf-8")
        return decode_password, trans_time

    def request_to_pay(self, phone_number, amount, external_id, **kwargs):
        url = f"{mpesa.BASE_URL}/mpesa/stkpush/v1/processrequest"
        headers = {"Content-Type": "application/json"}

        password, trans_time = self.generate_timed_password()
        phone_number = format_phone_number(phone_number, self.operator)
        callback = f"{mpesa.CALLBACK_URL}/api/payments/mpesa_callback/"
        data = {
            "BusinessShortCode": mpesa.COMPANY_CODE,
            "Password": password,
            "Timestamp": trans_time,
            "TransactionType": "CustomerBuyGoodsOnline",
            "Amount": int(amount),
            "PartyA": phone_number,
            "PartyB": mpesa.TILL_NUMBER,
            "PhoneNumber": phone_number,
            "CallBackURL": callback,
            "AccountReference": mpesa.ACCOUNT_REFERENCE,
            "TransactionDesc": mpesa.PAYER_MESSAGE,
        }
        ref = None
        response = self.request("POST", url, headers, data)
        if response.status_code == 200:
            ref = response.json().get("CheckoutRequestID")
        return response, ref

    def get_transaction_status(self, transaction_id):
        url = f"{mpesa.BASE_URL}/mpesa/stkpushquery/v1/query"
        headers = {"Content-Type": "application/json"}
        password, trans_time = self.generate_timed_password()
        data = {
            "BusinessShortCode": mpesa.COMPANY_CODE,
            "Password": password,
            "Timestamp": trans_time,
            "CheckoutRequestID": transaction_id,
        }
        res = self.request("POST", url, headers, data)
        return res.json()

    @staticmethod
    def get_transaction_id(success_callback, trans_id):
        for item in success_callback.get("Item"):
            if item.get("Name") == "MpesaReceiptNumber":
                trans_id = item.get("Value")
        return trans_id

    def get_status(self, ref_no, **kwargs):
        response = self.get_transaction_status(ref_no)
        result_code = response.get("ResultCode")
        error_code = response.get("errorCode")
        message = response.get("ResultDesc")
        if error_code:
            return response, "received", None, message
        elif result_code == 0:
            return response, "successful", None, message
        return response, "failed", None, message
