import base64
import json
import random
from datetime import datetime, timedelta

import requests
from src.payment.renderers import CustomXMLParser, CustomXMLRenderer
from src.payment.settings import airtel_api_settings as airtel
import jwt
from .requests_patch import patch_requests
from .utils import format_phone_number


def json_patch(self, **kwargs):
    def inner_json():
        return CustomXMLParser().parse(self.text)

    return inner_json


class AirtelApi:
    _retries = 3
    JWT_EXP_DELTA_DAYS = 2

    def __init__(self):
        self.xml_render = CustomXMLRenderer()

    def request(self, xml):
        cert_file_path = airtel.CERT_PATH
        key_file_path = airtel.CERT_KEY_PATH
        cert = (cert_file_path, key_file_path)
        self.auth_token = self.get_api_token(xml)
        headers = {
            "Content-Type": "application/xml",
            "Authorization": f"Bearer {self.auth_token}",
        }
        requests.adapters.DEFAULT_RETRIES = self._retries
        response = requests.post(
            airtel.BASE_URL,
            data=xml,
            headers=headers,
            cert=cert,
        )
        response.json = json_patch(response)
        return response

    def get_transaction_status(self, transaction_id):
        data = {
            "serviceType": "TXNEQREQ",
            "interfaceId": airtel.INTERFACE_ID,
            "EXTTRID": transaction_id,
        }
        xml_data = self.xml_render.render(data)
        response = self.request(xml_data)
        return response.json()

    def request_to_pay(self, phone_number, amount, external_id, **kwargs):
        data = {
            "interfaceId": airtel.INTERFACE_ID,
            "MSISDN": phone_number,
            "MSISDN2": airtel.COMPANY_CODE,
            "EXTTRID": external_id,
            "REFERENCE": external_id,
            "BILLERID": airtel.COMPANY_CODE,
            "MEMO": airtel.MEMO,
            "serviceType": "MERCHPAY",
            "USERNAME": airtel.USERNAME,
            "PASSWORD": airtel.PASSWORD,
            "AMOUNT": amount,
        }
        xml_data = self.xml_render.render(data)
        response = self.request(xml_data)
        return response, external_id

    def get_status(self, ref_no, **kwargs):
        response = self.get_transaction_status(ref_no)
        tran_id = response.get("TXNID")
        status = response.get("TXNSTATUS")
        message = response.get("MESSAGE")
        if status == 200 and tran_id:
            return response, "successful", tran_id, message
        return response, "failed", None, message

    def get_api_token(self, xml):
        issue_time = datetime.utcnow()
        exp = issue_time + timedelta(days=self.JWT_EXP_DELTA_DAYS)
        payload = {
            "jti": str(random.randint(1, 1000000)),
            "iat": int(issue_time.timestamp()),
            "exp": int(exp.timestamp()),
            "sub": airtel.SUBJECT,
            "iss": airtel.ISSUER,
            "PAYLOAD": xml,
        }
        token = jwt.encode(payload, base64.b64decode(airtel.KEY))
        return token.decode("utf-8")
