# Generated by Django 2.2.10 on 2023-10-30 06:39

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("payment", "0013_auto_20231016_1650"),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name="payment",
            name="unique_payments_per_transaction_when_not_null",
        ),
        migrations.AlterUniqueTogether(
            name="payment",
            unique_together={("transaction_id", "provider", "status", "user_id")},
        ),
    ]
