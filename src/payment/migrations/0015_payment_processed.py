# Generated by Django 2.2.10 on 2023-11-21 18:43

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("payment", "0014_auto_20231030_0939"),
    ]

    operations = [
        migrations.AddField(
            model_name="payment",
            name="processed",
            field=models.BooleanField(default=False),
        ),
    ]
