# Generated by Django 2.2.10 on 2020-11-21 15:22

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("payment", "0007_paymentmethod_journal_id"),
    ]

    operations = [
        migrations.AddField(
            model_name="payment",
            name="message",
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
