from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import DistrictsListAPIView

router = routers.DefaultRouter()
router.register(r"", DistrictsListAPIView, basename="districts")

urlpatterns = [
    path("", DistrictsListAPIView.as_view(), name="list-districts"),
]
