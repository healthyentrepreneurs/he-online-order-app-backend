import logging

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from ..core.pagination import PostLimitOffsetPagination

from .models import Districts
from .serializers import DistrictsSerializer

from django_filters.rest_framework import DjangoFilterBackend


logger = logging.getLogger(__name__)


class DistrictsListAPIView(generics.ListAPIView):
    _model = Districts

    serializer_class = DistrictsSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]

    pagination_class = None

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ["company_id"]

    def get_queryset(self):
        return Districts.objects.all().order_by("name")
