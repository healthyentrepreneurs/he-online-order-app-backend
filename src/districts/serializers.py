from rest_framework import serializers
from src.user.models import Country


class DistrictsSerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    id = serializers.IntegerField(read_only=True)
    country = serializers.PrimaryKeyRelatedField(
        source="company_id",
        queryset=Country.objects.all(),
    )

    class Meta:
        read_only_fields = ["id", "country"]
