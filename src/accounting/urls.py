from django.urls import path, re_path
from rest_framework import routers

from .views import InvoicesViewSet, PaymentTermsViewSet

router = routers.DefaultRouter()

urlpatterns = [
    re_path(r"^invoices/?$", InvoicesViewSet.as_view({"get": "list"}), name="invoices"),
    path(
        r"payment_terms",
        PaymentTermsViewSet.as_view({"get": "list"}),
        name="payment-terms",
    ),
]
