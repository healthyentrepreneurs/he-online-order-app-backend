from rest_framework import serializers


class InvoicesSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    partner_id = serializers.CharField()
    paid = serializers.SerializerMethodField()
    balance = serializers.IntegerField(source="amount_residual_signed")
    sales_person = serializers.SerializerMethodField()
    amount_total_signed = serializers.IntegerField()
    amount_residual_signed = serializers.IntegerField()
    status = serializers.CharField(source="state")
    display_name = serializers.CharField()
    due_date = serializers.DateTimeField(source="date")
    created_at = serializers.DateTimeField(source="create_date")
    order_id = serializers.SerializerMethodField()
    order = serializers.CharField(source="invoice_origin")
    payment_term_id = serializers.SerializerMethodField()
    payment_state = serializers.CharField(required=False)

    def get_sales_person(sef, obj):
        id_ = (
            obj["invoice_user_id"][1]
            if isinstance(obj["invoice_user_id"], list)
            else obj["invoice_user_id"]
        )
        return id_.id if hasattr(id_, "id") else id_

    def get_payment_term_id(sef, obj):
        id_ = (
            obj["invoice_payment_term_id"][0]
            if isinstance(obj["invoice_payment_term_id"], list)
            else obj["invoice_payment_term_id"]
        )
        return id_.id if hasattr(id_, "id") else id_

    def get_paid(sef, obj):
        return int(obj["amount_total_signed"]) - int(obj["amount_residual_signed"])

    def get_order_id(sef, obj):
        return (
            obj["invoice_origin"][2:]
            if isinstance(obj["invoice_origin"], str)
            else obj["invoice_origin"]
        )


class PaymentTermsSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    display_name = serializers.CharField()
    company = serializers.SerializerMethodField()
    name = serializers.CharField()
    sequence = serializers.CharField()
    created_at = serializers.DateTimeField(source="create_date")
    # order_id = serializers.SerializerMethodField()
    note = serializers.CharField()

    def get_company(self, obj):
        try:
            return {"odoo_id": obj["id"], "name": obj["display_name"]}
        except KeyError:
            return {"odoo_id": False, "name": False}
