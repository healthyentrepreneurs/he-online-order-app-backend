import json
from src.core.filters import OdooSearchFilter
from src.djangodoo.utils import switch_country
from src.core.pagination import OdooPagination
from src.djangodoo.authentication import OdooAuthentication, OdooSessionAuthentication
from rest_framework.response import Response

from .models import Invoices, PaymentTerms
from .serializers import InvoicesSerializer, PaymentTermsSerializer
from src.djangodoo.views import OdooObjectViewSet
from django.conf import settings


class InvoicesViewSet(OdooObjectViewSet):
    http_method_names = ["get", "head"]
    queryset = Invoices.objects.all()
    serializer_class = InvoicesSerializer
    pagination_class = OdooPagination
    authentication_classes = [OdooAuthentication, OdooSessionAuthentication]
    filter_backends = [OdooSearchFilter]
    search_fields = ["date", "state", "partner_id"]
    _model = Invoices

    def filter_queryset(self, queryset):
        domains = super().filter_queryset(queryset)
        odoo_id = self.request.user.odoo_id
        use_blank_term = self.request.user.company_id.blank_payment_term
        payment_terms = self.request.user.company_id.payment_terms
        payment_term_ids = (
            payment_terms
            if isinstance(payment_terms, list)
            else json.loads(payment_terms)
        )

        if odoo_id:
            partner_id_domain = [("partner_id", "=", odoo_id)]
            payment_terms_domain = []
            if payment_term_ids:
                payment_terms_domain = [
                    ("invoice_payment_term_id", "in", payment_term_ids)
                ]
                if use_blank_term:
                    payment_terms_domain = ["&", "|"] + payment_terms_domain
                    payment_terms_domain += [
                        ("invoice_payment_term_id", "=", False),
                        ["journal_id.name", "=", "Customer Invoices"],
                    ]
            partner_id_domain += payment_terms_domain
            partner_id_domain.insert(0, "&")

            if domains:
                domains.insert(0, "&")
            domains.extend(partner_id_domain)

        print("InvoicesViewSet domains----------", domains)
        return domains

    def list(self, request, *args, **kwargs):
        if request.query_params.get("open", False):
            odoo_id = request.user.odoo_id
            domain = [
                ("partner_id", "=", odoo_id),
                ("invoice_payment_term_id", "=", 1),
                ("payment_state", "in", ("not_paid", "partial")),
                ("state", "=", "posted"),
            ]
            odoo_client = request.user.odoo_client or switch_country(
                request.user.company_id.odoo_id
            )
            data = Invoices.odoo_fetch(domain, odoo_client)
            serializer = InvoicesSerializer(data, many=True)

            return Response(serializer.data)

        return super().list(request, *args, **kwargs)


class PaymentTermsViewSet(OdooObjectViewSet):
    http_method_names = ["get", "head"]
    queryset = PaymentTerms.objects.all()
    serializer_class = PaymentTermsSerializer
    pagination_class = None
    authentication_classes = [OdooAuthentication, OdooSessionAuthentication]
    filter_backends = [OdooSearchFilter]
    search_fields = ["company_id", "name", "display_name"]
    _model = PaymentTerms

    def list(self, request, *args, **kwargs):
        # fix payment terms query to uganda client
        odoo_client = switch_country(1)
        results = PaymentTerms.odoo_fetch([], odoo_client)
        serializer = PaymentTermsSerializer(results, many=True)

        return Response(dict(results=serializer.data))
