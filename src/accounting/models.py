from src.djangodoo.models import OdooModel
from src.core.models import TimestampedModel, SoftDeletionModel
from src.djangodoo.utils import switch_country
from django.conf import settings
from src.utils.helpers import Helpers


#  * The 'draft' status is used when a user is encoding a new and unconfirmed Invoice.
#  * The 'open' status is used when user creates invoice, an invoice number is generated. It stays in the open status till the user pays the invoice.
#  * The 'in_payment' status is used when payments have been registered for the entirety of the invoice in a journal configured to post entries at bank reconciliation only, and some of them haven't been reconciled with a bank statement line yet.
#  * The 'paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.
#  * The 'cancel' status is used when user cancel invoice.
class Invoices(TimestampedModel, SoftDeletionModel, OdooModel):
    _odoo_model = "account.move"
    _odoo_fields = [
        "partner_id",
        "amount_residual_signed",  #
        "date",  #
        "create_date",
        "display_name",
        "invoice_user_id",
        "invoice_payment_term_id",
        "payment_state",
        "state",
        "amount_total_signed",
        "id",
        "invoice_origin",
    ]

    @classmethod
    def odoo_fetch(cls, domain, client=None, limit=None, offset=0, **kwargs):
        domain = cls.inject_credit_invoices_domain(domain)
        return super().odoo_fetch(
            domain, client, limit, offset, order="create_date asc", **kwargs
        )

    @classmethod
    def odoo_count(cls, domain, client=None):
        domain = cls.inject_credit_invoices_domain(domain)
        return super().odoo_count(domain, client=client)

    @classmethod
    def inject_credit_invoices_domain(cls, domain):
        validated_order_states = [
            "&",
            ["state", "!=", "draft"],
            ["state", "!=", "cancel"],
        ]
        if domain:
            if already_set := [
                check
                for check in validated_order_states
                if check in domain and check != "&"
            ]:
                return domain
        domain.extend(validated_order_states)
        return domain

    @classmethod
    def get_credit_balance(cls, obj, client, payment_term_ids):

        domain = [
            "&",
            ["partner_id", "=", obj.odoo_id],
            ["invoice_payment_term_id", "in", payment_term_ids],
        ]
        use_blank_term = obj.company_id.blank_payment_term
        if use_blank_term:
            [op, cond1, cond2] = domain
            domain = [
                "&",
                op,
                cond1,
                "|",
                cond2,
                ["invoice_payment_term_id", "=", False],
                ["journal_id.name", "=", "Customer Invoices"],
            ]
        records = cls.odoo_fetch(
            domain,
            client=client,
        )
        credit_balance = sum(
            (float(record["amount_residual_signed"]) for record in records)
        )
        return dict(credit_balance=credit_balance)

    @classmethod
    def get_open_invoices_count(cls, odoo_id, client):
        count = cls.odoo_count(
            [
                ("partner_id", "=", odoo_id),
                ("invoice_payment_term_id", "=", 1),
                ("payment_state", "in", ("not_paid", "partial")),
                ("state", "=", "posted"),
            ],
            client=client,
        )
        return dict(open_payments=count)

    @classmethod
    def register_payment(cls, invoice_id, instance, client=None):
        from src.user.models import User
        from src.payment.utils import check_payment_created_in_odoo

        client = switch_country(instance.company_id.odoo_id)

        payment_in_odoo = Helpers.call_with_retries(
            lambda: check_payment_created_in_odoo(instance, client)
        )

        print(
            f"---------payment in odoo? {payment_in_odoo}-----------", instance.status
        )
        if payment_in_odoo is None:

            print("registering payment---start")
            amount = instance.amount
            user_id = instance.user_id.odoo_id
            company = instance.company_id

            payment_method_id = company.payment_method_id
            company_id = company.odoo_id
            currency_id = company.currency_id
            journal_id = instance.provider.journal_id

            odoo_model = cls._odoo_model

            payment_method_line_id_mapper = {
                "momo": 140,
                "mpesa": 101,
                "airtel": 141,
                "ihela": 177,
            }
            outstanding_account_id_mapper = {
                "momo": 799,
                "mpesa": 599,
                "airtel": 799,
            }

            data = {
                "partner_type": "customer",
                "payment_type": "inbound",
                "company_id": company_id,
                "partner_id": user_id,
                "country_code": company.country_code,
                "journal_id": journal_id,
                "outstanding_account_id": outstanding_account_id_mapper[
                    instance.provider.name
                ],
                "payment_method_line_id": payment_method_line_id_mapper[
                    instance.provider.name
                ],  # payment_method_id,
                "state": "draft",
                "amount": float(amount),
                "currency_id": currency_id,
                "from_product_app": True,
                "ref": instance.transaction_id,
            }
            print(
                "\n\n\n\n\n\n---111111111-++++++okay comes here",
                data,
                "\n\n\n\n\n---___",
                float(amount),
            )
            payment_id = client.create("account.payment", data)

            print(payment_id)
            res = None
            try:
                res = client.action_post("account.payment", payment_id)
            except Exception as e:
                print("\n\n\n\nerror occured---action_post------>", e, end="\n\n\n\n")
            cls.attach_payment_to_invoice(
                instance.invoice_id, company_id, payment_id, client=client
            )
            print("\n\n\n\n\n payment_id ----->", payment_id)
            user = User.objects.filter(ordering_for=user_id).first()
            if user:
                user.ordering_for = None
                user.save(update_fields=["ordering_for"])

            print("registering payment---end", res)
            return
        print("\n\n\n\n\n payment already registered ----->", payment_in_odoo)

    @classmethod
    def attach_payment_to_invoice(cls, invoice_id, company_id, payment_id, client=None):
        client = client or switch_country(company_id)

        client.match_payment(payment_id, invoice_id)
        print(f"payment attached for ---->invoice---> {invoice_id}")


class PaymentTerms(TimestampedModel, OdooModel):
    _odoo_model = "account.payment.term"
    _odoo_fields = [
        "company_id",
        "display_name",
        "id",
        "name",
        "create_date",
        "note",
        "sequence",
    ]
