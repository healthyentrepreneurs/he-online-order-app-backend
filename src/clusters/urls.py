from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import ClustersListAPIView

router = routers.DefaultRouter()
router.register(r"", ClustersListAPIView, basename="clusters")

urlpatterns = [
    path("", ClustersListAPIView.as_view(), name="list-clusters"),
]
