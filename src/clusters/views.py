import logging

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from ..core.pagination import PostLimitOffsetPagination

from .models import Clusters
from .serializers import ClustersSerializer

from django_filters.rest_framework import DjangoFilterBackend


logger = logging.getLogger(__name__)


class ClustersListAPIView(generics.ListAPIView):
    _model = Clusters

    serializer_class = ClustersSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]

    pagination_class = None

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ["company_id"]

    def get_queryset(self):
        return Clusters.objects.all().order_by("name")
