from django.db import models
from src.core.models import TimestampedModel, SoftDeletionModel

from src.user.models import Country


class Clusters(TimestampedModel, SoftDeletionModel):
    name = models.CharField(max_length=100, unique=True)
    company_id = models.ForeignKey(
        Country, on_delete=models.CASCADE, parent_link=True, null=True
    )
