"""Contains Custom user manager to add email and password."""

from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _
from phonenumber_field.modelfields import PhoneNumberField
from phonenumbers import parse


class SoftDeletionQuerySet(models.QuerySet):
    def delete(self):
        return super(SoftDeletionQuerySet, self).update(deleted_at=timezone.now())

    def hard_delete(self):
        return super(SoftDeletionQuerySet, self).delete()

    def alive(self):
        return self.filter(deleted_at=None)

    def dead(self):
        return self.exclude(deleted_at=None)


class SoftDeletionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop("alive_only", True)
        super(SoftDeletionManager, self).__init__(*args, **kwargs)

    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(deleted_at=None)
        return SoftDeletionQuerySet(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class CustomUserManager(BaseUserManager):
    """
    User Manager to add email and phone number.

    Admin user model manager where email is the unique identifiers
    for authentication instead of phone numbers.
    """

    def create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError(_("The Email must be set"))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_che_user(self, password, phone_number, email=None, **extra_fields):
        """Create and save a User with the given phone_number and password."""
        if not phone_number:
            raise ValueError(_("The phone must be set"))
        phone = self.normalize_phone(phone_number)
        if not email:
            email = self.normalize_email("{}@{}".format(phone, settings.EMAIL_DOMAIN))
        che_user = self.model(phone_number=phone, email=email, **extra_fields)
        che_user.set_password(password)
        che_user.save()
        return che_user

    def normalize_phone(self, phone_number, region=None):
        region = region or settings.DEFAULT_REGION
        print("\n\n\n\n--normalize_phone---phone_number", phone_number)
        if phone_number is None:
            return phone_number
        phone = parse(phone_number, region)
        return "+" + str(phone.country_code) + str(phone.national_number)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault("is_admin", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError(_("Superuser must have is_staff=True."))
        if extra_fields.get("is_superuser") is not True:
            raise ValueError(_("Superuser must have is_superuser=True."))
        return self.create_user(email, password, **extra_fields)
