from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from rest_framework import authentication, exceptions

User = get_user_model()


class CHEAuthBackend(authentication.BaseAuthentication):
    def authenticate(self, request=None, password=None, phone_number=None):
        if not password or not phone_number:
            return None
        try:
            che = User.objects.get(phone_number=phone_number)
            if che.check_password(password) is True:
                return che
        except User.DoesNotExist:
            pass


class PinBackend(authentication.BaseAuthentication):
    def authenticate(self, request=None, pin=None, phone_number=None):
        if not pin or not phone_number:
            return None
        try:
            che = User.objects.get(phone_number=phone_number)
            if che.check_pin(pin) is True:
                return che
        except User.DoesNotExist:
            pass


class AdminBackend(authentication.BaseAuthentication):
    def authenticate(self, request, email=None, password=None):
        if email and password:
            try:
                user = User.objects.get(email=email)
                if user.check_password(password) is True:
                    return user
            except User.DoesNotExist:
                return None
        return None
