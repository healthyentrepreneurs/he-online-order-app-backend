from rest_framework.response import Response
from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination,
)
from django.core.paginator import Paginator as DjangoPaginator


class OdooPaginator(DjangoPaginator):
    _count = None

    def _get_count(self):
        if self._count is None:
            try:
                self._count = self.object_list.count
            except (AttributeError, TypeError):
                self._count = len(self.object_list)
        return self._count

    count = property(_get_count)


class PostLimitOffsetPagination(PageNumberPagination):
    page_size_query_param = "limit"


class OdooPagination(PostLimitOffsetPagination):
    page = None
    django_paginator_class = OdooPaginator

    def get_pagination_params(self):
        assert (
            self.page is not None
        ), "paginate_queryset must be called to use `get_pagination_params()`"
        offset = (self.page.number - 1) * self.page_size
        return {"limit": self.page_size, "offset": offset}

    def paginate_queryset(self, queryset, request, view=None):
        self.page_size = self.get_page_size(request)
        return super().paginate_queryset(queryset, request, view=None)
