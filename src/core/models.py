from django.db import models
from .managers import SoftDeletionManager
from django.utils import timezone


class SoftDeletionModel(models.Model):
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True

    def delete(self, using=None, **kwargs):
        self.deleted_at = timezone.now()
        self.save()

    def hard_delete(self, using=None):
        super(SoftDeletionModel, self).delete()


class TimestampedModel(models.Model):
    """A timestamp representing when this object was created/updated."""

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """Class Meta."""

        abstract = True

        # By default, any model that inherits from `TimestampedModel` should
        # be ordered in reverse-chronological order. We can override this on a
        # per-model basis as needed, but reverse-chronological is a good
        # default ordering for most models.
        ordering = ["-created_at", "-updated_at"]
