from rest_framework import filters
from django.utils.encoding import force_str
from datetime import datetime

from rest_framework.compat import coreapi, coreschema


class OdooSearchFilter(filters.SearchFilter):
    lookup_suffix = {
        "eq": "=",
        "ne": "!=",
        "gt": ">",
        "gte": ">=",
        "lt": "<",
        "lte": "<=",
        "like": "like",
        "nlike": "not like",
        "ilike": "ilike",
        "nilike": "not ilike",
        "in": "in",
        "nin": "not in",
    }

    DEFAULT_OPERATOR = "ilike"

    def filter_queryset(self, request, queryset, view):
        search_fields = self.get_search_fields(view, request)
        search_terms = self.get_search_terms(request)

        if not search_fields or not search_terms:
            return queryset

        queries = []
        for search_term in search_terms:
            orm_lookups = [
                queries.append(self.construct_search(str(search_field), search_term))
                for search_field in search_fields
            ]
        #  add and ops
        ops = ["|"] * (len(queries) - 1)
        ops.extend(queries)
        return ops

    def construct_search(self, field_name, value):
        field_name_parts = field_name.split("__")
        lookup = self.lookup_suffix.get(field_name_parts[-1])
        if lookup:
            field_name = field_name_parts[0]
        else:
            lookup = self.DEFAULT_OPERATOR
        return (field_name, lookup, value)


class OdooParameterFilter(OdooSearchFilter):
    SEP = "__"
    TYPE_SEP = "@"
    JOIN = "."
    DEFAULT_OPERATOR = "="

    type_suffix = {
        "bol": coreschema.Boolean,
        "str": coreschema.String,
        "int": coreschema.Integer,
        "datetime": coreschema.String,
    }
    DEFAULT_TYPE = coreschema.String

    def filter_queryset(self, request, queryset, view):
        filter_fields = self.get_filter_fields(view)
        filter_terms = self.get_filter_terms(request)
        if not filter_fields or not filter_terms:
            return queryset
        queries = []
        for filter_field in filter_fields:
            field_type_str, _, param_name = self.get_field_type(filter_field)
            raw_value = filter_terms.get(param_name)
            if raw_value:
                odoo_param_name = self.get_odoo_param_name(param_name)
                value = self.format_value(raw_value, field_type_str)
                queries.append(self.construct_search(odoo_param_name, value))
        #  add and ops
        ops = ["&"] * (len(queries) - 1)
        ops.extend(queries)
        return ops

    def format_value(self, value, field_type):
        type_suffix = {
            "bol": lambda val: val.lower() == "true",
            "str": str,
            "int": lambda val: int(val) if val.isdigit() else 0,
            "datetime": lambda val: val,
        }
        return type_suffix.get(field_type)(value)

    def get_filter_fields(self, view):
        """
        Filter fields are obtained from the view
        """
        return getattr(view, "filter_fields", None)

    def get_filter_terms(self, request):
        return request.query_params

    @classmethod
    def get_field_type(cls, field_name):
        field_name_parts = field_name.split(cls.TYPE_SEP)
        field_type_str = field_name_parts[0]
        formatted_field_name = cls.get_param_name(field_name_parts[1])
        field_type = cls.type_suffix.get(field_type_str, cls.DEFAULT_TYPE)
        return (field_type_str, field_type, formatted_field_name)

    @classmethod
    def get_param_name(cls, filter_field):
        return cls.SEP.join(filter_field.split(cls.JOIN))

    @classmethod
    def get_odoo_param_name(cls, filter_field):
        # get of the operator if any
        field_parts = filter_field.split(cls.SEP)
        operator = field_parts[-1]
        name_parts = field_parts[:-1]
        if operator not in cls.lookup_suffix.keys():
            operator = "eq"
            name_parts = field_parts
        name = cls.JOIN.join(name_parts)
        return f"{name}__{operator}"

    def get_schema_fields(self, view):
        assert (
            coreapi is not None
        ), "coreapi must be installed to use `get_schema_fields()`"
        assert (
            coreschema is not None
        ), "coreschema must be installed to use `get_schema_fields()`"

        filter_fields = self.get_filter_fields(view)
        core_api_fields = []
        for field in filter_fields:
            _, field_type, name = self.get_field_type(field)
            core_api_fields.append(
                coreapi.Field(
                    name=name,
                    required=False,
                    location="query",
                    schema=field_type(
                        title=force_str(name), description=force_str(name)
                    ),
                )
            )
        return core_api_fields
