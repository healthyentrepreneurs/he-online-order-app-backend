# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings
from urllib.parse import urlparse

import os
import redis
from .he_odoo import HeOdoo
from config.celery import app as celery_app

import psycopg2

DATABASE_URL = os.getenv("DATABASE_URL")


__all__ = ("celery_app",)


def set_redis_instance():
    try:
        url = os.environ.get("REDISCLOUD_URL", None)
        print("\n\nurl--------", url)
        if url:
            url = urlparse(url)
            redis_cache = redis.Redis(
                host=url.hostname, port=url.port, password=url.password
            )
        else:
            redis_cache = redis.Redis(host="localhost", port=6379)
        setattr(settings, "redis_cache", redis_cache)
        print(settings)
    except Exception as e:
        print("\n\n\n\n\n !!!!!! Failed to attach redis instance to settings.", e)
        raise


def set_odoo_client():
    config = getattr(settings, "ODOO_HOST", False)
    print("\n\n\n\n setting odoo_client")
    try:
        set_client(config)
    except Exception as e:
        print("Unable to connect to a running Odoo server.", e)
        raise


def access_mapper():
    mapper = {}

    def load_mapper(mapper=mapper):
        if not mapper:
            conn = psycopg2.connect(DATABASE_URL)
            cursor = conn.cursor()
            query = cursor.execute("SELECT * FROM user_country;")
            countries = cursor.fetchall()
            print(countries)
            for country in countries:
                country_name = (country[8] or "").lower()
                print(f"\n\n\n\n-------+++++{country_name}")
                if "uganda" in country_name:
                    mapper[country[4]] = (
                        os.environ.get("APP_USER_UG"),
                        os.environ.get("APP_USERPASS_UG"),
                    )
                elif "burundi" in country_name:
                    mapper[country[4]] = (
                        os.environ.get("APP_USER_BU"),
                        os.environ.get("APP_USERPASS_BU"),
                    )
                elif "kenya" in country_name:
                    mapper[country[4]] = (
                        os.environ.get("APP_USER_KY"),
                        os.environ.get("APP_USERPASS_KY"),
                    )
                elif "nigeria" in country_name:
                    mapper[country[4]] = (
                        os.environ.get("APP_USER_NG"),
                        os.environ.get("APP_USERPASS_NG"),
                    )
            print(mapper)
            cursor.close()
            conn.close()
        return mapper

    return load_mapper


mapper = access_mapper()
print(f"\n\n\n\n mapper here {mapper}")

access_obj = {
    1: (os.environ.get("APP_USER_UG"), os.environ.get("APP_USERPASS_UG")),
    2: (os.environ.get("APP_USER_KY"), os.environ.get("APP_USERPASS_KY")),
    4: (os.environ.get("APP_USER_BU"), os.environ.get("APP_USERPASS_BU")),
    72: (os.environ.get("APP_USER_NG"), os.environ.get("APP_USERPASS_NG")),
}


def set_client(config):
    odoo_clients = {}
    # access_obj = mapper()
    print(f"\n\n\n\n mapper here {access_obj}")

    for odoo_id, access in access_obj.items():
        print(f"creating client for {access[0]}")
        username, password = access

        client = HeOdoo(
            config,
            username,
            password,
            context={"lang": settings.LANGUAGE_CODE, "company_id": odoo_id},
        )

        odoo_clients[odoo_id] = client
    settings.deferred_m2o = {}
    settings.deferred_o2m = {}
    settings.odoo = odoo_clients


if os.environ.get("DJANGO_SETTINGS_MODULE") != "config.settings.test":
    set_odoo_client()
    set_redis_instance()
