import logging

from django.conf import settings
from django.core.cache import caches
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.authentication import SessionAuthentication
from rest_framework import exceptions
from django.db import transaction
from src.djangodoo.utils import switch_country
from src.utils.helpers import Helpers

logger = logging.getLogger(__name__)


class OdooAuthentication(JWTAuthentication):
    @transaction.atomic
    def authenticate(self, request):
        user_auth_tuple = super().authenticate(request)
        return set_odoo_client(user_auth_tuple)


class OdooSessionAuthentication(SessionAuthentication):
    @transaction.atomic
    def authenticate(self, request):
        user_auth_tuple = super().authenticate(request)
        return set_odoo_client(user_auth_tuple)


def set_odoo_client(user_auth_tuple):
    if user_auth_tuple is None:
        return None

    user, _ = user_auth_tuple
    try:
        if company := user.company_id:
            user.odoo_client = switch_country(company.odoo_id)
        else:
            raise exceptions.AuthenticationFailed("User is not attached to any country")

    except Exception as error:
        logger.error("odoo auth failed")
        print(error)
        raise exceptions.AuthenticationFailed(
            "Authentication failed, please try again"
        ) from error

    return user, None
