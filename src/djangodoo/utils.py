from django.conf import settings
from django.views.decorators.cache import cache_page
from src.utils.helpers import Helpers

from functools import wraps


def switch_country(company_id):
    clients = settings.odoo or {}
    print(f"getting clinent for ------>{company_id}----{clients}")
    try:
        return clients.get(company_id)

    except Exception as error:
        print(error, "++++++++> odoo clients", clients)


def vary_per_company(timeout):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            company = "not_auth"
            if request.user.is_authenticated:
                company = request.user.company_id
                request.META["x_company_id"] = company.odoo_id
            return cache_page(timeout, key_prefix="_company_{}_".format(company))(
                view_func
            )(request, *args, **kwargs)

        return _wrapped_view

    return decorator


def vary_per_training(timeout):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            training = "not_auth"
            if request.user.is_authenticated and request.user.trainings:
                training = request.user.trainings
                request.META["training"] = training
            return cache_page(timeout, key_prefix="_training_{}_".format(training))(
                view_func
            )(request, *args, **kwargs)

        return _wrapped_view

    return decorator


def get_or_none(cls, odoo_id):
    if odoo_id is None:
        return None
    try:
        return cls.objects.get(odoo_id=odoo_id)
    except cls.DoesNotExist:
        return None


def maybe_check_by_email_or_phone(obj, cls, args):
    if cls._odoo_model == "res.partner":
        obj = (
            obj
            or cls.objects.filter(email=args["email"]).first()
            or cls.objects.filter(phone_number=args["phone_number"]).first()
        )
    return obj
