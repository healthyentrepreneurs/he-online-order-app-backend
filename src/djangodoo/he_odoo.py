import requests


class HeOdoo:
    def __init__(self, config, user, password, context=None) -> None:
        self.config = config
        self.user = user
        self.password = password
        self.base_url = config["HOST"]
        self.db = config["DB"]
        self.port = config["PORT"]
        self.client = self.login()
        self.context = context or {}

    def login(self):
        s = requests.Session()
        req = s.post(
            f"{self.base_url}/web/session/authenticate",
            json={
                "jsonrpc": "2.0",
                "params": {
                    "db": self.db,
                    "login": self.user,
                    "password": self.password,
                },
            },
        )
        req.raise_for_status()
        print("\n\n\n\n--after login--", req.json())
        self.client = s
        return s

    def get_client(self):
        return self.client

    def get(self, model, method, params=None):
        return self.call(model, method, params)

    def get_by_ids(self, model, ids, fields=None):
        params = {
            "model": model,
            "method": "read",
            "args": [ids, fields] if fields else [ids],
            "kwargs": {},
        }
        return self.call(model, "read", params)

    def unlink(self, model, ids):
        params = {
            "model": model,
            "method": "unlink",
            "args": [ids],
            "kwargs": {},
        }
        return self.call(model, "unlink", params)

    def write(self, model, id, data):
        params = {
            "model": model,
            "method": "write",
            "args": [id, data],
            "kwargs": {},
        }
        return self.call(model, "write", params)

    def create(self, model, params, context=None):
        params = {
            "model": model,
            "method": "create",
            "args": [params],
            "kwargs": {"context": context} if context else {},
        }
        return self.call(model, "create", params)

    def search_count(self, model, domain, offset=0, limit=0, order=""):
        params = {
            "model": model,
            "method": "search_count",
            "args": [],
            "kwargs": {"domain": domain},
        }
        return self.call(model, "search_count", params)

    def action_confirm(self, model, id):
        params = {
            "model": model,
            "method": "action_confirm",
            "args": [id],
            "kwargs": {},
        }
        return self.call(model, "action_confirm", params)

    def set_route(self, order_id):
        params = {
            "model": "sale.order",
            "method": "set_route",
            "args": [order_id],
            "kwargs": {},
        }
        return self.call("sale.order", "set_route", params)

    def action_post(self, model, id):
        params = {
            "model": model,
            "method": "action_post",
            "args": [id],
            "kwargs": {},
        }
        return self.call(model, "action_post", params)

    def force_lines_to_invoice_policy_order(self, id):
        params = {
            "model": "sale.order",
            "method": "force_lines_to_invoice_policy_order",
            "args": [id],
            "kwargs": {},
        }
        return self.call("sale.order", "force_lines_to_invoice_policy_order", params)

    def match_payment(self, payment_id, invoice_id):
        params = {
            "model": "account.move",
            "method": "match_payment",
            "args": [payment_id],
            "kwargs": {
                "payment_id": payment_id,
                "invoice_id": invoice_id,
            },
        }
        return self.call("account.move", "match_payment", params)

    def action_invoice_create(self, model, order_id):
        params = {
            "model": model,
            "method": "create",
            "args": [
                {
                    "sale_order_ids": [order_id],
                    "deduct_down_payments": False,
                    "advance_payment_method": "delivered",
                    "date_end_invoice_timesheet": False,
                    "date_start_invoice_timesheet": False,
                    "fixed_amount": 0,
                    "amount": 100,
                    "has_down_payments": False,
                    # "from_product_app": True,
                    # "ref": f"heapp-invoice/{order_id}",
                }
            ],
            "kwargs": {
                "context": {
                    "default_advance_payment_method": "delivered",
                    "allowed_company_ids": [self.context.get("company_id")],
                    "active_model": "sale.order",
                    "active_id": order_id,
                    "active_ids": [order_id],
                }
            },
        }
        return self.call(model, "create", params)

    def search_read(self, model, domain, fields=None, offset=0, limit=None, order=None):
        print(domain)
        if fields is None:
            fields = []
        params = {
            "model": model,
            "method": "search_read",
            "args": [],
            "kwargs": {
                "domain": domain,
                "fields": fields,
                "limit": limit,
                "offset": offset,
                "order": order,
            },
        }
        return self.call(model, "search_read", params)

    def call_action(self, model, method, id):
        params = {
            "model": model,
            "method": method,
            "args": [id],
            "kwargs": {},
        }

        return self.call(model, method, params)

    def call(self, model, method, params):
        req = self.client.post(
            f"{self.base_url}/web/dataset/call_kw/{model}/{method}",
            json={"jsonrpc": "2.0", "params": params or {}, "context": self.context},
        )
        req.raise_for_status()
        data = req.json()
        if error := data.get("error"):
            if error["code"] == 100:
                # session expired
                print("\n\n\n\n\nsession expired--- renewing session", error)
                self.login()
                return self.call(model, method, params)

        print(
            f"x herere--{model}----{method}-{params}---\n\n\n-{data if 'error' in data else {}}-\n\n\n\n\n\nn"
        )
        return data["result"] if "result" in data else data
