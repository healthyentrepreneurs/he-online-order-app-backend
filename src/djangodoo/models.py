# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.core.cache import caches
from .utils import get_or_none, maybe_check_by_email_or_phone
from src.utils.helpers import Helpers


search_read = "search_read"
search_count = "search_count"


class OdooModel(models.Model):
    """Model of a Odoo object copied in Django

    Attributes:
        _odoo_model: name of the Odoo model that will be copied in Django
        _odoo_fields: list of field names that will be copied from Odoo. If None, all field are copied.
        _odoo_ignore_fields: list of field names that will NOT be copied from Odoo
    """

    _odoo_model = None
    _odoo_fields = None
    _odoo_ignore_fields = None

    odoo_id = models.IntegerField(unique=True, null=True, blank=True)

    class Meta:
        abstract = True

    @classmethod
    def _get_odoo_fields(cls):
        res = cls._odoo_fields or settings.odoo[1].get(cls._odoo_model, "fields_get")
        return [f for f in res if f not in (cls._odoo_ignore_fields or [])]

    @classmethod
    def convert_data(cls, data, name):
        if "user.User.training" in str(name):
            return data
        if data and isinstance(data, (list, tuple)) and len(data) == 2:
            model = name.remote_field.model.objects.get(odoo_id=data[0])
            return model
        return data

    @classmethod
    def odoo_get_by_id(cls, odoo_id, client=None):
        def update_or_create(args):
            try:
                obj = cls.objects.get(odoo_id=args["odoo_id"])
                for k, v in args.items():
                    setattr(obj, k, v)
            except Exception:
                obj = cls(**args)
            obj.save()
            return obj

        client = client or settings.odoo[1]
        odoo_model = cls._odoo_model
        odoo_fields = cls._get_odoo_fields()
        rec = client.get_by_ids(odoo_model, [odoo_id], fields=odoo_fields)
        if not rec:
            return {}
        args = {}
        args["odoo_id"] = rec.get("id")
        for field in cls._meta.fields:
            if field.name in rec:
                args[field.name] = cls.convert_data(rec[field.name])
        obj = update_or_create(args)
        return args

    @classmethod
    def odoo_get(cls, odoo_id, client=None, fields=None):
        if fields is None:
            fields = []
        return cls.odoo_list([int(odoo_id)], client=client, fields=fields)

    @classmethod
    def odoo_list(cls, odoo_ids, client=None, fields=None):
        if fields is None:
            fields = []
        odoo_model = cls._odoo_model
        odoo_fields = fields or cls._get_odoo_fields()
        client = client or settings.odoo[1]
        return client.get_by_ids(odoo_model, odoo_ids, fields=odoo_fields)

    @classmethod
    def odoo_fetch(cls, domain, client=None, limit=None, offset=0, **kwargs):
        client = client or settings.odoo[1]
        odoo_fields = cls._get_odoo_fields()
        fields = kwargs.get("fields", None)
        sort_order = kwargs.get("order", None)
        arguments = {
            "limit": limit,
            "offset": offset,
            "order": sort_order,
            "fields": fields or odoo_fields,
        }
        results = lambda: client.search_read(
            cls._odoo_model, tuple(domain), **arguments
        )
        return Helpers.call_with_retries(results)

    @staticmethod
    def fetch(model, domain, fields=None, client=None, limit=None, offset=0):
        if fields is None:
            fields = []
        client = client or settings.odoo[1]
        return client.search_read(
            model,
            tuple(domain),
            **{"limit": limit, "offset": offset, "fields": fields},
        )

    @staticmethod
    def fetch_by_ids(model, ids, fields=None, client=None):
        if fields is None:
            fields = []
        client = client or settings.odoo[1]
        return client.get_by_ids(model, ids, fields=fields)

    @classmethod
    def odoo_count(cls, domain, client=None):
        client = client or settings.odoo[1]
        return client.search_count(cls._odoo_model, domain)

    @classmethod
    def odoo_create_or_update(cls, record, client=None):
        def update_or_create(args):
            obj = get_or_none(cls, args["odoo_id"])

            obj = maybe_check_by_email_or_phone(obj, cls, args)

            if obj is None:
                obj = cls(**args)
            else:
                for k, v in args.items():
                    setattr(obj, k, v)
                if isinstance(obj, User):
                    setattr(obj, "is_active", obj.is_active)

            obj.save()
            return obj

        # odoo_model = cls._odoo_model
        # odoo_fields = cls._get_odoo_fields()
        # client = client or settings.odoo[1]
        args = {}
        args["odoo_id"] = record.get("id")
        del record["id"]
        for field in cls._meta.fields:
            if field.name in record:
                args[field.name] = cls.convert_data(record[field.name], field)
        return update_or_create(args)
