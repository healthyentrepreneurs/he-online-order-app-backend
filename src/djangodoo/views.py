from rest_framework import viewsets
from rest_framework.exceptions import NotFound, ParseError
from rest_framework.response import Response
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_headers
from src.djangodoo.utils import vary_per_company, vary_per_training
from django.utils.decorators import method_decorator
from django.conf import settings
from rest_framework import status
from src.utils.helpers import Helpers
from django.utils.translation import gettext as _


class OdooObjectViewSet(viewsets.ModelViewSet):
    _model = None

    def get_queryset(self):
        return []

    def get_model(self):
        assert self._model is not None, "Model must be defined"
        return self._model

    def filter_queryset(self, queryset):
        # add this every after 2 searches
        domain = []
        for index, backend in enumerate(list(self.filter_backends)):
            sub_domain = backend().filter_queryset(self.request, queryset, self)
            if sub_domain and domain and index > 0:
                domain.insert(0, "&")
            domain.extend(sub_domain)
        return domain

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_pagination_params(self):
        has_pagination_params = hasattr(self.paginator, "get_pagination_params")
        return (
            {} if not has_pagination_params else self.paginator.get_pagination_params()
        )

    def get_count(self, domain):
        client = self.get_odoo_client()
        print("\n\n\n\nclient--get_count->", client)
        get_count = lambda: self.get_model().odoo_count(domain, client=client)

        return Helpers.call_with_retries(get_count)

    # @method_decorator(vary_per_company(60 * 60))
    # @method_decorator(vary_per_training(60 * 60))
    # @method_decorator(vary_on_headers("Bearer", "Cookie", "x_company_id", "training"))
    def list(self, request, *args, **kwargs):
        try:
            domain = self.filter_queryset(self.get_queryset())
            client = self.get_odoo_client()

            class OdooCollection(list):
                def __init__(self, iterable=None, count=None):
                    if iterable is None:
                        iterable = []
                    super().__init__(iterable)
                    self.count = count

            try:
                queryset = OdooCollection(count=self.get_count(domain))
            except Exception as error:
                print(error)
                raise ParseError("Invalid query parameters in the url", 400) from error
            page = self.paginate_queryset(queryset)
            get_data = lambda: self.get_model().odoo_fetch(
                domain, client=client, **self.get_pagination_params()
            )

            data = Helpers.call_with_retries(get_data)
            serializer = self.get_serializer(data, many=True)
            if page is not None:
                return self.get_paginated_response(serializer.data)
            data = serializer.data
            if page is None:
                data = dict(results=data)
            return Response(data)
        except Exception as e:
            print(Helpers.stacktrace(e, "-----odoo list error"))
            return Response(
                {"error": _("Failed to fetch from odoo.")},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    def get_odoo_client(self):
        return (
            self.request.user.odoo_client
            or settings.odoo[self.request.user.company_id.odoo_id]
        )

    def get_object(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        value = self.kwargs[lookup_url_kwarg]
        try:
            obj = self.get_model().odoo_get(
                odoo_id=value, client=self.get_odoo_client()
            )
        except Exception as error:
            raise NotFound() from error
        if not len(obj):
            raise NotFound()
        return obj[0]
