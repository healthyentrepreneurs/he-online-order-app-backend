from django.conf import settings
from datetime import datetime, timedelta
import jwt


class AccountActivationTokenGenerator:
    def make_token(self, user):
        payload = {
            "user_id": user.pk,
            "exp": datetime.utcnow()
            + timedelta(seconds=settings.PIN_JWT_EXP_DELTA_SECONDS),
        }
        encoded_data = jwt.encode(
            payload, settings.SECRET_KEY, settings.PIN_JWT_ALGORITHM
        )
        return encoded_data.decode("utf-8")

    def decode_token(self, token):
        try:
            decoded_data = jwt.decode(
                token,
                settings.SECRET_KEY,
                algorithms=[settings.PIN_JWT_ALGORITHM],
            )
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return None

        return decoded_data["user_id"]


account_activation_token = AccountActivationTokenGenerator()
