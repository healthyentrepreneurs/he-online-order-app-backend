import random
import string


def generate_password():
    random_numbers = "".join((random.choice(string.digits) for i in range(3)))
    random_letters_numbers = f"{''.join((random.choice(string.ascii_letters) for i in range(8)))}{random_numbers}"
    special_xters = "".join(random.choice("@$!%*#?&") for _ in range(2))
    return f"{random_letters_numbers}{special_xters}"
