from datetime import datetime, timedelta
import logging
import pytz
from django.conf import settings

from src.products.models import Product, Category
from fcm_django.api.rest_framework import FCMDevice
from src.utils.tasks import send_notifications
from django_q.models import Schedule, Task
from django_q.tasks import schedule
from django.utils import timezone

from src.user.models import Country, User
from src.djangodoo.utils import switch_country
from src.utils.sync_categories import category_id_domain
from django.utils.translation import gettext as _


import os
import boto3
import base64


s3_client = boto3.client(
    "s3",
    aws_access_key_id=os.environ.get("AWS_ACCESS_KEY_ID"),
    aws_secret_access_key=os.environ.get("AWS_SECRET_ACCESS_KEY"),
)
s3_resource = boto3.resource(
    "s3",
    aws_access_key_id=os.environ.get("AWS_ACCESS_KEY_ID"),
    aws_secret_access_key=os.environ.get("AWS_SECRET_ACCESS_KEY"),
)

bucket_name = "he-products-images"


logger = logging.getLogger(__name__)


def get_updated_products(last_update=None):
    date_check = [
        "|",
        ("write_date", ">", last_update),
        ("create_date", ">", last_update),
    ]
    if not last_update:
        last_update = datetime.now() - timedelta(minutes=7 * 60 * 24)
        date_check = [
            "|",
            ("write_date", ">", last_update),
            ("create_date", ">", last_update),
        ]
    companies = list(
        Country.objects.values_list("odoo_id", "last_update", "country_code", "id")
    )

    new_records = []
    for company in companies:
        company_id, last_update, country_code, id = company
        # temporarily hack to switch current company of the user
        # switch_country(company_id)

        new_records += fetch_data(date_check, company_id)
    if len(new_records) > 0:
        devices = FCMDevice.objects.all()
        send_notifications(
            devices,
            title=_("Products updated"),
            body=_("Products have been updated. Press here to refresh application."),
            products=new_records,
        )
        print(
            "\n\n\n Notification sent---->>>>",
            len(new_records),
            [(r["name"], r["write_date"], r["create_date"]) for r in new_records],
        )
    return new_records


def fetch_data(date_check, company_id):
    domains = []

    category_domains = get_category_domains()  # + date_check
    category_ops = ["|"] * (len(category_domains) - 1)
    category_ops.extend(category_domains)
    category_ops.insert(0, "&")
    category_ops.extend(date_check)

    domains.extend([category_ops])
    print("\n\n\n\ndomains--->>>>", domains)
    data = Product.odoo_fetch(domains, client=switch_country(company_id))
    return data


def get_category_domains():
    categories = Category.objects.filter(view=True).values_list("odoo_id")
    return [category_id_domain(category_id) for category_id in categories]


def create_task(minutes=1440, repeats=-1):
    sche = Schedule.objects.filter(name="check_product_updates").first()
    if sche:
        sche.delete()
    sche = schedule(
        "src.utils.products_sync.get_updated_products",
        None,
        name="check_product_updates",
        schedule_type=Schedule.MINUTES,
        minutes=minutes,
        repeats=repeats,
        next_run=timezone.now() + timedelta(minutes=minutes),
    )
    print("\n\n\n Schedule created---=>", sche)
    return sche


def sync():
    res = []
    domains = []
    failed = []
    category_domains = get_category_domains()
    category_ops = ["|"] * (len(category_domains) - 1)
    category_ops.extend(category_domains)
    domains.extend((category_ops))
    print(domains)

    companies = list(
        Country.objects.values_list("odoo_id", "last_update", "country_code", "id")
    )

    records = []
    for company in companies:
        company_id, last_update, country_code, id = company
        # temporarily hack to switch current company of the user
        # switch_country(company_id)
        records += Product.odoo_fetch(
            domains,
            fields=["id", "image_256"],
            sorted=False,
            client=switch_country(company_id),
        )
    for rec in records:
        image = rec["image_256"]
        product_id = rec["id"]
        try:
            print(f"Uploading image for {product_id}, {str(image)[:6]}")
            upload_image(image, product_id)
            res.append(product_id)
        except Exception as e:
            failed.append(product_id)
            print(f"error occured while uploading image for product: {product_id}, {e}")
    print(f"Images updated for records {len(res)} {res},--- {len(failed)} failed.")


def upload_image(image, odoo_id):
    file_name_with_extention = f"{odoo_id}.jpeg"
    obj = s3_resource.Object(bucket_name, file_name_with_extention)
    obj.put(Body=base64.b64decode(image))
    # get bucket location
    location = s3_client.get_bucket_location(Bucket=bucket_name)["LocationConstraint"]
    # get object url
    object_url = "https://%s.s3-%s.amazonaws.com/%s" % (
        bucket_name,
        location,
        file_name_with_extention,
    )
    print(object_url)
    return object_url
