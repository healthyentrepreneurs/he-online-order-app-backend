from xmlrpc.client import boolean
from django.conf import settings

from datetime import datetime

import operator
from functools import reduce
from django.db.models import Q
from src.user.models import Country, User
from src.accounting.models import PaymentTerms
from src.clusters.models import Clusters
from src.districts.models import Districts
from src.djangodoo.utils import switch_country
from src.trainings.models import Trainings
from src.user.models import UserDefinedDefaults
import json
from django.db.utils import IntegrityError
from .helpers import Helpers


def fetch_data(company_id):
    domain = [
        [
            ["phone", "!=", False],
            # ["customer", "=", True],
            ["company_id", "=", company_id],
        ]
    ]
    print(domain)
    return User.odoo_fetch(domain, client=switch_country(company_id))


def backfill_user_clusters():
    companies = list(
        Country.objects.values_list("odoo_id", "last_update", "country_code", "id")
    )

    failed = []

    for company in companies:
        company_id, last_update, country_code, id = company
        # switch_country(company_id)
        odoo_users = fetch_data(company_id)
        country = Country.objects.filter(id=company_id).first()

        for odoo_user in odoo_users:
            try:
                user = User.objects.get(odoo_id=odoo_user["id"])
                cluster = odoo_user["street"]
                new_cluster = Clusters.objects.filter(name=cluster).first()
                new_district = Districts.objects.filter(name=odoo_user["city"]).first()
                setattr(user, "street", cluster or None)
                if not new_cluster:
                    cluster = Clusters(name=odoo_user["street"], company_id=country)
                    cluster.save()
                    print(f"Saved cluster: {odoo_user['street']} to db")
                if not new_district:
                    district = Districts(name=odoo_user["city"], company_id=country)
                    district.save()
                    print(f"Saved district: {odoo_user['city']} to db")
                user.save()
                print(f"Updated user: {user} cluster: {user.street}")

            except User.DoesNotExist:
                print(f"Odoo user not found in local db: {odoo_user['id']}")

            except Exception as e:
                failed.append(user)
                print("Error occured while updating record: ", e)

    print(f"Total failed: {len(failed)}\n\nFailed: {failed}")


def sync_company_trainings(progress_recorder=None):
    redis_cache = settings.redis_cache

    companies = Country.objects.all()

    failed = []
    companies_len = len(companies)
    last_run = Helpers.get_cached_data(redis_cache, "sync_trainings")

    for index, company in enumerate(companies):
        print(company, "company here--")
        company_id = company.odoo_id
        client = switch_country(company_id)
        domain = ["company_id", "=", company_id]
        trainings = Trainings.odoo_fetch([domain], client=client)
        company_defaults = UserDefinedDefaults.odoo_fetch([domain], client=client)
        old_company_defaults = (
            company.company_defaults
            if isinstance(company.company_defaults, dict)
            else json.loads(company.company_defaults)
        )
        company_payment_terms = old_company_defaults.get("payment_terms", [])
        trainings = [
            {
                "name": training["name"],
                "id": training["id"],
                "temp_product_ids": training["product_ids"],
            }
            for training in trainings
        ]
        trainings_copy = trainings.copy()

        save_trainings(trainings)

        company_defaults = {
            "defaults": [
                {
                    "user_id": (
                        default["user_id"]
                        if isinstance(default["user_id"], bool)
                        else default["user_id"][0]
                    ),
                    "id": default["id"],
                    "company_id": (
                        default["company_id"][0]
                        if isinstance(default["company_id"], list)
                        else default["company_id"].id
                    ),
                    "display_name": default["display_name"],
                    "condition": default["condition"],
                    "json_value": default["json_value"],
                }
                for default in company_defaults
            ],
            "payment_terms": company_payment_terms,
        }
        print(
            f"\n\n\n\n[[]------------company_defaults---------{trainings, company_defaults}-------------\n\n\n\n\n\n"
        )

        trainings = json.dumps(trainings)
        company_defaults = json.dumps(company_defaults)
        print(
            "\n\n\ncompany Trainings & company_defaults syncing--->>>",
            trainings,
            company_defaults,
        )

        try:
            country = Country.objects.get(id=company_id)
            update_che_trainings(company_id, trainings_copy)
            country.trainings = trainings
            country.company_defaults = company_defaults
            country.save(update_fields=["trainings", "company_defaults"])
        except Exception as e:
            print(f"error occured: {e}")
            failed.append([country, trainings, company_defaults])
        if progress_recorder:
            progress_recorder.set_progress(index, companies_len)
    if progress_recorder:
        last_run["failed"] = failed
        last_run["running"] = False
        last_run["last_sync_time"] = str(datetime.now())
        last_run["success"] = True
        redis_cache.set("sync_trainings", bytes(json.dumps(last_run), "utf-8"))
    print(f"Total failed: {len(failed)}\n\nFailed: {failed}")


def save_trainings(trainings):
    for usg in trainings:
        obj = Trainings.objects.filter(odoo_id=usg["id"]).first()
        if obj is None:
            print("\n\n\n obj is None")
            try:
                obj = Trainings(
                    name=usg["name"],
                    odoo_id=usg["id"],
                    temp_product_ids=usg["temp_product_ids"],
                )
                obj.save()
            except IntegrityError:
                print("-----obj already exists---IntegrityError", obj)
                pass
        else:
            print("-----obj already exists", obj)
            obj.name = usg["name"]
            obj.odoo_id = usg["id"]
            obj.temp_product_ids = usg["temp_product_ids"]
            obj.save()

        print(f"\n\n\n\n\nTraining saved-----{obj}")


def update_che_trainings(company_id, trainings):
    from .che_sync import get_trainings

    base_trainings = get_trainings(company_id)
    country_ches = User.objects.filter(Q(company_id=company_id))
    updated_ches = []
    for che in country_ches:
        user_templates = [
            temp["template_id"][0]
            for temp in base_trainings
            if temp["id"] in che.training_ids
        ]
        print("\n\n\n\n\n------user_templates-", user_templates)
        che.trainings = user_templates
        updated_ches.append(che)
    User.objects.bulk_update(updated_ches, fields=["trainings"])

    print(f"\n\n\n\n\n\n----new country_training_ches----{len(country_ches)}")
