import datetime
from dateutil import parser

from django.conf import settings
from src.user.models import Country, User
from src.payment.models import Payment
from src.orders.models import Orders
from src.djangodoo.utils import switch_country

import psycopg2, sys, os

DATABASE_URL = os.getenv("DATABASE_URL")
conn = psycopg2.connect(DATABASE_URL)


def fetch_data(company_id):
    domain = [[["state", "=", "draft"], ["company_id", "=", company_id]]]
    print(domain)
    data = Orders.odoo_fetch(
        domain, do_no_inject=True, client=switch_country(company_id)
    )
    return data


def cleanup_unpaid_quotations():
    companies = list(
        Country.objects.values_list("odoo_id", "last_update", "country_code", "id")
    )

    scanned_quotations = []
    overall_outdated_quotations = []

    for company in companies:
        company_id, last_update, country_code, id = company
        # switch_country(company_id)
        quotations = fetch_data(company_id)
        scanned_quotations.extend(quotation["id"] for quotation in quotations)

        outdated_quotations = [
            rec["id"]
            for rec in quotations
            if parser.parse(rec["create_date"])
            < (datetime.datetime.today() - datetime.timedelta(weeks=1))
        ]
        overall_outdated_quotations.extend(outdated_quotations)
        Orders.delete_quotations(outdated_quotations, client=switch_country(company_id))
        outdated_payments = Payment.objects.filter(
            order_name__in=tuple([f"SO{v}" for v in outdated_quotations])
        )
        for payment in outdated_payments:
            Payment.clear_schedules(payment)
            payment.delete()

        print(
            "\n\n\n outdated_quotations----->>>",
            outdated_quotations,
            "\n\n\n\n outdate payments------->>",
            outdated_payments,
        )
        x = Orders.odoo_list(
            overall_outdated_quotations, fields=["name", "id", "state"]
        )
        print(f"\n\n\n\n\n\n fetching deleted orders:......{x}")
    print(
        f"\n\n\n\n\n\n scanned_quotations ---------->>> {len(scanned_quotations)}, overall deleted: {len(overall_outdated_quotations)}"
    )
