from src.products.models import Category


def sync():
    res = []
    records = Category.odoo_fetch([])
    categs_to_delete = categories_to_delete(records)

    print("\n\n\n\n To be deleted-->", [c.name for c in categs_to_delete])

    for rec in records:
        rec["created_at"] = rec.pop("create_date")
        rec["updated_at"] = rec.pop("write_date")
        category = Category.objects.filter(odoo_id=rec["id"]).first()
        try:
            if category:
                rec.update(view=category.view)
            print(category, rec, end="\n\n")
            res.append(Category.odoo_create_or_update(rec))

        except Exception as error:
            print(rec, str(error), "-error when creating---------->><<<<<<")
            raise error
    print("\n\n\n\n Processed-->", [c.name for c in res])
    [delete_category(c) for c in categs_to_delete]
    print("\n\n\n\n Deleted-->", [c.name for c in categs_to_delete])
    return res


def categories_to_delete(records):
    all_categories = Category.objects.all()
    print([c.name for c in all_categories])
    record_ids = [rec.get("id") for rec in records]
    print(sorted([c.odoo_id for c in all_categories]), sorted(record_ids))
    return [
        category for category in all_categories if category.odoo_id not in record_ids
    ]


def delete_category(obj):
    # nullify odoo_id when deleting to avoild
    # constraint for soft delete
    odoo_id = f"--{obj.odoo_id}"
    obj.odoo_id = None
    obj.name = obj.name + odoo_id
    obj.display_name = obj.display_name + odoo_id
    obj.save()
    obj.delete()


def category_id_domain(category_id):
    if isinstance(category_id, (list, tuple)):
        if len(category_id) == 1:
            return ("categ_id.id", "=", category_id[0])
        else:
            return ("categ_id.id", "in", category_id)
    return ("categ_id.id", "=", category_id)
