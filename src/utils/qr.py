import qrcode
import qrcode.image.svg

import bcrypt

# from django.conf import settings


def make_qr_code(string):
    qr = qrcode.QRCode(image_factory=qrcode.image.svg.SvgPathImage)
    qr.add_data(string)
    qr.make(fit=True)
    return qr.make_image()


def salted_hash(string):
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(bytes(string, "utf-8"), salt)
    return hashed.decode("utf-8")


def check_hash(string, hashed):
    return bcrypt.checkpw(string, hashed)
