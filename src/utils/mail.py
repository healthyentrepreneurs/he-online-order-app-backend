from django.core.mail import send_mail


def send_error_email(subject, message, from_email, to_emails):
    send_mail(
        subject,
        message,
        from_email,
        to_emails,
        fail_silently=False,
    )
