import traceback
import logging
import time, json
import pandas as pd

logger = logging.getLogger(__name__)


class Helpers:
    @classmethod
    def get_cached_data(cls, cache, key):
        data = cache.get(key)
        if data is None:
            return data
        if data == bytes("", "utf-8"):
            cache.delete(key)
            return None
        else:
            return json.loads(data.decode("utf-8"))

    @classmethod
    def merge_dicts_by_id_pandas(cls, list1, list2, id_key="id"):
        df1 = pd.DataFrame(list1).set_index(id_key)
        df2 = pd.DataFrame(list2).set_index(id_key)

        merged_df = pd.concat([df1, df2], axis=1, join="outer", ignore_index=False)
        merged_dicts = merged_df.reset_index().to_dict(orient="records")

        return merged_dicts

    @classmethod
    def stacktrace(cls, error, error_desc):
        logger.info(f"{error_desc} {error}")
        err_string = "".join(
            traceback.TracebackException.from_exception(error).format()
        )
        print(f"{error_desc} {error} {err_string}")
        logger.error(err_string)
        return err_string

    @classmethod
    def call_with_retries(cls, func_name, retries=6):
        print("funciton--->", func_name)
        while retries > 0:
            try:
                return func_name()
            except Exception as e:
                print(e, "-------\n\n error here")
                if retries == 0:
                    break

                if any(x for x in ["request-sent"] if x in str(e).lower()):
                    print(
                        f"\n\n\n\nerror retrying--{func_name}------>",
                        e,
                        end="\n\n\n\n",
                    )
                    time.sleep(0.7)
                    retries -= 1
                    return cls.call_with_retries(func_name, retries=retries)

                raise e
