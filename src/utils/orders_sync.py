from django.conf import settings
from src.user.models import Country, User
from src.djangodoo.utils import switch_country


def fetch_data(company_id):
    domain = [
        # [
        ["phone", "!=", False],
        # ["customer", "=", True],
        ["company_id", "=", company_id],
        # ]
    ]
    print(domain)
    data = User.odoo_fetch(domain, client=switch_country(company_id))
    return data


def backfill_price_list_ids():
    companies = list(
        Country.objects.values_list("odoo_id", "last_update", "country_code", "id")
    )

    failed = []

    for company in companies:
        company_id, last_update, country_code, id = company
        # switch_country(company_id)
        odoo_users = fetch_data(company_id)

        for odoo_user in odoo_users:
            try:
                user = User.objects.get(odoo_id=odoo_user["id"])
                setattr(user, "x_price_list_id", odoo_user["x_price_list_id"][0])
                user.save()
            except User.DoesNotExist:
                print(f"Odoo user not found in local db: {odoo_user['id']}")
            except IndexError:
                print(f"Odoo user has no price list: {odoo_user['x_price_list_id']}")
            except Exception as e:
                failed.append(user)
                print("Error occured while updating record: ", e)

            print(f"Updated user: {user} pricelist id: {user.x_price_list_id}")
    print(f"Total failed: {len(failed)}\n\nFailed: {failed}")
