from fcm_django.api.rest_framework import FCMDevice
from src.utils.tasks import send_notifications
import logging

logger = logging.getLogger(__name__)


def send_promotion_notificaiton(training_id, title, message, refresh):
    try:
        devices = FCMDevice.objects.filter(user__training=training_id)
        send_notifications(
            devices,
            title="Promotions",
            body=message,
            promotion_title=title,
            refresh=refresh,
        )
    except Exception as error:
        print(error)
        logger.error("sending Promotion Notificaiton failed")
