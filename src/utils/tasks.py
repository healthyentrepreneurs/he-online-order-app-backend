import logging
import time

from src.payment.aggregator import AirtelOpenApi, MomoApi, MpesaApi, IhelaClient
from src.payment.models import Payment
from src.user.models import Country, User
from src.djangodoo.utils import switch_country
from fcm_django.api.rest_framework import FCMDevice

logger = logging.getLogger(__name__)


def send_notifications(devices, title="HE Products", body=None, **kwargs):
    results = []
    try:
        response = devices.send_message(
            title=title,
            body=body,
            data={"title": title, "body": body, "extra_kwargs": kwargs},
        )
        results.append(response)
        logger.info(f"\n\n\nadded--notification-response {response}")
        print("\n\n\nadded--notification-response {response}")

    except Exception as error:
        print(error)
        logger.error("sending failed")
    print(f"\n\n\n\n notification responses {results}")
    return results


def send_notification(user, title="HE Products", body=None, **kwargs):
    try:
        devices = FCMDevice.objects.filter(user=user).all()
        for device in devices:
            response = device.send_message(
                data={"title": title, "body": body, "extra_kwargs": kwargs},
            )
            print(
                "device user_id============.>",
                device.user,
                "user_id============",
                user,
                response,
            )
        logger.debug("notification")
    except Exception as error:
        print(error)
        logger.error("sending failed")


def get_transaction_status(ref_no, **kwargs):
    from src.payment.utils import check_payment_created_in_odoo

    trans = Payment.objects.filter(reference_no=ref_no, status="received").first()
    logger.info(f"\n\n\n\n--trans-----  ---{trans}--------\n\n\n\n\n")
    if trans is None:
        logger.info(f"\n\n\n\n--trans-----  ---{trans}----is None----\n\n\n\n\n")
        return True
    payment_in_odoo = check_payment_created_in_odoo(
        trans, switch_country(trans.company_id.odoo_id)
    )
    if payment_in_odoo is None:
        print(
            f"\n\n\n\n\nthis is cget_transaction_status ->>>>>>>--payments not processed yet -{payment_in_odoo}-----------{ref_no}----------->>>>>>>>>\n\n\n\n\n"
        )
        logger.info(
            f"\n\n\n\n--inside-----refno  ---{ref_no}-------kwargs ==={kwargs}-\n\n\n\n\n"
        )
        # RETRIES = 5

        # check_payment_status_sync(trans.provider.name, ref_no, **kwargs)
        logger.info(f"\n\n\n\n--trans----- is not None ---{trans}--------\n\n\n\n\n")
        provider = trans.provider.name
        mapper = {
            "mpesa": MpesaApi(),
            "momo": MomoApi(),
            "airtel": AirtelOpenApi(),
            # "ihela": IhelaClient(),
        }

        logger.info(
            f"\n\n\n\n--inside-----get_transaction_status---{provider}--{mapper[provider]}--++before call -\n\n\n\n\n"
        )
        print(
            f"\n\n\n\n\nthis is cget_transaction_status ->>>>>>>------------{provider}--{mapper[provider]}------------>>>>>>>>>\n\n\n\n\n"
        )

        response, status, trans_id, message = mapper[provider].get_status(
            ref_no, **kwargs
        )
        print(
            f"\n\n\n\n\nmapper[provider].get_status ->>>>>>>------------{response}----response, status, trans_id, message--->>{status} -----{trans_id}---{message}---{provider}------------>>>>>>>>>\n\n\n\n\n"
        )

        logger.info(
            f"\n\n\n\n--inside-----get_transaction_status- after call ---{response}----response, status, trans_id, message--->>{status} -----{trans_id}---{message}---{provider}-\n\n\n\n\n"
        )

        print(status, "=======================>")
        logger.info(f"\n\n\n\n-----status----get_transaction_status-------> {status}")
        if status in ["pending", "Pending"]:
            logger.info(
                f"\n\n\n\n--statuss----- is in [pending, Pending] ---{status}-{provider}-------{ref_no}-------\n\n\n\n\n"
            )
            return
        trans.data = response
        trans.status = status
        trans.message = message
        if trans_id:
            trans.transaction_id = trans_id
        trans.save()
        logger.info(f"\n\n\n\n--trans----- is saved ---{vars(trans)}--------\n\n\n\n\n")
        print(f"\n\n\n\n--trans----- is saved ---{vars(trans)}--------\n\n\n\n\n")
        return trans
    else:
        print(
            f"\n\n\n get_transaction_status-- Payment alrady process {payment_in_odoo}"
        )


def update_companies():
    countries = Country.odoo_fetch([])
    ids = []
    old_ids = []
    for country in countries:
        # odoo_id = country.pop("id")
        odoo_id = country["id"]
        ids.append(odoo_id)
        try:
            obj = Country.objects.get(odoo_id=odoo_id)
        except Exception:
            obj = Country(odoo_id=odoo_id)
        for k, v in country.items():
            if k == "country_id":
                setattr(obj, "name", v[1])
            if k == "name":
                setattr(obj, "company_name", v)
            if k == "currency_id":
                setattr(obj, "currency_id", v[0])
                setattr(obj, "currency", v[1])
                continue
            setattr(obj, k, v)

        obj.save()

    # remove deleted companies
    Country.objects.exclude(odoo_id__in=ids).delete()
