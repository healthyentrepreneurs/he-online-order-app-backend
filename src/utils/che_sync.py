from datetime import datetime, timedelta
import logging
import pytz
from django.conf import settings
from src.user.models import Country, User

from src.djangodoo.utils import switch_country
from django.conf import settings
import json
from .helpers import Helpers


logger = logging.getLogger(__name__)


def get_new_ches(company_id, last_update=None):
    date_check = ("create_date", ">", last_update)
    if not last_update:
        date_check = ["create_date", "<", datetime.now()]
    return fetch_data(company_id, date_check, client=switch_country(company_id))


def get_updated_ches(company_id, last_update=None):
    date_check = ("write_date", ">", last_update)
    if not last_update:
        date_check = ("write_date", "<", datetime.now())
    return fetch_data(company_id, date_check, client=switch_country(company_id))


def get_all_ches(company_id, user_ids, client=None):
    domain = [
        ["phone", "!=", False],
        # ["customer", "=", True],
        ["company_id", "=", company_id],
        ["id", "in", user_ids],
    ]
    return User.odoo_fetch(domain, client=client)


def sync_deleted_users():
    companies = list(
        Country.objects.values_list("odoo_id", "last_update", "country_code", "id")
    )

    deleted_users = []

    for company in companies:
        company_id, last_update, country_code, id = company
        current_users = [
            user
            for user in User.objects.filter(company_id=company_id).all()
            if user.odoo_id is not None
        ]
        current_user_ids = []

        current_user_ids = [
            int(user.odoo_id) for user in current_users if user.odoo_id is not None
        ]
        client = switch_country(company_id)
        odoo_user_ids = get_all_ches(
            company_id, current_user_ids, client=client
        )  # User.odoo_list(current_user_ids, fields=["id"])

        odoo_user_ids = (
            [int(user["id"]) for user in odoo_user_ids]
            if isinstance(odoo_user_ids, list)
            else []
        )

        deleted_in_odoo = [
            user for user in current_user_ids if user not in odoo_user_ids
        ]
        print(
            f"\n\n\n odoo users {odoo_user_ids} ====--\n\n\ncompany_id {company_id} ====\n\n\n\ncurrent_user_ids-local--{current_user_ids}  \n\n\n||||||======{deleted_in_odoo}"
        )

        deleted_users.extend(
            [user for user in current_users if int(user.odoo_id) in deleted_in_odoo]
        )
    for user in deleted_users:
        try:
            user.delete()
        except Exception as e:
            print("error occured while deleting user---===>>", e, user)

    print("deleted users ------>", len(deleted_users))


def fetch_data(company_id, date_check, client=None):
    domain = [
        "&",
        "&",
        "&",
        ["phone", "!=", False],
        ["contact_type", "in", ["customer", "employee"]],
        ["company_id", "=", company_id],
        ["is_company", "=", False],
        # date_check,]
    ]
    print(domain)
    return User.odoo_fetch(domain, client=client)


def get_trainings(company_id):
    arguments = {
        "limit": None,
        "offset": None,
        "order": None,
        "fields": ["id", "template_id"],
    }
    domain = [["company_id", "=", company_id]]
    client = switch_country(company_id)
    if client is None:
        return []
    results = lambda: client.search_read("he.training", tuple(domain), **arguments)
    return Helpers.call_with_retries(results)


def get_company_updates(progress_recorder=None):
    redis_cache = settings.redis_cache
    companies = list(
        Country.objects.values_list(
            "odoo_id", "last_update", "country_code", "id", "company_name"
        )
    )
    companies_len = len(companies)
    last_run = Helpers.get_cached_data(redis_cache, "last_run")
    for index, company in enumerate(companies):
        company_id, last_update, country_code, id, name = company
        last_update = last_update - timedelta(days=2)
        print(last_update, "---------last_update---------", company_id, name, id)
        # temporarily hack to switch current company of the user
        country_obj = Country.objects.filter(odoo_id=company_id).first()
        base_trainings = get_trainings(company_id)

        new_ches = get_new_ches(company_id, last_update=last_update)
        updated_ches = get_updated_ches(company_id, last_update=last_update)
        all_ches = new_ches + updated_ches
        # insert the data into the database
        rec, lr = load_data(all_ches, country_code, country_obj, base_trainings)
        last_run[f"{name}"] = lr
        # update the company last update
        new_last_update = pytz.utc.localize(datetime.utcnow())
        Country.objects.filter(odoo_id=company_id).update(last_update=new_last_update)
        if progress_recorder:
            progress_recorder.set_progress(index, companies_len)

    redis_cache.delete("last_run")
    last_run.update(
        {"last_sync_time": str(datetime.now()), "success": True, "running": False}
    )
    print("\n\n\nlast riun----->", last_run)

    redis_cache.set("last_run", bytes(json.dumps(last_run), "utf-8"))


price_list_mapper = dict([(3, 1), (6, 6), (7, 11), (4, 4)])


def load_data(records, country_code, company, base_trainings):
    res = []
    errs = []
    success = 0
    rec = {}
    for rec in records:
        rec["created_at"] = rec.pop("create_date")
        rec["updated_at"] = rec.pop("write_date")
        try:
            phone = rec.pop("phone")
            phone_number = (
                User.objects.normalize_phone(phone, country_code) if phone else None
            )
            rec["phone_number"] = phone_number
            rec["email"] = User.objects.normalize_email(
                f"{phone_number}@{settings.EMAIL_DOMAIN}"
            )
            name = rec.get("name", "")
            names = name.split(" ", 1)
            if len(names) > 1:
                rec["first_name"], rec["last_name"] = names
            elif len(names) == 1:
                rec["first_name"], rec["last_name"] = names[0], ""

            rec["company_id"] = company
            rec["contact_type"] = rec["contact_type"].lower()
            print(f"\n\n\n\n\n-----+++++-compnay---------{company}-{rec}--------")
            template_ids = []
            # for i in base_trainings:
            [
                template_ids.append(tr["template_id"][0])
                for tr in base_trainings
                if tr["id"] in rec["training_ids"]
            ]

            # training template ids
            rec["trainings"] = template_ids
            rec["x_price_list_id"] = rec["property_product_pricelist"][
                0
            ] or price_list_mapper.get(company.odoo_id)
            # actual product training ids
            print("\n\n\n\n---traiing ids before save", rec["training_ids"])
            res.append(User.odoo_create_or_update(rec))
            success += 1
        except Exception as error:
            rec["email"] = User.objects.normalize_email(
                f'{rec.get("phone_number")}@{settings.EMAIL_DOMAIN}'
            )

            stacktrace = Helpers.stacktrace(error, "-----che sync")
            errs.append(
                dict(
                    error=error,
                    odoo_id=rec.get("id"),
                    phone_number=rec.get("phone_number"),
                    email=rec.get("email"),
                )
            )
            print("error--adding che-rec->", rec, error, stacktrace)
    summary = f"""
        {errs}  <========================total errors:
        company_id: {rec.get("company_id")}
        records: {len(records)}
        errors: {len(errs)}
        success: {success}
    """

    last_run = {
        "errors": [str(err) for err in errs],
        "company_id": str(rec.get("company_id", {})),
        "records": len(records),
        "errors_count": len(errs),
        "success": success,
    }
    print(summary, last_run)

    return res, last_run
