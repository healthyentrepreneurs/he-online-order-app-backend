## customer_name - field to allow search by che/partenr_id
## dependencies: partner_id
## model sale.order
"""
for record in self:
  customer = record["partner_id"]
  name = customer.name
  record["customer_name"] = name
"""

## field: x_stored_order_status
## to allow filtering by order status since filtering requires a stored field
# selection field
## model sale.order


## Unique phone number
# automated action
## model sale.order
"""
if record.phone:
  existing_user_phone = env["res.partner"].search([("id", "!=", record.id),("phone", "=", record.phone)])
  if existing_user_phone:
    raise Warning('A user with phone number {0} already exists.'.format(record.phone))
"""


## Update sale order status
# automated action
## model sale.order
"""
statuses = {
    "invoice": {
        "confirmed_statuses": {"open", "paid", "in payment"},
        "unconfirmed_status": "draft",
        "cancelled_status": "cancel",
    },
    "delivery_order": {
        "not_delivered_statuses": {
            "ready",
            "draft",
            "waiting another operation",
            "cancel",
            "waiting",
        },
        "delivered_status": "done",
        "confirmed_status": "confirmed",
    },
}
for record in self:
    if record:
        result = None
        if record["invoice_count"] >= 1:
            status = record.invoice_ids[0].state.lower()

            if status == statuses["invoice"]["unconfirmed_status"]:
                result = "not confirmed"
            elif status in statuses["invoice"]["confirmed_statuses"]:
                result = "confirmed"
            elif status == statuses["invoice"]["cancelled_status"]:
                result = "cancelled"

        if record["delivery_count"] >= 1:
            status = record.picking_ids[0].state.lower()
            if status in statuses["delivery_order"]["not_delivered_statuses"]:
                if result not in {"confirmed, cancelled"}:
                    result = "not delivered"
            elif status == statuses["delivery_order"]["delivered_status"]:
                result = "delivered"
            elif status == statuses["delivery_order"]["confirmed_status"]:
                result = "confirmed"

    record["x_stored_order_status"] = result or "not confirmed"
"""

# x_price_list_id
# dependency property_product_pricelist
# object relation product.pricelist
# model :Contact
"""
for record in self:
    record['x_price_list_id'] = record['property_product_pricelist'].id
"""

# he.training
# dependency property_product_pricelist
# object relation
# model :Contact


# model: he.training.template
##  with fields
# "__last_update",
# "id",
# "create_date",
# "create_uid",
# "display_name",
# "write_date",
# "training",
# "name",
# "free_products"


# x_he_transactoin_id
## model: account.payment
# transaction id from payment provider
# updating to fix typo -- x_he_transaction_id


# payment_term_id (use the id for one time payment)/(on company)
# curreny_id (on company)
# journal_id (payment provider modal)
# payment_method_id ( on company)
# used when creating an order
# x_price_list_id per user, this is synched with the users
