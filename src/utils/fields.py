from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class CharNullField(models.CharField):
    description = "CharField that stores NULL but returns ''."

    def get_prep_value(self, value):
        if (
            value == ""
        ):  # If Django tries to save an empty string, send the db None (NULL).
            return None
        else:  # Otherwise, just pass the value.
            return value

    def from_db_value(self, value, expression, connection):
        if value is None:
            return ""
        else:
            return value

    def to_python(self, value):
        if isinstance(
            value, models.CharField
        ):  # If an instance, just return the instance.
            return value
        if value is None:  # If db has NULL, convert it to ''.
            return ""
        return value  # Otherwise, just return the value.


class CustomPhoneNumberField(PhoneNumberField, CharNullField):
    pass


class CustomEmailField(models.EmailField):
    def to_python(self, value):
        """
        Convert email to lowercase.
        """
        value = super().to_python(value)
        # Value can be None so check that it's a string before lowercasing.
        if isinstance(value, str):
            return value.lower()
        return value
