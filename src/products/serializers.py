from rest_framework import serializers
from .models import Product, Category


class OrderProducts(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    quantity = serializers.IntegerField(required=True)


class PriceCheckSerializer(serializers.Serializer):
    order_products = serializers.ListField(child=OrderProducts(), required=True)


class ProductSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    created_at = serializers.DateTimeField(source="create_date")
    updated_at = serializers.DateTimeField(source="write_date")
    name = serializers.CharField()
    display_name = serializers.CharField()

    description = serializers.CharField()
    description_sale = serializers.CharField()
    sale_ok = serializers.BooleanField()

    sequence = serializers.IntegerField()
    type = serializers.CharField()

    category = serializers.SerializerMethodField()

    available = serializers.IntegerField(source="qty_available")
    code = serializers.CharField()
    default_code = serializers.CharField()

    currency = serializers.SerializerMethodField()
    company = serializers.SerializerMethodField()

    image = serializers.SerializerMethodField()

    # price = serializers.CharField()
    # sale_avg_price = serializers.CharField()
    lst_price = serializers.CharField()

    def get_category(self, obj):
        return self.serializer_obj(obj["categ_id"])

    def get_image(self, obj):
        return (
            f"https://he-products-images.s3-us-west-2.amazonaws.com/{obj['id']}.jpeg"
            if obj["image_256"]
            else None
        )

    def get_company(self, obj):
        return self.serializer_obj(obj["company_id"])

    def serializer_obj(self, item):
        if item:
            return (
                {"odoo_id": item[0], "name": item[1]}
                if isinstance(item, list)
                else {"odoo_id": item.id, "name": item.name}
            )
        return {"odoo_id": False, "name": False}

    def get_currency(self, obj):
        item = obj.get("currency_id", False)
        return self.serializer_obj(item)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"
