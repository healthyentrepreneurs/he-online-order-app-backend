from django.db import models
from src.core.models import TimestampedModel, SoftDeletionModel
from src.djangodoo.models import OdooModel


class Product(TimestampedModel, SoftDeletionModel, OdooModel):
    _odoo_model = "product.product"
    _odoo_fields = [
        "name",
        "id",
        "list_price",
        "description",
        "active",
        "sale_ok",
        "categ_id",
        "qty_available",
        # "qty_at_date",
        # "currency",
        "code",
        "company_id",
        "create_date",
        "write_date",
        "currency_id",
        "description_sale",
        "display_name",
        # "price",
        # "sale_avg_price",
        # "che_selling_price",
        "image_256",
        "sequence",
        "type",
        # "image",
        "taxes_id",
        "lst_price",
        "default_code",
    ]
    name = models.CharField(max_length=100)

    @classmethod
    def odoo_fetch(cls, domain, client=None, limit=None, offset=0, **kwargs):
        return super().odoo_fetch(
            domain, client, limit, offset, order="name asc", **kwargs
        )

    @classmethod
    def update_product_image(cls, product):
        image_url = None
        if product["image"]:
            image_url = f"https://he-products-images.s3-us-west-2.amazonaws.com/{product['id']}.jpeg"
        product.update(image=image_url)
        return product


class Category(TimestampedModel, SoftDeletionModel, OdooModel):
    _odoo_model = "product.category"
    _odoo_fields = [
        "name",
        "id",
        "display_name",
        "create_date",
        "write_date",
    ]
    name = models.CharField(max_length=100)
    display_name = models.CharField(max_length=100)
    view = models.BooleanField(default=True)


class PriceListRules:
    _odoo_model = "product.pricelist.item"
    _odoo_fields = [
        "pricelist_id",
        "name",
        "price",
        "min_quantity",
        "date_start",
        "date_end",
        "company_id",
        "fixed_price",
        "compute_price",
        "percent_price",
        "price_discount",
        "rule_tip",
        "product_id",
        "price_round",
        "categ_id",
        "applied_on",
        "product_tmpl_id",
    ]
