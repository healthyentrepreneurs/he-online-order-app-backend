from django.contrib import admin
from src.products.models import Product, Category


class ProductAdmin(admin.ModelAdmin):
    pass


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "view")


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
