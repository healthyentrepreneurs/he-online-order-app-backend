from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import ProductsViewSet, CategoryViewSet

router = routers.DefaultRouter()
router.register(r"category", CategoryViewSet)
router.register(r"", ProductsViewSet)

urlpatterns = [
    path(r"", include(router.urls)),
]
