from django.core.management.base import BaseCommand, CommandError

from src.utils.products_sync import sync, get_updated_products, create_task
from src.utils.sync_categories import sync as sync_categories


class Command(BaseCommand):
    help = "Load products data"

    def handle(self, *args, **options):
        try:
            sync_categories()
            create_task()
        except Exception as err:
            # raise err
            raise CommandError('Products sync failed ::\n"%s" ' % str(err))
        self.stdout.write(
            self.style.SUCCESS("Successfully created product updates check task ")
        )
