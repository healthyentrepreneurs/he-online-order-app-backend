from django.core.management.base import BaseCommand, CommandError

from src.utils.products_sync import sync


class Command(BaseCommand):
    help = "Upload products images"

    def handle(self, *args, **options):
        try:
            sync()
        except Exception as err:
            raise err
            raise CommandError('Products images upload failed ::\n"%s" ' % str(err))
        self.stdout.write(self.style.SUCCESS("Successfully upload product images "))
