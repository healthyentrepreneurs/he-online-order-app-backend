import json
from django.conf import settings

from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework.permissions import IsAuthenticated
from src.core.filters import OdooSearchFilter, OdooParameterFilter
from src.core.pagination import OdooPagination
from src.djangodoo.authentication import OdooAuthentication, OdooSessionAuthentication

from .models import Product, Category, PriceListRules
from .serializers import ProductSerializer, CategorySerializer, PriceCheckSerializer
from src.djangodoo.views import OdooObjectViewSet
from src.utils.products_sync import create_task
from src.utils.sync_categories import category_id_domain
from src.utils.helpers import Helpers
from src.trainings.models import Trainings
from rest_framework.decorators import action
from src.utils.helpers import Helpers

from django.utils.translation import gettext as _


from itertools import groupby


class ProductsViewSet(OdooObjectViewSet):
    http_method_names = ["get", "head", "post"]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    pagination_class = OdooPagination
    authentication_classes = [OdooAuthentication, OdooSessionAuthentication]
    filter_backends = [OdooParameterFilter, OdooSearchFilter]
    filter_fields = [
        "int@categ_id.id",
        "bol@sale_ok",
        "str@categ_id.name",
        "int@qty_available__gt",
        "int@qty_available__lt",
    ]
    search_fields = ["name", "description", "default_code"]
    _model = Product

    def get_odoo_client(self):
        return (
            self.request.user.odoo_client
            or settings.odoo[self.request.user.company_id.odoo_id]
        )

    def filter_queryset(self, queryset):
        domains = super().filter_queryset(queryset)
        categories = Category.objects.filter(view=True).values_list("odoo_id")

        category_domains = [
            category_id_domain(category_id) for category_id in categories
        ]
        category_ops = ["|"] * (len(category_domains) - 1)
        category_ops.extend(category_domains)
        if domains:
            domains.insert(0, "&")
        domains.extend(category_ops)

        # add threshold filter
        domains = self.training_templates_domain(self.inject_threshold_domain(domains))

        print(domains, "====>")
        return domains

    def inject_threshold_domain(self, domains):
        company_threshold = self.request.user.company_id.minimum_threshold
        threshold_domain = [["qty_available", ">=", company_threshold]]
        if any(i for i in domains if i[0] == "qty_available"):
            return domains
        if domains:
            domains.insert(0, "&")
        domains.extend(threshold_domain)
        return domains

    def training_templates_domain(self, domains):
        temp_ids = self.request.user.trainings or []
        print(
            "\n\n\n\n\n------===-temp_ids",
            temp_ids,
            self.request.user,
            end="\n\n\n\nbb----",
        )
        training_ids = []
        temps_domain = [["id", "in", []]]
        if temp_ids:
            trs = Trainings.objects.filter(odoo_id__in=temp_ids).all()
            [training_ids.extend(tr.temp_product_ids) for tr in trs]
            prod_ids = training_ids  # tr.temp_product_ids
            temps_domain = [["id", "in", prod_ids]]
            print("\n\n\n--temps_domain-----++++==", temps_domain)

        if domains and temps_domain:
            domains.insert(0, "&")
        if temps_domain:
            domains.extend(temps_domain)
        return domains

    @action(
        methods=["post"],
        detail=False,
        authentication_classes=[OdooAuthentication, OdooSessionAuthentication],
        serializer_class=PriceCheckSerializer,
    )
    def check_prices(self, request):
        data = request.data
        serializer = PriceCheckSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        order_products = data.get("order_products", [])

        product_ids = [odp["id"] for odp in order_products]

        pricelist_id = self.request.user.company_id.pricelist_id or None
        client = self.get_odoo_client()

        updated_products = []

        if pricelist_id is None:
            return Response(
                {
                    "error": _(
                        "no pricelist configured for country, please contact admin for support."
                    )
                },
                status=status.HTTP_404_NOT_FOUND,
            )

        if product_ids:
            price_list = client.get_by_ids(
                "product.pricelist", [pricelist_id], fields=["item_ids"]
            )
            if not price_list:
                return Response(
                    {
                        "error": _(
                            "country configured pricelist not found, please contact admin for support."
                        )
                    },
                    status=status.HTTP_404_NOT_FOUND,
                )
            price_list = price_list[0]
            pricelist_rule_ids = price_list["item_ids"]

            rules = client.get_by_ids(
                PriceListRules._odoo_model,
                pricelist_rule_ids,
                fields=PriceListRules._odoo_fields,
            )
            products = Product.fetch_by_ids(
                "product.product",
                product_ids,
                ["lst_price", "id", "categ_id"],
                client=client,
            )
            # 1_product, 2_product_category, 3_global - applied_on
            # fixed (fixed_price-float), percentage (percent_price-float), formula (price_discount-float %) -- compute price
            discount_type_mapper = {
                "formula": [
                    "price_discount",
                    lambda product_price, discount: product_price
                    * ((100 - discount) / 100),
                ],
                "percentage": [
                    "percent_price",
                    lambda product_price, discount: product_price
                    * ((100 - discount) / 100),
                ],
                "fixed": ["fixed_price", lambda product_price, discount: discount],
            }
            grouped_rules = {
                key: list(group)
                for key, group in groupby(rules, key=lambda rule: rule["applied_on"])
            }

            rules_matcher = {
                "3_global": lambda rule, product: True,
                "2_product_category": lambda rules, product: [
                    rule
                    for rule in rules
                    if product["categ_id"][0] == rule["categ_id"][0]
                ],
                "1_product": lambda rules, product: [
                    rule
                    for rule in rules
                    if product["id"] == rule["product_tmpl_id"][0]
                ],
            }
            products = Helpers.merge_dicts_by_id_pandas(products, order_products)
            for product in products:
                product = run_rules(
                    grouped_rules, product, discount_type_mapper, rules_matcher
                )
                updated_products.append(product)
        return Response({"products": updated_products})


def run_rules(grouped_rules, product, discount_type_mapper, rules_matcher):
    global_rule = "3_global"
    categ_rule = "2_product_category"
    product_rule = "1_product"

    gr = grouped_rules.get(global_rule)
    cr = grouped_rules.get(categ_rule)
    pr = grouped_rules.get(product_rule)
    if gr and rules_matcher[global_rule](gr, product):
        product = run_rule(gr[0], discount_type_mapper, product)
    if cr and rules_matcher[categ_rule](cr, product):
        product = run_rule(
            rules_matcher[categ_rule](cr, product)[0], discount_type_mapper, product
        )
    if pr and rules_matcher[product_rule](pr, product):
        product = run_rule(
            rules_matcher[product_rule](pr, product)[0], discount_type_mapper, product
        )
    return product


def run_rule(current_rule, discount_type_mapper, product):
    if int(product["quantity"]) >= int(current_rule["min_quantity"]):
        action_field = discount_type_mapper[current_rule["compute_price"]][0]
        action = discount_type_mapper[current_rule["compute_price"]][1]
        new_price = action(product["lst_price"], current_rule[action_field])
        if round(new_price) != round(product["lst_price"]):
            product["new_price"] = new_price
    return product


class CategoryViewSet(viewsets.ModelViewSet):
    http_method_names = ["get", "head"]
    queryset = Category.objects.filter(view=True)
    serializer_class = CategorySerializer
