from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django import forms
from .models import User, Country, CountryAccess


class CustomUserAdmin(UserAdmin):
    model = User
    list_display = (
        "email",
        "company_id",
        "is_active",
        "phone_number",
        "name",
        "is_admin",
        "odoo_id",
    )
    list_filter = ("is_admin", "is_active", "pin_set", "is_staff")
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "email",
                    "password",
                    "phone_number",
                    "pin",
                    "pin_set",
                    "company_id",
                    "name",
                    "x_price_list_id",
                    "training_ids",
                )
            },
        ),
        (
            "Permissions",
            {"fields": ("is_staff", "is_active", "is_admin", "is_superuser")},
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "password1",
                    "password2",
                    "is_staff",
                    "is_active",
                    "phone_number",
                    "company_id",
                    "is_admin",
                    "x_price_list_id",
                ),
            },
        ),
    )
    search_fields = ("email", "name")
    ordering = ("email", "company_id")
    readonly_fields = ["pin"]


class CountryAccessInline(admin.ModelAdmin):
    model = CountryAccess
    list_display = ["id", "username"]


class CountryAdmin(admin.ModelAdmin):
    list_display = ("name", "odoo_id", "currency", "company_name", "payments")

    def payments(self, obj):
        return ",\n".join([p.label for p in obj.payment_methods.all()])


admin.site.register(CountryAccess, CountryAccessInline)
admin.site.register(Country, CountryAdmin)

admin.site.register(User, CustomUserAdmin)
