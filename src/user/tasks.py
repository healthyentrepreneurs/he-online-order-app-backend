import json
from celery import shared_task
from src.utils.che_sync import get_company_updates
from celery_progress.backend import ProgressRecorder
from datetime import datetime
from django.conf import settings
from src.utils.helpers import Helpers


@shared_task(bind=True, name="sync_ches")
def sync_ches(self):
    """Syncs che users from odoo to application."""
    redis_cache = settings.redis_cache
    try:
        progress_recorder = ProgressRecorder(self)
        get_company_updates(progress_recorder=progress_recorder)
    except Exception as err:
        print(Helpers.stacktrace(err, "-----odoo list error"))
        redis_cache.delete("last_run")
        last_run = {
            "last_sync_time": str(datetime.now()),
            "success": False,
            "running": False,
            "error": str(err),
        }
        print("\n\n\nlast run----->", last_run)
        redis_cache.set("last_run", bytes(json.dumps(last_run), "utf-8"))
