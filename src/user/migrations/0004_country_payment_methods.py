# Generated by Django 2.2.10 on 2020-10-27 22:56

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("payment", "0004_auto_20201027_2256"),
        ("user", "0003_auto_20201005_0246"),
    ]

    operations = [
        migrations.AddField(
            model_name="country",
            name="payment_methods",
            field=models.ManyToManyField(to="payment.PaymentMethod"),
        ),
    ]
