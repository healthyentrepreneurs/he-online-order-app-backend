# Generated by Django 2.2.10 on 2023-11-20 08:56

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):
    dependencies = [
        ("user", "0028_user_trainings"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="user",
            name="training",
        ),
        migrations.AddField(
            model_name="user",
            name="training_ids",
            field=jsonfield.fields.JSONField(default=[]),
        ),
    ]
