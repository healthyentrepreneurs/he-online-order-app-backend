# Generated by Django 2.2.10 on 2020-12-13 10:38

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("user", "0010_auto_20201209_0824"),
    ]

    operations = [
        migrations.AlterField(
            model_name="tokens",
            name="access_token",
            field=models.CharField(max_length=255, verbose_name="refresh_token"),
        ),
    ]
