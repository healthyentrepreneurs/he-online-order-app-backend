import os, json
from django_q.tasks import schedule
import requests
from src.utils.pincode_token import account_activation_token
from email.mime.image import MIMEImage
from django.db.models import Q
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.translation import gettext as _
from rest_framework import pagination
from rest_framework.exceptions import NotFound
from src.djangodoo.utils import switch_country
from django.http import HttpResponse, FileResponse
from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    ListCreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    UpdateAPIView,
    RetrieveDestroyAPIView,
    GenericAPIView,
)
from fcm_django.api.rest_framework import FCMDevice
from django.urls import resolve

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAuthenticatedOrReadOnly,
    IsAdminUser,
)

from src.utils.tasks import update_companies
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework import status, viewsets, mixins
from rest_framework_simplejwt.views import TokenObtainPairView
from .models import Country
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

from django.conf import settings

from .renderers import UserJSONRenderer
from .serializers import (
    LoginSerializer,
    UserSerializer,
    User,
    TokenRefreshSerializer,
    CHELoginSerializer,
    CHEPinSetSerializer,
    CHELaunchLoginSerializer,
    CHEActivateSerializer,
    ChangePinSerializer,
    LogoutSerializer,
    CHEPasswordSerializer,
    CHECSVCredentialsSerializer,
    CountrySerializer,
    CreateUserSerializer,
    UserUpdateSerializer,
    UserActivateSerializer,
    AdminResetPasswordSerializer,
    MyFCMDeviceSerializer,
    CHEPasswordCSVSerializer,
    CHEUpdateSerializer,
    QRCodeGenerateSerializer,
)
from rest_framework.decorators import action
from ..core.pagination import PostLimitOffsetPagination

from ..utils.genrate_password import generate_password

from ..utils.qr import make_qr_code, check_hash, salted_hash
from src.user.tasks import sync_ches
from dateutil import parser
from src.utils.helpers import Helpers


import csv
from django_q.models import Schedule
from django_q.tasks import schedule


from datetime import datetime, timedelta
from django.utils import timezone

User = get_user_model()


class CheRefreshAPIView(GenericAPIView):
    permission_classes = (IsAdminUser,)

    def get(self, request):
        redis_cache = settings.redis_cache
        last_run = request.GET.get("last_run", None)
        data = Helpers.get_cached_data(redis_cache, "last_run") or {}
        if last_run:
            if data:
                return Response(
                    {"message": "Last Che sync run", "data": data},
                    status=status.HTTP_200_OK,
                )
            else:
                return Response(
                    {
                        "message": "Last Che sync run",
                        "data": {},
                        "message": "No Sync has been run yet",
                    },
                    status=status.HTTP_200_OK,
                )

        try:
            for key in redis_cache.scan_iter("*celery-task*"):
                data = Helpers.get_cached_data(redis_cache, key)

                if isinstance(data, dict):
                    if data["status"] != "PROGRESS":
                        redis_cache.delete(key)
                        print(f"Deleted sync run over 1 day: {data['task_id']}")
            data = Helpers.get_cached_data(redis_cache, "last_run") or {}
            if data.get("running", False) is True:
                return Response(
                    {
                        "message": f"Che sync task already running in background.",
                        "data": data,
                    },
                    status=status.HTTP_200_OK,
                )

            task = sync_ches.delay()
            last_run = {
                "task_id": task.id,
                "running": True,
                "last_sync_time": data.get("last_run", str(datetime.now())),
            }
            redis_cache.set("last_run", bytes(json.dumps(last_run), "utf-8"))
            return Response(
                {"message": f"Refresh started in background", "task_id": task.task_id},
                status=status.HTTP_200_OK,
            )

        except Exception as err:
            print(Helpers.stacktrace(err, "-----uer fetch"))
            redis_cache.delete("last_run")
            last_run = {
                "last_sync_time": str(datetime.now()),
                "success": False,
                "running": False,
                "error": str(err),
            }
            print("\n\n\nlast riun----->", last_run)

            redis_cache.set("last_run", bytes(json.dumps(last_run), "utf-8"))
            return Response(
                {"message": f"An error occured {err} ----{type(err)}"},
                status=status.HTTP_200_OK,
            )


class QRCodeAPIView(GenericAPIView):
    serializer_class = QRCodeGenerateSerializer

    def get_permissions(self):
        if self.request.method == "GET":
            permission_classes = (AllowAny,)
        else:
            permission_classes = (IsAdminUser,)
        return [permission() for permission in permission_classes]

    def post(self, request):
        data = request.data
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.filter(email=data["email"]).first()
        if user is not None:
            hash = salted_hash(serializer.data["email"])
            if user.activate_hash:
                hash = user.activate_hash

            activate_link = f"{settings.FRONT_CHE_END_BASE_URL}/qr_code/{user.odoo_id}/activate?auth_code={hash}"
            img = make_qr_code(activate_link)
            svg = img.to_string().decode("utf-8").replace('"', "'")
            serializer.update(user, dict(activate_hash=hash))
            return Response(
                {"qr_code": svg},
                status=status.HTTP_200_OK,
            )
        return Response(
            {"error": f"Invalid email, please enter a valid HE admin user email!"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    def get(self, request, *args, **kwargs):
        auth_code = request.GET.get("auth_code", None)
        uid = request.GET.get("uid", None)

        user = User.objects.filter(odoo_id=uid).first()

        valid_hash = user.activate_hash == auth_code
        if user and valid_hash:
            if user.is_active == True:
                user.activate_hash = None
                user.save()
                message = {
                    "message": f"User account for {user.email} is already activated and password has already been set, please login to continue."
                }
                return Response(message, status=status.HTTP_200_OK)
            token = account_activation_token.make_token(user)
            user.pin_set = False
            user.is_active = True
            user.activate_hash = None
            user.save()
            message = {
                "message": "qr code activation link verified!",
                "email": user.email,
                "key": token,
            }
            return Response(message, status=status.HTTP_200_OK)
        return Response(
            {"error": _(f"Invalid activation code!")},
            status=status.HTTP_400_BAD_REQUEST,
        )


class TokenRefreshView(CreateAPIView):
    """
    post:
    refresh token.
    """

    permission_classes = (AllowAny,)
    renderer_classes = (UserJSONRenderer,)
    serializer_class = TokenRefreshSerializer


class AdminResetPasswordView(CreateAPIView):
    """
    post:
    Resets password for admin user.
    """

    permission_classes = (AllowAny,)
    serializer_class = AdminResetPasswordSerializer

    def post(self, request):
        data = request.data

        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.filter(email=data["email"], is_admin=True).first()
        if user is not None:
            uid = force_text(urlsafe_base64_encode(user.email.encode("utf8")))
            reset_token = default_token_generator.make_token(user)

            mail_subject = "Reset your Healthy Entrepreneurs Account Password."

            url = f"{settings.FRONT_END_BASE_URL}/reset/password/{uid}/{reset_token}"
            html_content = render_to_string(
                "reset_password.html", {"name": user.name or "", "url": url}
            )
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(
                mail_subject, text_content, settings.DEFAULT_FROM_EMAIL, [user.email]
            )
            msg.attach_alternative(html_content, "text/html")

            img_path = os.path.join(
                os.path.dirname(os.path.abspath(__file__)), "templates/logo.png"
            )
            img_data = open(img_path, "rb").read()
            img = MIMEImage(img_data, "jpeg")

            img.add_header("Content-Id", "<myimage>")
            img.add_header("Content-Disposition", "inline", filename="myimage")
            msg.attach(img)
            try:
                msg.send(fail_silently=False)

            except Exception as e:
                print("error sending email:", e)

            return Response(
                {
                    "message": "Password reset link sent to email, please click on link to continue password reset process."
                },
                status=status.HTTP_200_OK,
            )
        return Response(
            {"error": f"Invalid email, please enter a valid HE admin user email!"},
            status=status.HTTP_400_BAD_REQUEST,
        )


class AccountVerificationAPIView(GenericAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = []

    def get(self, request, uidb64, activation_token):
        try:
            email = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(email=email)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(
            user, activation_token
        ):
            if user.is_active == True:
                message = {
                    "message": "Your account is already activated and password has already been set, please login to continue."
                }
                return Response(message, status=status.HTTP_200_OK)

            message = {"message": "password activation link verified!", "email": email}
            return Response(message, status=status.HTTP_200_OK)

        return Response(
            {"error": _("Activation link is invalid or expired !!")},
            status=status.HTTP_400_BAD_REQUEST,
        )


class ChangePasswordView(UpdateAPIView):
    """
    post:
    change admin user password.
    """

    permission_classes = (AllowAny,)
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer

    def update(self, request, *args, **kwargs):
        data = request.data
        set_password = request.GET.get("set-password", None)
        user_email = request.headers.get("X-USER-EMAIL", None)
        user_token = request.headers.get("X-USER-TOKEN", None)

        if "password" not in data:
            return Response(
                {"error": "Password required to perform password update!"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if set_password:
            if user_email is None:
                return Response(
                    {"error": "Missing header for password update!"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            return handle_set_password(self, user_email, data)
        else:
            if user_token is None:
                return Response(
                    {"error": "Missing header for password update!"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            return handle_password_reset(self, user_token, data)


def handle_set_password(self, user_email, data):
    try:
        user = User.objects.get(email=user_email)
        if user.is_active:
            return Response(
                {
                    "error": "Cannot set password for already activated user, please reset password to update your password."
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        serializer = self.serializer_class(
            data=dict(password=data["password"]), partial=True
        )
        serializer.is_valid(raise_exception=True)

        user_obj = User.objects.filter(email=user_email).first()
        if user_obj is not None:
            data = dict(password=data["password"])
            if not user_obj.is_active:
                data["is_active"] = True
            serializer.update(user_obj, serializer.validate(data))
            return Response(
                {
                    "message": "User password updated succesfully, please login to continue!"
                },
                status=status.HTTP_200_OK,
            )
    except User.DoesNotExist:
        return Response(
            {"error": "Invalid user email!"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def handle_password_reset(self, user_token, data):
    user_token_data = user_token.split("/")
    if len(user_token_data) == 2:
        uidb64, reset_token = user_token_data
        user_email = None
        try:
            email = force_text(urlsafe_base64_decode(uidb64))
            user_email = email
            user = User.objects.get(email=email)
            if not user.is_active:
                return Response(
                    {
                        "error": "User account not active, please ask an admin to activate your account so you can reset your password."
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
            elif default_token_generator.check_token(user, reset_token):
                serializer = self.serializer_class(
                    data=dict(password=data["password"]), partial=True
                )
                serializer.is_valid(raise_exception=True)

                user_obj = User.objects.filter(email=user_email).first()
                if user_obj is not None:
                    data = dict(password=data["password"])
                    serializer.update(user_obj, serializer.validate(data))
                    return Response(
                        {
                            "message": "User password updated succesfully, please login to continue!"
                        },
                        status=status.HTTP_200_OK,
                    )
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            pass
        return Response(
            {"error": "Invalid token for password update!"},
            status=status.HTTP_400_BAD_REQUEST,
        )


class CreateUserView(CreateAPIView):
    """
    post:
    Register a user.
    """

    permission_classes = (IsAdminUser,)
    serializer_class = CreateUserSerializer

    def post(self, request):
        user = request.data

        user["password"] = generate_password()

        if "company_id" in user:
            del user["company_id"]

        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        user_obj = User.objects.filter(email=user["email"]).first()
        if user_obj is None:
            serializer.save()
            user = User.objects.get(email=user["email"])
            uid = force_text(urlsafe_base64_encode(user.email.encode("utf8")))
            activation_token = default_token_generator.make_token(user)

            mail_subject = "Activate your Healthy Entrepreneurs Account."

            url = (
                f"{settings.FRONT_END_BASE_URL}/user/activate/{uid}/{activation_token}"
            )
            html_content = render_to_string(
                "activate_account.html", {"name": user.name or "", "url": url}
            )
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(
                mail_subject, text_content, settings.DEFAULT_FROM_EMAIL, [user.email]
            )
            msg.attach_alternative(html_content, "text/html")

            img_path = os.path.join(
                os.path.dirname(os.path.abspath(__file__)), "templates/logo.png"
            )
            img_data = open(img_path, "rb").read()
            img = MIMEImage(img_data, "jpeg")

            img.add_header("Content-Id", "<myimage>")
            img.add_header("Content-Disposition", "inline", filename="myimage")
            msg.attach(img)
            try:
                msg.send(fail_silently=False)
            except Exception as e:
                print("error sending email:", e)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(
            {"error": f'User {user["email"]} already exists !!'},
            status=status.HTTP_409_CONFLICT,
        )


class AdminLoginAPIView(CreateAPIView):
    """
    post:
    Login a user.
    """

    permission_classes = (AllowAny,)
    renderer_classes = (UserJSONRenderer,)
    serializer_class = LoginSerializer

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserListAPIView(ListAPIView):
    permission_classes = [IsAdminUser]
    serializer_class = UserSerializer
    pagination_class = PostLimitOffsetPagination

    def get_queryset(self, *args, **kwargs):
        queryset_list = User.objects.filter(is_admin=True).all()

        page_size = "page_size"
        if self.request.GET.get(page_size):
            pagination.PageNumberPagination.page_size = self.request.GET.get(page_size)
        else:
            pagination.PageNumberPagination.page_size = 10
        query = self.request.GET.get("q")
        print
        if query:
            queryset_list = queryset_list.filter(
                Q(email__icontains=query) | Q(username__icontains=query)
            )

        return queryset_list.order_by("-id")


class UserCreateAPIView(CreateAPIView):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = User.objects.all()


class UserDetailAPIView(RetrieveAPIView):
    queryset = User.objects.filter(is_admin=True)
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer


class UserDeleteAPIView(RetrieveDestroyAPIView):
    queryset = User.objects.filter(is_admin=True)
    permission_classes = [IsAuthenticated, IsAdminUser]
    serializer_class = UserSerializer

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        super().destroy(request, *args, **kwargs)
        return Response(
            {"message": "User successfully deleted"}, status=status.HTTP_200_OK
        )


class UserUpdateAPIView(UpdateAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        data = request.data

        serializer = self.serializer_class(data=data, partial=True)
        serializer.is_valid(raise_exception=True)

        serializer.update(instance, serializer.validate(data))

        return Response(
            {"msg": "user updated succesfully", "data": serializer.data},
            status=status.HTTP_200_OK,
        )


class CHEUserUpdateAPIView(UpdateAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = User.objects.all()
    serializer_class = CHEUpdateSerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        data = request.data

        serializer = self.serializer_class(data=data, partial=True)
        serializer.is_valid(raise_exception=True)

        serializer.update(instance, serializer.validated_data)

        return Response(
            {
                "msg": "user data updated succesfully",
                "data": serializer.data,
            },
            status=status.HTTP_200_OK,
        )


class CHEActivateView(TokenObtainPairView):
    serializer_class = CHEActivateSerializer


class CHESetPinView(CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = CHEPinSetSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class CHELoginView(TokenObtainPairView):
    serializer_class = CHELoginSerializer


class CHELaunchView(TokenObtainPairView):
    serializer_class = CHELaunchLoginSerializer


class ChangePinView(UpdateAPIView):
    serializer_class = ChangePinSerializer
    model = User
    http_method_names = ["patch"]

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            # Check old pin
            old_pin = serializer.data.get("old_pin")
            new_pin = serializer.data.get("new_pin")
            if not self.object.check_pin(old_pin):
                return Response(
                    {"old_pin": ["Wrong pin."]},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            # set_pin also hashes the pin that the user will get
            self.object.set_pin(new_pin)
            self.object.save()
            return Response(
                {
                    "success": True,
                    "message": "Pin successfully changed",
                },
                status=status.HTTP_200_OK,
            )

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CHEUserViewSet(viewsets.ReadOnlyModelViewSet, mixins.DestroyModelMixin):
    """CHE User view set to retrieve, list and delete ches"""

    permission_classes = [IsAdminUser]
    queryset = User.objects.filter(odoo_id__isnull=False)
    serializer_class = UserSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_fields = ["company_id", "is_active", "city", "street"]
    search_fields = ["name", "phone_number", "odoo_id", "city"]

    def get_permissions(self):
        if self.action == "list":
            return [IsAuthenticated()]
        return super().get_permissions()

    def destroy(self, *args, **kwargs):
        serializer = self.get_serializer(self.get_object())
        super().destroy(*args, **kwargs)
        return Response(
            {"message": "User successfully deleted"}, status=status.HTTP_200_OK
        )

    def get_odoo_client(self):
        return switch_country(self.request.user.company_id.odoo_id)

    def list(request, *args, **kwargs):
        try:
            return super().list(request, *args, **kwargs)

        except Exception as e:
            print(Helpers.stacktrace(e, "-----uer fetch"))

    @action(
        methods=["post"],
        detail=True,
        permission_classes=[IsAdminUser],
        serializer_class=CHEPasswordSerializer,
    )
    def generate_password(self, request, pk=None):
        """Generate a password for a che user"""
        instance = self.get_object()
        password = User.objects.make_random_password()
        instance.set_password(password)
        # activate account
        instance.is_active = True
        instance.pin_set = False
        instance.save()
        serializer = self.get_serializer(
            {"password": password, "phone_number": instance.phone_number}
        )
        print(
            f"\n\n\n\n-----------generate_password------------{instance} {instance.name}"
        )
        return Response(serializer.data, status.HTTP_200_OK)

    @action(
        methods=["post"],
        detail=False,
        permission_classes=[IsAdminUser],
        serializer_class=CHEPasswordCSVSerializer,
    )
    def generate_passwords(self, request, *args, **kwargs):
        """Generate a password for che users"""
        user_ids = request.data.get("che_user_ids", None)
        csv_data = []

        if user_ids:
            ches = [che for che in self.get_queryset() if che.odoo_id in set(user_ids)]
            if not ches:
                return Response(
                    {"error": "no che users found for the ids provided."},
                    status.HTTP_400_BAD_REQUEST,
                )
            for che in ches:
                password = User.objects.make_random_password()
                che.set_password(password)
                che.is_active = True
                che.pin_set = False
                che.save()
                serializer = CHECSVCredentialsSerializer(
                    {
                        "password": password,
                        "phone_number": che.phone_number,
                        "name": che.name,
                        "district": che.city,
                        "cluster": che.street,
                    }
                )
                csv_data.append(serializer.data)

            print(
                f"\n\n\n\n-----------generate_passwords--multiple users----------{csv_data}"
            )
            if csv_data:
                response = HttpResponse(
                    content_type="text/csv",
                )
                response["Content-Disposition"] = (
                    'attachment; filename="che-user-passwords.csv"'
                )
                writer = csv.writer(response)
                writer.writerow(
                    ["name", "phone_number", "password", "district", "cluster"]
                )
                for record in csv_data:
                    writer.writerow(
                        [
                            record["name"],
                            record["phone_number"],
                            record["password"],
                            record["district"],
                            record["cluster"],
                        ]
                    )
                return response
        return Response(
            {"error": "che_user_ids not provided."}, status.HTTP_400_BAD_REQUEST
        )

    @action(
        methods=["put"],
        detail=True,
        permission_classes=[IsAdminUser],
        serializer_class=UserActivateSerializer,
    )
    def activate(self, request, pk=None):
        """Activate and deactivate a user"""
        instance = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # toggle activation account
        instance.is_active = serializer.data.get("is_active")
        instance.save()
        return Response(UserSerializer(instance).data, status.HTTP_200_OK)


class LogoutView(CreateAPIView):
    serializer_class = LogoutSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args):
        sz = self.get_serializer(data=request.data)
        sz.is_valid(raise_exception=True)
        sz.save()
        return Response(
            {"message": "User successfully logout"}, status=status.HTTP_200_OK
        )


class UserProfileView(RetrieveAPIView):
    serializer_class = UserSerializer

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def get_odoo_client(self):
        return self.request.user.odoo_client or switch_country(
            self.request.user.company_id.odoo_id
        )


class CountryViewSet(viewsets.ModelViewSet):
    http_method_names = ["get", "head"]
    permission_classes = [IsAdminUser]
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    pagination_class = None

    def get_odoo_client(self, country_id=None):
        if country_id:
            return settings.odoo[country_id]
        return (
            self.request.user.odoo_client
            or settings.odoo[self.request.user.company_id.odoo_id]
        )

    @action(
        methods=["get"],
        detail=True,
        permission_classes=[IsAdminUser],
    )
    def country_pricelists(self, request, pk=None):
        country = Country.objects.filter(id=pk).first()
        if country is None:
            return Response(
                {"error": f"Country {pk} Not Found"}, status=status.HTTP_404_NOT_FOUND
            )
        client = self.get_odoo_client(int(pk))

        arguments = {
            "limit": None,
            "offset": 0,
            "order": "sequence ASC",
            "fields": ["id", "name", "currency_id", "company_id", "active"],
        }

        domain = [["company_id", "=", int(pk)]]

        results = lambda: client.search_read(
            "product.pricelist", tuple(domain), **arguments
        )
        data = Helpers.call_with_retries(results)
        if isinstance(data, dict) and "error" in data:
            print(
                f"\n\n\n\n error occured here fetching country_pricelists for {pk}",
                data,
            )
            Response(
                {"error": f"Error occured while fetching pricelists for {pk}"},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )
        return Response({"data": data}, status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        try:
            if request.query_params.get("update"):
                update_companies()
            return super().list(request, *args, **kwargs)
        except Exception as e:
            print(Helpers.stacktrace(e, "-----Country vieset---"))


class PhoneNumberCheckView(ListAPIView):
    permission_classes = (AllowAny,)

    def list(self, request, *args, **kwargs):
        print(args, kwargs, "=====================\n\n\n\n")
        redis_cache = settings.redis_cache
        phone_number = kwargs.get("phone_number", "")

        number_prefix = phone_number[:6]
        try:
            data = Helpers.get_cached_data(redis_cache, number_prefix)
            print("\n\n\n\n\ncached data here-------", data, type(data))
            if isinstance(data, dict):
                data.update(
                    national_format=f"0{phone_number[4:7]} {phone_number[7:]}",
                    phone_number=phone_number,
                )
                print(f"\n\n\n\n\n\nFound data in cache for {number_prefix} {data}")
                return Response(data, status=status.HTTP_200_OK)

            api_url = f"https://api.telnyx.com/v2/number_lookup/{phone_number}?type=carrier&type=caller-name"
            TELNYX_API_KEY = os.environ.get("TELNYX_API_KEY")
            headers = {
                "Authorization": f"Bearer {TELNYX_API_KEY}",
                "Content-Type": "application/json",
                "Accept": "application/json",
            }

            response = requests.get(api_url, headers=headers)
            print(response.text)
            response.raise_for_status()
            # if response.status_code == 200:
            data = json.loads(response.text)
            response.close()
            data = json.loads(response.text)
            if len(phone_number) == 13 or (
                "+257" in phone_number and len(phone_number) >= 12
            ):
                redis_cache.set(number_prefix, bytes(json.dumps(data), "utf-8"))
                redis_cache.expire(number_prefix, 60 * 60 * 24 * 3)
            print("\n\n\n\ storingndatat in cache", data)
            return Response(data, status=status.HTTP_200_OK)
        except Exception as err:
            print(f"Error occurred with telnyx api {err}")
            return Response(
                {"error": _("Error occurred with telnyx api")},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class CountryUpdateApiView(RetrieveUpdateAPIView):
    serializer_class = CountrySerializer
    model = Country
    queryset = Country.objects.all()
    http_method_names = ["patch", "put"]

    def get_odoo_client(self):
        return (
            self.request.user.odoo_client
            or settings.odoo[self.request.user.company_id.odoo_id]
        )

    def patch(self, request, pk=None):
        instance = Country.objects.get(id=pk)
        try:
            data = request.data

            client = self.get_odoo_client()
            country = client.get_by_ids(Country._odoo_model, [pk], fields=["id"])
            if len(country) != 1:
                return Response(
                    {"error": f"invalid company id {pk}"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            serializer = self.serializer_class(data=dict(data))
            serializer.is_valid(raise_exception=True)

            serializer.update(instance, serializer.validated_data)
            serializer.validated_data["payment_method_ids"] = [
                x.id for x in serializer.validated_data.get("payment_method_ids", [])
            ]

            return Response(
                {
                    "success": "Payment terms updated for company {pk}",
                    "data": serializer.validated_data,
                },
                status=status.HTTP_200_OK,
            )
        except Exception as e:
            print(Helpers.stacktrace(e, "-----country update error"))


class DeviceIdViewSet(ListCreateAPIView):
    serializer_class = MyFCMDeviceSerializer
    permission_classes = (IsAuthenticated,)
    queryset = FCMDevice.objects.all()

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(user=user)
