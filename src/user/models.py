from datetime import datetime, timedelta

import json
from django.contrib.auth.hashers import check_password as check_pin
from django.contrib.auth.hashers import make_password as make_pin
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _
from model_utils import Choices
from django.contrib.postgres.fields import ArrayField
import jsonfield

from src.djangodoo.utils import switch_country


# from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken

from config.settings.common import SECRET_KEY
from src.djangodoo.models import OdooModel
from src.utils.fields import CustomPhoneNumberField, CustomEmailField
from src.core.models import TimestampedModel, SoftDeletionModel
from src.core.managers import CustomUserManager
from src.payment.models import PaymentMethod


class Tokens(TimestampedModel, models.Model):
    """
    Model to manage tokens
    """

    refresh_token = models.CharField(
        _("refresh_token"), null=False, blank=False, max_length=255
    )
    access_token = models.CharField(
        _("refresh_token"), null=False, blank=False, max_length=255
    )

    def __str__(self):
        return (
            f"<Refresh Token: {self.refresh_token} Access Token: {self.access_token}>"
        )


class UserRole(TimestampedModel, SoftDeletionModel):
    name = models.CharField(_("name"), null=False, blank=False, max_length=50)

    def __str__(self):
        return self.name


class CountryAccess(models.Model):
    username = models.CharField(max_length=50)
    access_pass = models.CharField(max_length=80)

    def __str__(self):
        return self.username


class UserDefinedDefaults(TimestampedModel, OdooModel):
    _odoo_model = "ir.default"
    _odoo_fields = [
        "user_id",
        "company_id",
        "id",
        "display_name",
        "condition",
        "json_value",
    ]


class Country(TimestampedModel, OdooModel):
    _odoo_model = "res.company"
    _odoo_fields = ["name", "country_id", "currency_id", "country_code"]

    LANGUAGE_CODE_CHOICES = [
        ("en", "English"),
        ("fr", "French"),
    ]

    DEFAULT_LANGUAGE_CODE = "en"

    name = models.CharField(max_length=100)
    odoo_id = models.IntegerField(unique=True, null=True)
    currency = models.CharField(max_length=100, null=True)
    country_code = models.CharField(max_length=100, default="UG")
    last_update = models.DateTimeField(auto_now=True, null=True)
    blank_payment_term = models.BooleanField(default=False)
    company_name = models.CharField(
        blank=True, help_text="", max_length=512, null=True, verbose_name="Company Name"
    )
    payment_methods = models.ManyToManyField(PaymentMethod)
    payment_method_id = models.IntegerField(default=1, help_text="Method id in odoo")
    currency_id = models.IntegerField(
        default=44, help_text="Currency for country in orders"
    )
    access_id = models.OneToOneField(
        CountryAccess, on_delete=models.CASCADE, null=True, blank=True
    )
    payment_term_id = models.IntegerField(
        default=1,
        help_text="Payment terms to be used in orders, defaults to immediate payment",
    )
    minimum_threshold = models.IntegerField(default=15)
    minimum_order_value = models.IntegerField(default=0)
    phone_number_prefix = models.CharField(default="+256", max_length=5)
    trainings = jsonfield.JSONField()
    company_defaults = jsonfield.JSONField()
    payment_terms = jsonfield.JSONField()
    language_code = models.CharField(
        max_length=20, choices=LANGUAGE_CODE_CHOICES, default=DEFAULT_LANGUAGE_CODE
    )
    pricelist_id = models.IntegerField(null=True)

    class Meta:
        ordering = ["id", "name"]

    def __str__(self):
        return self.name


class User(AbstractUser, TimestampedModel, OdooModel):
    """User class to modify the base user class.

    Arguments:
        AbstractUser {class} -- Original user model out of the box.
    """

    _odoo_model = "res.partner"
    _odoo_fields = [
        "name",
        "phone",
        "email",
        "contact_type",
        # "customer",
        "is_company",
        "company_id",
        "create_date",
        "write_date",
        "city",
        "property_product_pricelist",
        "training_ids",
        "street",
        "contact_type",
    ]

    CONTACT_TYPE_CHOICES = [
        ("cutomer", "Customer"),
        ("vendor", "Vendor"),
        ("employee", "Employee"),
        ("cluster", "Cluster"),
        {"other", "Other"},
    ]

    username = None
    x_price_list_id = models.IntegerField(null=True)
    email = CustomEmailField(_("email address"), unique=True)
    USERNAME_FIELD = "email"
    is_admin = models.BooleanField(default=False)
    phone_number = CustomPhoneNumberField(
        _("Phone number"), null=True, blank=True, unique=True
    )
    default_phone_number = CustomPhoneNumberField(
        _("Deault Phone number"), null=True, blank=True, unique=False
    )
    company_id = models.ForeignKey(
        Country, on_delete=models.CASCADE, parent_link=True, null=True
    )
    city = models.CharField(_("district"), max_length=100, null=True, blank=True)
    pin_set = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    pin = models.CharField(null=True, blank=True, default="", max_length=100)
    name = models.CharField(
        blank=True, help_text="", max_length=512, null=True, verbose_name="Name"
    )
    training_ids = jsonfield.JSONField(default=[])  # actual trainings ids
    trainings = jsonfield.JSONField(
        default=[]
    )  # trainig template ids #TODO rename field
    street = models.CharField(_("cluster"), max_length=100, null=True, blank=True)

    REQUIRED_FIELDS = ["phone_number"]

    odoo_client = None
    activate_hash = models.CharField(
        blank=True, max_length=512, null=True, verbose_name="Qr code hash"
    )
    contact_type = models.CharField(
        max_length=20, choices=CONTACT_TYPE_CHOICES, default="customer"
    )
    ordering_for = models.IntegerField(null=True)

    objects = CustomUserManager()

    __original_training = None

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        self.__original_training = self.trainings

    @classmethod
    def get_user(cls, odoo_id):
        if odoo_id:
            user = User.objects.filter(odoo_id=odoo_id).first()
            return user

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        from src.utils.tasks import send_notification

        if self.trainings != self.__original_training:
            send_notification(
                self.id,
                _(f"Product list for training updated"),
                _("Success"),
                **{
                    "old_training": self.__original_training,
                    "new_training": self.trainings,
                },
            )

        super(User, self).save(force_insert, force_update, *args, **kwargs)
        self.__original_training = self.trainings

    def __str__(self):
        """Email is returned when the user is parsed as a string."""
        return self.email

    def _generate_auth_tokens(self):
        """
        Generates the token that stores the user's id
        It also expires in an hour
        """
        refresh = RefreshToken.for_user(self)
        return dict(access_token=str(refresh.access_token), refresh_token=str(refresh))

    @property
    def token(self):
        """
        This function allows us to generate the user token as `user.token` through
        calling the token as a "dynamic property" when we add the `@property`
        """
        return self._generate_auth_tokens()

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
        ordering = ["id", "name"]

    def set_pin(self, raw_pin):
        self.pin = make_pin(raw_pin)
        self._pin = raw_pin

    def check_pin(self, raw_pin):
        def setter(raw_pin):
            self.set_pin(raw_pin)
            self._pin = None
            self.save(update_fields=["pin"])

        return check_pin(raw_pin, self.pin, setter)
