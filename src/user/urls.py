from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import (
    AccountVerificationAPIView,
    AdminLoginAPIView,
    AdminResetPasswordView,
    ChangePasswordView,
    ChangePinView,
    CHEActivateView,
    CHELoginView,
    CHELaunchView,
    CHESetPinView,
    CHEUserViewSet,
    CreateUserView,
    DeviceIdViewSet,
    LogoutView,
    TokenRefreshView,
    UpdateAPIView,
    UserCreateAPIView,
    UserDeleteAPIView,
    UserDetailAPIView,
    UserListAPIView,
    UserProfileView,
    UserUpdateAPIView,
    CheRefreshAPIView,
    CHEUserUpdateAPIView,
    QRCodeAPIView,
)

router = routers.DefaultRouter()
router.register(r"", CHEUserViewSet)

urlpatterns = [
    # admin routes
    path("admin/login", AdminLoginAPIView.as_view(), name="admin-user-login"),
    path("auth/refresh", TokenRefreshView.as_view(), name="refresh-auth-token"),
    path("admin/create-user", CreateUserView.as_view(), name="create-user"),
    path("admin/users", UserListAPIView.as_view(), name="list-users"),
    path("admin/users/<int:pk>", UserDeleteAPIView.as_view(), name="delete-user"),
    path(
        "admin/users/<int:pk>/update", UserUpdateAPIView.as_view(), name="update-user"
    ),
    path(
        "admin/reset-password", AdminResetPasswordView.as_view(), name="reset-password"
    ),
    path("admin/change-password", ChangePasswordView.as_view(), name="change-password"),
    path(
        "admin/refresh-ches",
        CheRefreshAPIView.as_view(),
        name="refresh-csv",
    ),
    path(
        "admin/activate/<uidb64>/<activation_token>",
        AccountVerificationAPIView.as_view(),
        name="verify-account-activation-link",
    ),
    # auth routes
    url(r"^auth/activate/?$", CHEActivateView.as_view(), name="che-activate"),
    url(r"^auth/activate/setpin/?$", CHESetPinView.as_view(), name="che-pin-set"),
    path("auth/login/", CHELoginView.as_view(), name="che-login-set"),
    path("auth/launch/", CHELaunchView.as_view(), name="che-launch"),
    path("auth/logout/", LogoutView.as_view(), name="che-logout-set"),
    path("auth/profile/", UserProfileView.as_view(), name="user-profile"),
    path("auth/change_pin/", ChangePinView.as_view(), name="change_pin"),
    # che routes
    path(r"ches/", include(router.urls)),
    path("ches/<int:pk>", CHEUserUpdateAPIView.as_view(), name="update-che"),
    path(r"device/", DeviceIdViewSet.as_view(), name="device_register"),
    path(r"auth/qr_code", QRCodeAPIView.as_view(), name="qr_code_activation"),
]
