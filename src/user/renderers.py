import json

from rest_framework.renderers import JSONRenderer


class UserJSONRenderer(JSONRenderer):
    charset = "utf-8"

    def render(self, data, media_type=None, renderer_context=None):
        # formart JSON responses
        errors = data.get("errors", None)
        access_token = data.get("access_token", None)
        refresh_token = data.get("refresh_token", None)

        if errors is not None:
            return super(UserJSONRenderer, self).render(data)

        if (
            refresh_token
            and access_token
            and all(
                [isinstance(token, bytes) for token in [access_token, refresh_token]]
            )
        ):
            data["refresh_token"] = token.decode("utf-8")
            data["access_token"] = token.decode("utf-8")

        return json.dumps(data)
