from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import CountryViewSet, CountryUpdateApiView

router = routers.DefaultRouter()
router.register(r"", CountryViewSet, basename="country")

urlpatterns = [
    # che routes
    path("", include(router.urls)),
    path("<int:pk>", CountryUpdateApiView.as_view(), name="update-user"),
]
