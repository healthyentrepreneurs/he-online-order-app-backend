import re
import json

from config.settings.common import SIMPLE_JWT
from django.contrib.auth import authenticate, get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import transaction
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import ugettext as _
from fcm_django.api.rest_framework import FCMDevice, FCMDeviceSerializer
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from src.payment.models import PaymentMethod
from src.payment.serializers import PaymentMethodSerializer
from src.utils.pincode_token import account_activation_token
from src.utils.tasks import send_notifications
from src.djangodoo.utils import switch_country
from django.conf import settings
from src.accounting.models import Invoices
from django.utils.translation import gettext as _


from .models import Country, Tokens

User = get_user_model()


class CountrySerializer(serializers.ModelSerializer):
    payment_methods = PaymentMethodSerializer(many=True, read_only=True)
    payment_method_ids = serializers.PrimaryKeyRelatedField(
        queryset=PaymentMethod.objects.all(), write_only=True, many=True, required=False
    )
    trainings = serializers.SerializerMethodField()
    minimum_threshold = serializers.IntegerField(required=False)
    minimum_order_value = serializers.IntegerField(required=False)
    phone_number_prefix = serializers.CharField(max_length=5, required=False)
    language_code = serializers.ChoiceField(
        choices=Country.LANGUAGE_CODE_CHOICES, default=Country.DEFAULT_LANGUAGE_CODE
    )
    payment_terms = serializers.JSONField(required=False)
    blank_payment_term = serializers.BooleanField(required=False)

    class Meta:
        model = Country
        depth = 1
        read_only_fields = (
            "payment_method_id",
            "company_name",
            "country_code",
            "name",
            "odoo_id",
            "currency",
            "currency_id",
            "payment_term_id",
            "payment_methods",
            "trainings",
            "minimum_threshold",
            "minimum_order_value",
            "blank_payment_term",
        )
        exclude = ("access_id",)

    def get_trainings(self, obj):
        print("country trainings before--->", obj.trainings, type(obj.trainings))
        trainings = (
            obj.trainings
            if isinstance(obj.trainings, dict)
            else json.loads(obj.trainings)
        )
        return trainings

    @transaction.atomic
    def update(self, instance, validated_data):
        payment_ids = validated_data.get("payment_method_ids", None)
        payment_term_ids = validated_data.get("payment_terms", None)
        company_defaults = (
            instance.company_defaults
            if isinstance(instance.company_defaults, dict)
            else json.loads(instance.company_defaults)
        )
        company_defaults["payment_terms"] = payment_term_ids
        validated_data["company_defaults"] = json.dumps(company_defaults)

        update = super().update(instance, validated_data)
        if payment_ids is not None:
            update.payment_methods.set(payment_ids)
            data = validated_data.copy()
            data["payment_method_ids"] = PaymentMethodSerializer(
                validated_data["payment_method_ids"], many=True
            ).data
            update.save()
            # send notifications to users of this company
            devices = FCMDevice.objects.filter(user__company_id=update.id)
            print(f"\n\n\n\n sending notif for {devices}")
            send_notifications(
                devices,
                title=_("Payment methods updated"),
                body=_("We have changed the payment options. Press here to confirm"),
                **data,
            )
        update.save()
        return update


class QRCodeGenerateSerializer(serializers.ModelSerializer):
    """Serializers for user qr code creation."""

    activate_hash = serializers.CharField(max_length=225, required=False)
    email = serializers.EmailField(max_length=225, allow_blank=False, required=True)

    class Meta:
        model = User
        fields = ["email", "activate_hash"]

    def validate(self, data):
        return super().validate(data)

    def update(self, instance, validated_data):
        hash = validated_data.pop("activate_hash", None)

        setattr(instance, "activate_hash", hash)
        instance.save()
        return instance


class AdminResetPasswordSerializer(serializers.ModelSerializer):
    """Serializers for admin user password reset."""

    email = serializers.EmailField(max_length=225, allow_blank=False, required=True)

    class Meta:
        model = User
        fields = ["email"]

    def validate(self, data):
        return super().validate(data)


class CreateUserSerializer(serializers.ModelSerializer):
    """Serializers user creation requests and creates a new admin user."""

    email = serializers.CharField(max_length=255)
    name = serializers.CharField(max_length=255)
    country = serializers.PrimaryKeyRelatedField(
        source="company_id",
        queryset=Country.objects.all(),
    )
    password = serializers.CharField(max_length=128, min_length=8, write_only=True)

    @staticmethod
    def password_validation(password):
        if (
            re.compile(
                r"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&.,_])[A-Za-z\d$@$!%*#?&]{8,}$"
            ).search(password)
            is None
        ):
            raise serializers.ValidationError(
                {
                    "error": "Ensure your password is alphanumeric, with Minimum eight characters, at least one letter, one number and one special character. Special chracters allowed include '@$!%*#?&' "
                }
            )

    def validate(self, data):
        validated_data = super().validate(data)
        email = validated_data.get("email", None)
        password = validated_data.get("password", None)
        country = validated_data.get("company_id", None)
        name = validated_data.get("name", None)

        CreateUserSerializer.password_validation(password)
        return {
            "email": email,
            "password": password,
            "company_id": country,
            "name": name,
        }

    class Meta:
        model = User
        fields = ["email", "is_admin", "password", "country", "company_id", "name"]

    def create(self, validated_data):
        return User.objects.create_superuser(**validated_data, is_active=False)

    def to_representation(self, instance):
        return UserSerializer(self).to_representation(instance)


class UserUpdateSerializer(serializers.ModelSerializer):
    email = serializers.CharField(max_length=255, required=False)
    name = serializers.CharField(max_length=255, required=False)
    ordering_for = serializers.IntegerField(required=False)
    country = serializers.PrimaryKeyRelatedField(
        source="company_id", queryset=Country.objects.all(), required=False
    )
    password = serializers.CharField(
        max_length=128, min_length=8, write_only=True, required=False
    )

    class Meta:
        model = User
        fields = [
            "email",
            "is_admin",
            "password",
            "country",
            "company_id",
            "name",
            "ordering_for",
        ]

    def validate(self, data):
        data = super().validate(data)
        if "ordering_for" in data:
            msg = None
            shadowed_user = User.objects.filter(odoo_id=data["ordering_for"]).first()
            if shadowed_user is None:
                msg = _(f"User {data['ordering_for']} not found.")
            if shadowed_user.is_active is False:
                msg = _(f"User {data['ordering_for']} is not active.")
            if msg:
                raise serializers.ValidationError({"error": msg})

        if "password" in data:
            CreateUserSerializer.password_validation(data["password"])
        return data

    def update(self, instance, validated_data):
        password = validated_data.pop("password", None)
        for key, value in validated_data.items():
            if key in ["company_id", "country"]:
                key = "company_id"
                country = Country.objects.filter(id=value).first()
                if country is None:
                    raise serializers.ValidationError(
                        "Invalid country, please enter a valid country."
                    )
                value = country
            setattr(instance, key, value)

        if password is not None:
            instance.set_password(password)
        instance.save()

        return instance


class TokenRefreshSerializer(serializers.Serializer):
    refresh_token = serializers.CharField()
    access_token = serializers.CharField(read_only=True)

    def validate(self, attrs):
        try:
            refresh = RefreshToken(attrs["refresh_token"])
        except TokenError as error:
            msg = _("Invalid refresh token")
            raise serializers.ValidationError(msg)

        data = {"access_token": str(refresh.access_token)}

        if SIMPLE_JWT["ROTATE_REFRESH_TOKENS"]:
            if SIMPLE_JWT["BLACKLIST_AFTER_ROTATION"]:
                try:
                    # Attempt to blacklist the given refresh token
                    refresh.blacklist()
                except AttributeError:
                    # If blacklist app not installed, `blacklist` method will
                    # not be present
                    pass

            refresh.set_jti()
            refresh.set_exp()

            data["refresh_token"] = str(refresh)

        return data

    def create(self, validated_data):
        return Tokens(**validated_data)


class UserSerializer(serializers.ModelSerializer):
    country = CountrySerializer(read_only=True, allow_null=True, source="company_id")
    district = serializers.CharField(source="city")
    x_price_list_id = serializers.CharField(read_only=True)
    cluster = serializers.CharField(source="street")
    free_products = serializers.SerializerMethodField(required=False)
    credit_balance = serializers.SerializerMethodField(required=False)
    ordering_for = serializers.SerializerMethodField(required=False)
    open_invoices_count = serializers.SerializerMethodField(required=False)
    contact_type = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "email",
            "password",
            "is_staff",
            "is_admin",
            "name",
            "is_active",
            "country",
            "odoo_id",
            "phone_number",
            "district",
            "pin_set",
            "x_price_list_id",
            "training_ids",
            "trainings",
            "ordering_for",
            "cluster",
            "contact_type",
            # "promotional_message",
            "free_products",
            "credit_balance",
            "default_phone_number",
            "open_invoices_count",
        ]
        read_only_fields = ["training_ids", "odoo_id"]
        extra_kwargs = {"password": {"write_only": True}}

    def get_ordering_for(self, obj):
        if obj.ordering_for:
            ordering_for_user = User.objects.get(odoo_id=obj.ordering_for)
            return {
                "name": f"{ordering_for_user.first_name} {ordering_for_user.last_name}",
                "phone_number": ordering_for_user.phone_number.raw_input,
                "odoo_id": ordering_for_user.odoo_id,
            }
        return {}

    def get_credit_balance(self, obj):
        print("\n\n\n\nobj here00-=-==>>", vars(obj))
        try:
            if self.context.get("view").action == "list":
                print(
                    "self.context.get('view').action == 'list' ====this runs---comes here"
                )
                return 0
        except AttributeError as e:
            if [
                x
                for x in [
                    "UserListAPIView",
                    "UserCreateAPIView",
                    "UserDeleteAPIView",
                    "NoneType",
                ]
                if x in e.__str__()
            ]:
                return 0
        client = self.context.get("view").get_odoo_client()
        odoo_id = obj.odoo_id
        if odoo_id is None:
            from src.user.models import User

            odoo_id = User.objects.get(id=obj.id).odoo_id

        payment_terms = obj.company_id.payment_terms
        payment_term_ids = (
            payment_terms
            if isinstance(payment_terms, list)
            else json.loads(payment_terms)
        )
        return Invoices.get_credit_balance(obj, client, payment_term_ids).get(
            "credit_balance", 0
        )

    def get_open_invoices_count(self, obj):
        try:
            if self.context.get("view").action == "list":
                print(
                    "self.context.get('view').action == 'list' ====this runs---comes here"
                )
                return 0
        except AttributeError as e:
            if [
                x
                for x in [
                    "UserListAPIView",
                    "UserCreateAPIView",
                    "UserDeleteAPIView",
                    "NoneType",
                ]
                if x in e.__str__()
            ]:
                return 0
        client = self.context.get("view").get_odoo_client()
        odoo_id = obj.odoo_id
        if odoo_id is None:
            from src.user.models import User

            odoo_id = User.objects.get(id=obj.id).odoo_id

        return Invoices.get_open_invoices_count(odoo_id, client).get("open_payments", 0)

    def create(self, validated_data):
        username = validated_data["username"]
        email = validated_data["email"]
        password = validated_data["password"]
        user_obj = User(username=username, email=email, is_staff=True)
        user_obj.set_password(password)
        user_obj.save()
        return validated_data

    def to_representation(self, instance):
        """Convert `is_admin` to role."""
        data = super().to_representation(instance)
        name = data["name"]
        username = name.split()[0] if isinstance(name, str) else ""
        data["role"] = "Admin" if data["is_admin"] else "Che"
        data["username"] = username
        return data

    def get_free_products(self, obj):
        from src.trainings.models import FreeProduct
        from src.trainings.serializers import FreeProductSerializer

        serializer = FreeProductSerializer(
            FreeProduct.objects.filter(training__in=obj.trainings).all(),
            many=True,
        )
        return serializer.data


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    access_token = serializers.CharField(max_length=255, read_only=True)
    refresh_token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        email = data.get("email", None)
        password = data.get("password", None)

        if email is None:
            raise serializers.ValidationError(
                _("An email address is required to log in.")
            )

        if password is None:
            raise serializers.ValidationError(_("A password is required to log in."))

        user = authenticate(email=email, password=password)

        # If no user was found matching this email/password combination
        if user is None:
            raise serializers.ValidationError(_("Invalid username or password."))

        if not user.is_active:
            raise serializers.ValidationError(_("This user has been deactivated."))

        return {
            "email": user.email,
            "access_token": user.token.get("access_token"),
            "refresh_token": user.token.get("refresh_token"),
        }


class CHEPinSetSerializer(serializers.Serializer):
    pin = serializers.DecimalField(max_digits=5, decimal_places=0, min_value=999)
    key = serializers.CharField(required=True)

    def validate(self, attrs):
        validated_date = super().validate(attrs)
        raw_key = validated_date.get("key")
        pin = validated_date.get("pin")
        decoded_key = account_activation_token.decode_token(raw_key)
        if not decoded_key:
            msg = _("Invalid key")
            raise serializers.ValidationError(msg)
        try:
            user = User.objects.get(pk=decoded_key)
        except User.DoesNotExist:
            msg = _("User doesnot exist")
            raise serializers.ValidationError(msg)
        if user.pin_set:
            msg = _("Your pin has already been set try resetting pin")
            raise serializers.ValidationError(msg)

        user.set_pin(pin)
        user.pin_set = True
        user.save()
        serialized_data = UserSerializer(user).data
        return {
            "success": True,
            "message": _("Pin successfully set"),
            "phone_number": serialized_data.get("phone_number"),
        }


class CHEActivateSerializer(serializers.Serializer):
    phone_number = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    def validate_phone_number(self, phone_number):
        # import phonenumbers
        if len(phone_number) < 12 or phone_number[0] != "+":
            raise serializers.ValidationError(_("Invalid phone number"))
        return phone_number

    def validate(self, attrs):
        validated_data = super().validate(attrs)

        password = validated_data.get("password")
        phone_number = validated_data.get("phone_number")
        try:
            user_obj = User.objects.filter(phone_number=phone_number).first()
        except Exception as error:
            msg = _("CHE not found")
            raise serializers.ValidationError(msg)
        if user_obj is not None:
            credentials = {
                "phone_number": user_obj.phone_number,
                "password": password,
            }
            if all(credentials.values()):
                user = authenticate(**credentials)
                if user:
                    if not user.is_active:
                        msg = _("Contact admin to activate your account")
                        raise serializers.ValidationError(msg)
                    if not user.odoo_id:
                        msg = _("User is not a che")
                        raise serializers.ValidationError(msg)
                    token = account_activation_token.make_token(user)
                    return {
                        "key": str(token),
                    }
                msg = _("Unable to activate account with provided credentials.")
                raise serializers.ValidationError(msg)
            else:
                msg = _('Must include "{username_field}" and "password".')
                msg = msg.format(username_field=self.username_field)
                raise serializers.ValidationError(msg)

        else:
            msg = _("Account with this phone number does not exists")
            raise serializers.ValidationError(msg)


class CHELaunchLoginSerializer(serializers.Serializer):
    launch = serializers.CharField(required=True)

    def validate_launch(self, phone_number):
        if "+257" not in phone_number and len(phone_number) < 13:
            raise serializers.ValidationError(
                _("Invalid phone number length for country")
            )
        if phone_number[0] != "+":
            raise serializers.ValidationError(_("Invalid phone number"))
        if not User.objects.filter(phone_number=phone_number).exists():
            raise serializers.ValidationError(_("CHE not found"))
        return phone_number

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        launch = validated_data.get("launch")

        if user_obj := User.objects.filter(phone_number=launch).first():
            CHELoginSerializer.active_che(user_obj)
            return CHELoginSerializer.login_user(user_obj, launch)


class CHELoginSerializer(serializers.Serializer):
    phone_number = serializers.CharField(required=True)
    pin = serializers.DecimalField(max_digits=5, decimal_places=0, required=True)

    def validate_phone_number(self, phone_number):
        if "+257" not in phone_number and len(phone_number) < 13:
            raise serializers.ValidationError(
                _("Invalid phone number length for country")
            )
        if phone_number[0] != "+":
            raise serializers.ValidationError(_("Invalid phone number"))
        return phone_number

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        phone_number = validated_data.get("phone_number")
        pin = validated_data.get("pin")

        try:
            user_obj = User.objects.filter(phone_number=phone_number).first()
        except ValidationError:
            msg = _("CHE not found")
            raise serializers.ValidationError(msg)
        if user_obj is not None:
            if all([pin, phone_number]):
                user = authenticate(pin=pin, phone_number=phone_number)
                if user:
                    CHELoginSerializer.active_che(user)
                    return CHELoginSerializer.login_user(user, phone_number)
                    # if not user.is_active:
                    #     msg = _("Contact admin to activate your account")
                    #     raise serializers.ValidationError(msg)
                    # if not user.odoo_id:
                    #     msg = _("User is not a che")
                    #     raise serializers.ValidationError(msg)
                    # refresh = RefreshToken.for_user(user)
                    # user.default_phone_number = phone_number
                    # user.save()

                    # return {
                    #     "refresh": str(refresh),
                    #     "access": str(refresh.access_token),
                    #     "user_id": user.id,
                    #     "odoo_id": user.odoo_id,
                    # }
                msg = _("Unable to login with provided credentials.")
                raise serializers.ValidationError(msg)
            else:
                msg = _('Must include "{username_field}" and "password".')
                msg = msg.format(username_field=self.username_field)
                raise serializers.ValidationError(msg)

        else:
            msg = _("Account with this phone number does not exists")
            raise serializers.ValidationError(msg)

    @classmethod
    def active_che(cls, user):
        if not user.is_active:
            msg = _("Contact admin to activate your account")
            raise serializers.ValidationError(msg)
        if not user.odoo_id:
            msg = _("User is not a che")
            raise serializers.ValidationError(msg)

    @classmethod
    def login_user(cls, user, phone_number):
        refresh = RefreshToken.for_user(user)
        user.default_phone_number = phone_number
        user.save()

        return {
            "refresh": str(refresh),
            "access": str(refresh.access_token),
            "user_id": user.id,
            "odoo_id": user.odoo_id,
        }


class CHEUpdateSerializer(serializers.Serializer):
    default_phone_number = PhoneNumberField(required=False)
    ordering_for = serializers.IntegerField(required=False)

    def update(self, instance, validated_data):
        for key, value in validated_data.items():
            setattr(instance, key, value)
        instance.save()
        return instance

    def validate(self, data):
        data = super().validate(data)
        if "ordering_for" in data:
            msg = None
            shadowed_user = User.objects.filter(odoo_id=data["ordering_for"]).first()
            if shadowed_user is None:
                msg = _(f"User {data['ordering_for']} not found.")
            if shadowed_user.is_active is False:
                msg = _(f"User {data['ordering_for']} is not active.")
            if msg:
                raise serializers.ValidationError({"error": msg})

        return data


class ChangePinSerializer(serializers.Serializer):
    """
    Serializer for pin change endpoint.
    """

    old_pin = serializers.DecimalField(
        max_digits=5, decimal_places=0, min_value=999, required=True
    )
    new_pin = serializers.DecimalField(
        max_digits=5, decimal_places=0, min_value=999, required=True
    )


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    default_error_messages = {"bad_token": _("Refresh Token is invalid or expired")}

    def validate(self, attrs):
        self.token = attrs["refresh"]
        return attrs

    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()
        except TokenError:
            self.fail("bad_token")


class CHEPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(read_only=True)
    phone_number = serializers.CharField(required=True)

    def validate_phone_number(self, phone_number):
        if "+257" not in phone_number and len(phone_number) < 13:
            raise serializers.ValidationError(
                _("Invalid phone number length for country")
            )
        if phone_number[0] != "+":
            raise serializers.ValidationError(_("Invalid phone number"))
        return phone_number


class CHECSVCredentialsSerializer(CHEPasswordSerializer):
    name = serializers.CharField(read_only=True)
    district = serializers.CharField(read_only=True)
    cluster = serializers.CharField(read_only=True)


class CHEPasswordCSVSerializer(serializers.Serializer):
    che_user_ids = serializers.ListField(
        child=serializers.IntegerField(min_value=0), allow_empty=False, required=True
    )


class UserActivateSerializer(serializers.Serializer):
    is_active = serializers.BooleanField(required=True)


class MyFCMDeviceSerializer(FCMDeviceSerializer):
    class Meta:
        model = FCMDevice
        fields = ("registration_id", "active", "user")
        extra_kwargs = {
            "id": {"read_only": False, "required": False},
            "user": {"read_only": True},
        }
