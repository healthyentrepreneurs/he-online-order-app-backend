from django.core.management.base import BaseCommand, CommandError

from src.utils.che_sync import get_company_updates, sync_deleted_users


class Command(BaseCommand):
    help = "Load a che user data"

    def handle(self, *args, **options):
        try:
            get_company_updates()
            # sync_deleted_users()
        except Exception as err:
            raise CommandError('User  creation failed ::\n"%s" ' % str(err))
        self.stdout.write(self.style.SUCCESS("Successfully synched ches"))
