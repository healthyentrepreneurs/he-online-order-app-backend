from django.core.management.base import BaseCommand, CommandError

from src.user.models import User


class Command(BaseCommand):
    help = "Create a che User"

    def add_arguments(self, parser):
        parser.add_argument("--phone", type=str)
        parser.add_argument("--password", type=str)
        parser.add_argument("--id", type=int)

    def handle(self, *args, phone, password, id, **options):
        try:
            che = User.objects.create_che_user(password, phone, odoo_id=id)
        except Exception as err:
            raise CommandError('User  creation failed ::\n"%s" ' % str(err))
        self.stdout.write(self.style.SUCCESS('Successfully closed che "%s"' % che.id))
