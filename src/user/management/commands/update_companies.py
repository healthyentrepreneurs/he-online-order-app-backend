from django.core.management.base import BaseCommand, CommandError

from src.utils.tasks import update_companies


class Command(BaseCommand):
    help = "Update compnay data"

    def handle(self, *args, **options):
        try:
            update_companies()
        except Exception as err:
            raise CommandError('Comapny update failed ::\n"%s" ' % str(err))
        self.stdout.write(self.style.SUCCESS("Successfully synched ches"))
