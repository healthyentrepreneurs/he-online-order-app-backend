from django.core.management.base import BaseCommand, CommandError

from src.utils.sync_users import sync_company_trainings


class Command(BaseCommand):
    help = "Sync company trainigs"

    def handle(self, *args, **options):
        try:
            sync_company_trainings()
        except Exception as err:
            raise err
        self.stdout.write(self.style.SUCCESS("Successfully synched company tainings"))
