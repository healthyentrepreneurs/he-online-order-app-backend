from django.core.management.base import BaseCommand, CommandError

from src.utils.sync_users import backfill_user_clusters


class Command(BaseCommand):
    help = "Sync user pricelist ids"

    def handle(self, *args, **options):
        try:
            backfill_user_clusters()
        except Exception as err:
            raise err
            raise CommandError('Price List Id sync ::\n"%s" ' % str(err))
        self.stdout.write(self.style.SUCCESS("Successfully synched user clusters "))
