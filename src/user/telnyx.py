from django.urls import path

from .views import PhoneNumberCheckView


urlpatterns = [
    path("<str:phone_number>", PhoneNumberCheckView.as_view(), name="check-number"),
]
