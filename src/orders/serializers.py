from rest_framework import serializers
from .models import Orders
from src.user.models import User
from src.payment.serializers import TransactionSerializer
from src.payment.models import Payment, SUCCESSFUL, RECEIVED


class OrderSerializerUtils:
    @staticmethod
    def get_currency(obj):
        data = obj["currency_id"]

        currency_id = data[1] if isinstance(data, list) else data.name

        print(obj["currency_id"], obj)
        return currency_id

    @staticmethod
    def get_customer(obj):
        data = obj["partner_id"]
        name, partner_id = "", ""
        if isinstance(data, list):
            name = data[1]
            partner_id = data[0]
        else:
            name = data.name
            partner_id = data.id

        user = User.objects.filter(odoo_id=partner_id).first()
        phone = str(user.phone_number) if user and user.phone_number else ""

        return {
            "id": partner_id,
            "username": name,
            "phonenumber": phone,
        }

    @staticmethod
    def get_order_status(obj):
        return obj.get("x_stored_order_status")


class OrderPaymentCheckSerializer(serializers.Serializer):
    order_id = serializers.IntegerField(required=True)


class OrderLineSerializer(serializers.Serializer):
    product_id = serializers.IntegerField(required=True)
    product_uom_qty = serializers.IntegerField(required=True)
    last_price = serializers.DecimalField(
        required=True, decimal_places=2, max_digits=16
    )
    free_product = serializers.BooleanField(required=False)

    def validate(self, data):
        return super().validate(data)

    def to_representation(self, instance):
        data = dict(
            product_id=instance["product_id"],
            product_uom_qty=instance["product_uom_qty"],
        )
        if instance.get("free_product", False):
            data.update(discount=100)
        return (
            0,
            0,
            data,
        )


class CreateOrderSerializer(serializers.Serializer):
    order_line = serializers.ListField(child=OrderLineSerializer())
    sub_total = serializers.DecimalField(required=True, decimal_places=2, max_digits=16)
    total = serializers.DecimalField(required=True, decimal_places=2, max_digits=16)
    payment_method = serializers.ChoiceField(
        choices=["momo", "ihela", "airtel", "mpesa", "test", "cash"]
    )

    def validate(self, data):
        return super().validate(data)


class OrdersSerializer(serializers.Serializer):
    created_at = serializers.DateTimeField(source="create_date")
    updated_at = serializers.DateTimeField(source="write_date")
    name = serializers.CharField()
    order_id = serializers.IntegerField(source="id")
    customer = serializers.SerializerMethodField()
    order_status = serializers.SerializerMethodField()
    currency = serializers.SerializerMethodField()
    order_line_items = serializers.SerializerMethodField()
    total = serializers.CharField(source="amount_total")

    def get_currency(self, obj):
        return OrderSerializerUtils.get_currency(obj)

    def get_order_status(self, obj):
        return OrderSerializerUtils.get_order_status(obj)

    def get_order_line_items(self, obj):
        items = []
        client = self.context.get("view").get_odoo_client()
        order_line_item_ids = obj.get("order_line")
        print(f"\n\n\n\n---------{order_line_item_ids}")
        line_items = Orders.fetch_by_ids(
            "sale.order.line",
            order_line_item_ids,
            ["name"],
            client=client,
        )
        return line_items

    def get_customer(self, obj):
        return OrderSerializerUtils.get_customer(obj)


class OrdersDetailSerializer(serializers.Serializer):
    created_at = serializers.DateTimeField(source="create_date")
    updated_at = serializers.DateTimeField(source="write_date")
    name = serializers.CharField()
    order_id = serializers.IntegerField(source="id")
    customer = serializers.SerializerMethodField()
    order_status = serializers.SerializerMethodField()
    order_line_items = serializers.SerializerMethodField()
    currency = serializers.SerializerMethodField()
    transaction_ids = serializers.SerializerMethodField()
    delivery_date = serializers.SerializerMethodField()
    commitment_date = serializers.CharField()
    sub_total = serializers.CharField(source="amount_untaxed")
    total = serializers.CharField(source="amount_total")
    tax = serializers.CharField(source="amount_tax")
    payment_details = serializers.SerializerMethodField()

    def get_currency(self, obj):
        return OrderSerializerUtils.get_currency(obj)

    def get_order_status(self, obj):
        return OrderSerializerUtils.get_order_status(obj)

    def get_payment_details(self, obj):
        order_id = obj.get("id")
        payment_object = Payment.objects.filter(
            order_id=order_id, status=SUCCESSFUL.lower()
        ).first()
        if payment_object:
            return TransactionSerializer(payment_object).data
        return None

    def get_transaction_ids(self, obj):
        return []

    def get_delivery_date(self, obj):
        status = obj.get("x_stored_order_status")
        if status == "not confirmed":
            return ""
        return obj.get("expected_date")

    def get_customer(self, obj):
        return OrderSerializerUtils.get_customer(obj)

    def get_order_line_items(self, obj):
        client = self.context.get("view").get_odoo_client()
        items = []
        order_line_item_ids = obj.get("order_line")
        line_items = Orders.fetch_by_ids(
            "sale.order.line",
            order_line_item_ids,
            [
                "display_name",
                "is_downpayment",
                "is_expense",
                "price_unit",
                "price_total",
                "product_id",
                "product_uom",
                "product_uom_qty",
                "name",
                "currency_id",
            ],
            client=client,
        )
        return line_items
