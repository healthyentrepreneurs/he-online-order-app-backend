from django.contrib import admin
from src.orders.models import Orders


class OrdersAdmin(admin.ModelAdmin):
    pass


admin.site.register(Orders, OrdersAdmin)
