from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import OrdersViewSet, OrdersHistoryViewSet

router = routers.DefaultRouter()
router.register(r"", OrdersViewSet)

urlpatterns = [
    path(r"", include(router.urls)),
    path(
        "history", OrdersHistoryViewSet.as_view({"get": "list"}), name="order-history"
    ),
]
