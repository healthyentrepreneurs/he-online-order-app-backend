from django.db import models

from src.core.models import TimestampedModel, SoftDeletionModel
from src.djangodoo.models import OdooModel
from django.conf import settings
from src.djangodoo.utils import switch_country
import os
from django.utils.translation import gettext as _
from src.utils.mail import send_error_email
from src.utils.helpers import Helpers
import time


class Orders(TimestampedModel, SoftDeletionModel, OdooModel):
    _odoo_model = "sale.order"
    _odoo_fields = [
        "__last_update",
        "amount_total",
        "amount_tax",
        "amount_untaxed",
        "commitment_date",
        "company_id",
        "commitment_date",
        "create_date",
        "create_uid",
        "currency_id",
        # "date_order",
        "delivery_count",
        "display_name",
        "id",
        "invoice_count",
        "invoice_ids",
        "invoice_status",
        "name",
        "customer_name",
        "note",
        "order_line",
        "partner_id",
        "picking_ids",
        "name",
        # "purchase_order_count",
        "state",
        # "sale_ok",
        "route_id",
        "reward_amount",
        "transaction_ids",
        "expected_date",
        "write_date",  # last updated on
        "has_free_products",
    ]
    name = models.CharField(max_length=100)
    odoo_id = models.IntegerField(unique=True, null=True)

    @classmethod
    def confirm_order(cls, order_id, client=None):
        client = client or settings.odoo
        res = None
        try:
            order_confirm = lambda: client.action_confirm(cls._odoo_model, order_id)
            call(order_confirm)
            res = client.force_lines_to_invoice_policy_order(order_id)
        except Exception as e:
            print("\n\n\n\nerror occured--order-action_post------>", e, end="\n\n\n\n")

            send_error_email(
                f"Error occured while confirming Order {order_id}",
                f"Error: {e} \n\n\n Order: {order_id}",
                settings.DEFAULT_FROM_EMAIL,
                ["cedriclusiba@gmail.com", "thijs@healthyentrepreneurs.nl"],
            )
        print("confirmed order---->>", order_id, res)
        return res

    @classmethod
    def get_order_route(cls, order_id, client=None):
        orders = client.get_by_ids(
            cls._odoo_model, [order_id], fields=["id", "route_id"]
        )
        if not orders:
            return {"error": _(f"order {order_id} not found")}
        order = orders[0]
        return order["route_id"]

    @classmethod
    def get_delivery_info(cls, route_id, client=None):
        routes = client.get_by_ids(
            "he.route", [route_id], fields=["id", "date", "end_date"]
        )
        if not routes:
            return {"error": _(f"route {route_id} not found")}
        route = routes[0]
        return route

    @classmethod
    def create_invoice(cls, order_id, client=None):
        client = client or settings.odoo

        invoices = client.action_invoice_create("sale.advance.payment.inv", order_id)
        client.call_action("sale.advance.payment.inv", "create_invoices", invoices)

        print("created invoices---->>", invoices)
        return invoices

    @classmethod
    def confirm_invoices(cls, order_id, client=None, cash=False):
        client = client or settings.odoo
        order = client.get_by_ids("sale.order", [order_id], fields=["invoice_ids"])

        if len(order) == 1:
            order = order[0]
        invoices = order.get("invoice_ids")
        print("\n\n\n\n\invoice here------->>", invoices, order)

        invoice_id = order["id"]
        for invoice_id in invoices:
            try:
                # invoice.action_post()
                call(lambda: client.action_post("account.move", invoice_id))
            except Exception as e:
                print(
                    "\n\n\n\nerror occured--invoice-action_post------>",
                    e,
                    end="\n\n\n\n",
                )

                send_error_email(
                    f"Error occured while confirming invoice {invoices}",
                    f"Error: {e} \n\n\n Invoice: {vars(invoice_id)}",
                    settings.DEFAULT_FROM_EMAIL,
                    ["cedriclusiba@gmail.com", "thijs@healthyentrepreneurs.nl"],
                )

            if cash is True:
                client.write("account.move", invoice_id, {"payment_state": "paid"})
        print("confirmed invoices---->>", invoices)
        return invoices

    @classmethod
    def delete_quotations(cls, ids, client=None):
        client = client or settings.odoo
        val = client.unlink("sale.order", [ids])
        print(f"deleted {len(ids)} quotations---->>")
        return val

    @classmethod
    def register_payment(cls, invoices, instance, client=None):
        from src.user.models import User
        from src.payment.utils import check_payment_created_in_odoo

        client = client or switch_country(instance.company_id.odoo_id)

        payment_in_odoo = Helpers.call_with_retries(
            lambda: check_payment_created_in_odoo(instance, client)
        )

        print(
            f"---------payment in odoo? {payment_in_odoo}-----------", instance.status
        )
        if payment_in_odoo is None:
            print("registering payment---start")
            amount = instance.amount
            user_id = instance.user_id.odoo_id
            company = instance.company_id

            payment_method_id = company.payment_method_id
            company_id = company.odoo_id
            currency_id = company.currency_id
            journal_id = instance.provider.journal_id

            odoo_model = cls._odoo_model

            payment_method_line_id_mapper = {
                "momo": 140,
                "mpesa": 101,
                "airtel": 141,
                "ihela": 177,
            }
            outstanding_account_id_mapper = {
                "momo": 799,
                "mpesa": 599,
                "airtel": 799,
            }

            data = {
                "partner_type": "customer",
                "payment_type": "inbound",
                "company_id": company_id,
                "partner_id": user_id,
                "country_code": company.country_code,
                "journal_id": journal_id,
                # "payment_method_line_id": 141,
                "from_product_app": True,
                "outstanding_account_id": outstanding_account_id_mapper[
                    instance.provider.name
                ],
                "payment_method_line_id": payment_method_line_id_mapper[
                    instance.provider.name
                ],  # payment_method_id,
                "state": "draft",
                "amount": float(amount),
                "currency_id": currency_id,
                "ref": instance.transaction_id,
            }
            print(
                "\n\n\n\n\n\n---111111111-++++++okay comes here",
                data,
                "\n\n\n\n\n---",
                float(amount),
            )
            payment_id = client.create("account.payment", data)

            print(payment_id)
            res = None
            try:
                res = client.action_post("account.payment", payment_id)
            except Exception as e:
                print("\n\n\n\nerror occured---action_post------>", e, end="\n\n\n\n")

            cls.attach_payment_to_invoice(
                instance.order_id, company_id, payment_id, client=None
            )
            print("\n\n\n\n\n payment_id ----->", payment_id)

            print("registering payment---end", res)
            user = User.objects.filter(ordering_for=user_id).first()
            if user:
                user.ordering_for = None
                user.save(update_fields=["ordering_for"])
            return res
        print("\n\n\n\n\n payment already registered ----->", payment_in_odoo)

    @classmethod
    def attach_payment_to_invoice(cls, order_id, company_id, payment_id, client=None):
        client = client or switch_country(company_id)
        order = client.search_read(
            cls._odoo_model, [("id", "=", order_id)], fields=["invoice_ids"]
        )
        print(f"\n\n\n\n\n -----attach_payment_to_invoice---order {order}")
        invoices = order[0].get("invoice_ids")
        for invoice_id in invoices:
            client.match_payment(payment_id, invoice_id)
            print(f"\n\n\n\n\npayment attached for ---->invoice---> {invoice_id}")

    @classmethod
    def odoo_fetch(cls, domain, client=None, limit=None, offset=0, **kwargs):
        do_no_inject = kwargs.get("do_no_inject", False)
        domain = domain if do_no_inject else cls.inject_orders_domain(domain)

        return super().odoo_fetch(domain, client, limit, offset, **kwargs)

    @classmethod
    def odoo_count(cls, domain, client=None):
        domain = cls.inject_orders_domain(domain)
        return super().odoo_count(domain, client=client)

    @classmethod
    def inject_orders_domain(cls, domain):
        validated_order_states = [
            "&",
            "|",
            ("state", "!=", "draft"),
            ("state", "!=", "sent"),
            "|",
            "|",
            ("user_id", "=", int(os.environ.get("SALES_USER_KENYA", 9))),
            ("user_id", "=", int(os.environ.get("SALES_USER_UGANDA", 8))),
            ("user_id", "=", int(os.environ.get("SALES_USER_BURUNDI", 10))),
        ]
        if domain:
            already_set = [check for check in validated_order_states if check in domain]

            if already_set:
                return domain
            elif not already_set and len(domain) > 0:
                domain.insert(0, "&")
        domain.extend(validated_order_states)
        return domain


def call(func_name):
    try:
        return func_name()
    except Exception as e:
        if any([x for x in ["idle", "request-sent"] if x in str(e).lower()]):
            print(
                f"\n\n\n\nerror retrying--{func_name}------>",
                e,
                end="\n\n\n\n",
            )
            time.sleep(0.8)

            return call(func_name)

        raise e
