import logging
import json
from django.shortcuts import render
from rest_framework import status
from django.conf import settings
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from src.core.filters import OdooSearchFilter, OdooParameterFilter
from src.core.pagination import OdooPagination
from src.djangodoo.models import OdooModel
from src.djangodoo.authentication import OdooAuthentication, OdooSessionAuthentication
from src.djangodoo.views import OdooObjectViewSet
from src.djangodoo.utils import switch_country
from src.utils.helpers import Helpers
from src.trainings.models import Trainings
from django.utils.translation import gettext as _
from django.contrib.auth import get_user_model


from django.conf import settings

from .models import Orders
from .serializers import (
    OrderPaymentCheckSerializer,
    OrdersSerializer,
    OrdersDetailSerializer,
    CreateOrderSerializer,
    OrderLineSerializer,
)

User = get_user_model()


logger = logging.getLogger(__name__)


class OrdersViewSet(OdooObjectViewSet):
    _model = Orders

    http_method_names = ["get", "head", "post"]
    queryset = Orders.objects.all()
    pagination_class = OdooPagination
    filter_backends = [OdooParameterFilter, OdooSearchFilter]
    filter_fields = [
        # "str@x_stored_order_status",
        "datetime@create_date",
        "datetime@write_date",
        "datetime@create_date__gt",
        "datetime@create_date__gte",
        "datetime@write_date__gt",
        "datetime@write_date__gte",
        "datetime@create_date__lt",
        "datetime@create_date__lte",
        "datetime@write_date__lt",
        "datetime@write_date__lte",
    ]
    search_fields = ["name", "id", "customer_name"]
    authentication_classes = [OdooAuthentication, OdooSessionAuthentication]

    action_mapper = {
        "list": OrdersSerializer,
        "retrieve": OrdersDetailSerializer,
        "create": CreateOrderSerializer,
        "order_payment_status": OrderPaymentCheckSerializer,
    }

    def get_serializer_class(self):
        return self.action_mapper[self.action]

    def valid_prices_and_quantities(self, order_line_ids, order_line):
        items = OdooModel.fetch_by_ids(
            "product.product",
            order_line_ids,
            ["qty_available", "lst_price", "id", "sale_ok"],
            client=self.get_odoo_client(),
        )
        sorted_order_lines = sorted(order_line, key=lambda x: x["product_id"])
        if not items:
            for item in sorted_order_lines:
                item["errors"] = {"exists": False}
            return sorted_order_lines

        sorted_items = sorted(items, key=lambda x: x["id"])
        data = zip(sorted_items, sorted_order_lines)
        missing = []
        if len(sorted_items) < len(sorted_order_lines):
            missing = [
                item
                for item in sorted_order_lines
                if item["product_id"] not in [y["id"] for y in sorted_items]
            ]
            for item in missing:
                item["errors"] = {"exists": False}

        errors = []

        for item, order_item in data:
            order_item["errors"] = {}
            country_threshold = self.request.user.company_id.minimum_threshold

            threshold = lambda x: x - country_threshold
            available_quantity = threshold(float(item["qty_available"]))
            print(
                float(item["qty_available"]),
                ": <----qty_available, ",
                available_quantity,
                "<----available after deducting threshold of ",
                country_threshold,
                "validating order line items-----\n\n\n\n",
            )
            if (
                order_item.get("free_product", False) is False
                and order_item.get("price_list_rule", False) is False
                and (
                    item["id"] == order_item["product_id"]
                    and float(item["lst_price"]) != float(order_item["last_price"])
                )
            ):
                order_item["errors"]["price_changed"] = item["lst_price"]
            if item["id"] == order_item["product_id"] and available_quantity < 0:
                order_item["errors"]["out_of_stock"] = True

            if item["id"] == order_item["product_id"] and not item["sale_ok"]:
                order_item["errors"]["sale_ok"] = False

            if item["id"] == order_item["product_id"] and available_quantity < float(
                order_item["product_uom_qty"]
            ):
                order_item["errors"]["available_quantity"] = available_quantity
            errors.append(order_item) if order_item["errors"] else None
        errors.extend(missing)
        return errors

    def validate_product_trainings(self, user, product_ids):
        temp_ids = user.trainings or []

        domain = ["company_id", "=", user.company_id.odoo_id]
        trainings = Trainings.odoo_fetch([domain], client=self.get_odoo_client())
        allowed_prods = []
        [
            allowed_prods.extend(tr["product_ids"])
            for tr in trainings
            if tr["id"] in temp_ids
        ]
        unaccepted_products = list(set(product_ids) - set(allowed_prods))
        if unaccepted_products:
            return Response(
                {
                    "error": _("unaccepted products for user trainings"),
                    "unaccepted_products": unaccepted_products,
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

    @action(
        methods=["post"],
        detail=False,
        authentication_classes=[OdooAuthentication, OdooSessionAuthentication],
        serializer_class=OrderPaymentCheckSerializer,
    )
    def order_payment_status(self, request):
        from src.payment.models import Payment

        data = request.data
        user = request.user

        serializer = self.get_serializer_class()(data=data)
        serializer.is_valid(raise_exception=True)

        order_id = data["order_id"]
        client = self.get_odoo_client() or switch_country(user.company_id.odoo_id)

        payment = Payment.objects.filter(order_id=order_id).first()
        if payment:
            payment = dict(
                transaction_id=payment.transaction_id,
                amount=payment.amount,
                id=payment.id,
                status=payment.status,
            )

        order = client.get_by_ids(
            "sale.order",
            [order_id],
            fields=["id", "name", "display_name", "invoice_ids", "state", "route_id"],
        )
        order = order[0] if order else {}
        route_id = order.get("route_id")
        route = None
        if route_id:
            route = Orders.get_delivery_info(order["route_id"][0], client=client)
            route = None if "error" in route else route
        if route:
            route = dict(
                delivery_start=route["date"],
                delivery_end=route["end_date"],
                id=route["id"],
            )
        invoice_ids = order["invoice_ids"] if order else None
        invoice = None
        if invoice_ids:
            invoice = client.get_by_ids(
                "account.move",
                invoice_ids,
                fields=[
                    "id",
                    "payment_state",
                    "payment_reference",
                    "invoice_date",
                    "state",
                ],
            )
        return Response(
            {
                "data": dict(
                    order=order, invoice=invoice, payment=payment, route=route
                ),
            },
            status=status.HTTP_200_OK,
        )

    def create(self, request):
        data = request.data
        pricelist_id = self.request.user.company_id.pricelist_id

        ordering_for = None
        if request.user.contact_type == "employee":
            if request.user.ordering_for:
                ordering_for = User.objects.filter(
                    odoo_id=request.user.ordering_for
                ).first()
        user = request.user if ordering_for is None else ordering_for

        serializer = self.get_serializer_class()(data=data)
        serializer.is_valid(raise_exception=True)
        order_line = data["order_line"]

        order_line_ids = [item["product_id"] for item in order_line]
        has_free_products = (
            len([item for item in order_line if item.get("free_product", False)]) > 0
        )
        trainings_check = self.validate_product_trainings(user, order_line_ids)
        if trainings_check:
            return trainings_check

        errors = self.valid_prices_and_quantities(order_line_ids, order_line)

        print("\n\n\nerrors orderline", errors)

        valid_orderlines = [
            orderline
            for orderline in serializer.data["order_line"]
            if orderline[2]["product_id"]
            not in (
                x["product_id"]
                for x in errors
                if x.get("free_product")
                and (x["errors"].get("out_of_stock") or x["errors"].get("sale_ok"))
            )
        ]
        print("\n\n\nvalidated order lines", valid_orderlines)
        try:
            if errors:
                return Response({"errors": errors}, status=status.HTTP_400_BAD_REQUEST)
            min_order_value = user.company_id.minimum_order_value

            total_order_cost = float(data["total"])
            if total_order_cost < float(min_order_value):
                return Response(
                    {"error": _(f"Minimum order value is {min_order_value}")},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            payment_method = serializer.validated_data.get("payment_method")

            if payment_method == "test":
                return self.test_order(serializer)

            client = self.get_odoo_client() or switch_country(user.company_id.odoo_id)

            order_body = {
                "partner_id": user.odoo_id,
                "partner_invoice_id": user.odoo_id,
                "partner_shipping_id": user.odoo_id,
                "order_line": valid_orderlines,
                "pricelist_id": pricelist_id or user.x_price_list_id,
                "picking_policy": "direct",  # direct: when available, one: all at once
                "payment_term_id": user.company_id.payment_term_id,
                "from_product_app": True,
                "has_free_products": has_free_products,
            }
            print("\n\n\n\norder_body", order_body)

            company_defaults = (
                user.company_id.company_defaults
                if isinstance(user.company_id.company_defaults, dict)
                else json.loads(user.company_id.company_defaults)
            )
            print("---------------company_defaults")
            if company_defaults["defaults"]:
                try:
                    default_warehouses = [
                        default
                        for default in company_defaults["defaults"]
                        if "warehouse" in default.get("display_name", "").lower()
                        and compare_user_id(default["condition"], user.odoo_id)
                    ]
                    default_warehouse = default_warehouses.pop()
                    warehouse_id = int(default_warehouse["json_value"])
                    order_body["warehouse_id"] = warehouse_id
                except Exception as e:
                    print(e, "error with order default warehouse")

            sale_order = client.create("sale.order", order_body)
            client.set_route(sale_order)

            order = client.get_by_ids(
                "sale.order",
                [sale_order],
                fields=["name", "display_name", "amount_total", "total_due_readonly"],
            )
            if len(order) == 1:
                order = order[0]
            print(
                f"\n\n\n\===========Order {sale_order} {order} processed--->> Order body",
                order_body,
            )
            order_id = sale_order
            order_name = order.get("name")
            if payment_method == "cash":
                order_status = "Confirmed"
                try:
                    self.confirm_cash_order_and_invoices(order_id)
                except Exception as e:
                    order_status = "Failed"
                    print(f"\n\n\n\===========Cash order processing failed--->>{e}")

                    logger.exception(f"Cash order processing failed: {e}")
                others = {
                    "order_id": order_id,
                    "status": order_status,
                    "order_name": order_name,
                    "message": order_status,
                }

            data = serializer.data
            data.update(
                {
                    "id": order_id,
                    "name": order.get("display_name"),
                    "total": order.get("amount_total"),
                    "sub_total": order.get("amount_total"),
                }
            )

            if ordering_for:
                user = request.user
            return Response(
                {
                    "success": _(f"Order {sale_order} created successfully!"),
                    "data": data,
                },
                status=status.HTTP_200_OK,
            )

        except Exception as e:
            print(Helpers.stacktrace(e, "-----order creation error"))
            return Response(
                {"error": _("Error creating order.")},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    def confirm_cash_order_and_invoices(self, order_id):
        client = self.get_odoo_client()
        print(
            f"\n\n\n\===========starting Order confirmation and invoices processing--->>with client: {client}"
        )
        Orders.confirm_order(order_id, client=client)
        Orders.create_invoice(order_id, client=client)
        Orders.confirm_invoices(order_id, client=client, cash=True)
        print("\n\n\n\===========Order confirmed and invoices processed--->>")

    def test_order(self, serializer):
        data = serializer.data
        order_id = "ST001"
        data.update({"id": 1, "name": order_id})
        others = {"order_id": order_id, "status": "SUCCESSFULL", "order_name": order_id}
        return Response(
            {"success": "Test Order ST001 created successfully!", "data": data},
            status=status.HTTP_200_OK,
        )


def compare_user_id(condition_str, b):
    if condition_str:
        [_, a] = condition_str.split("=")
    return int(a) == int(b)


class OrdersHistoryViewSet(OdooObjectViewSet):
    _model = Orders

    http_method_names = ["get"]
    queryset = Orders.objects.all()
    serializer_class = OrdersSerializer
    pagination_class = OdooPagination
    filter_backends = [OdooParameterFilter, OdooSearchFilter]
    filter_fields = [
        # "str@x_stored_order_status",
        "datetime@create_date",
        "datetime@write_date",
        "datetime@create_date__gt",
        "datetime@create_date__gte",
        "datetime@write_date__gt",
        "datetime@write_date__gte",
        "datetime@create_date__lt",
        "datetime@create_date__lte",
        "datetime@write_date__lt",
        "datetime@write_date__lte",
    ]
    search_fields = ["name", "id"]
    authentication_classes = [OdooAuthentication, OdooSessionAuthentication]

    def filter_queryset(self, queryset):
        domains = super().filter_queryset(queryset)
        queries = [("partner_id", "=", self.request.user.odoo_id)]
        ops = ["|"] * (len(queries) - 1)
        if domains:
            domains.insert(0, "&")
        ops.extend(queries)
        domains.extend(ops)
        return domains
