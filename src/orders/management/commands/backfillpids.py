from django.core.management.base import BaseCommand, CommandError

from src.utils.orders_sync import backfill_price_list_ids


class Command(BaseCommand):
    help = "Sync user pricelist ids"

    def handle(self, *args, **options):
        try:
            backfill_price_list_ids()
        except Exception as err:
            raise err
            raise CommandError('Price List Id sync ::\n"%s" ' % str(err))
        self.stdout.write(self.style.SUCCESS("Successfully synched user pricelist ids"))
