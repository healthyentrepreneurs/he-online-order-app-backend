from django.core.management.base import BaseCommand, CommandError

from src.utils.odoo_cleanup import cleanup_unpaid_quotations


class Command(BaseCommand):
    help = "CLeanup unpaid quotations in odoo"

    def handle(self, *args, **options):
        try:
            cleanup_unpaid_quotations()
        except Exception as err:
            raise err
            raise CommandError('Odoo quotation cleanup fIled ::\n"%s" ' % str(err))
        self.stdout.write(self.style.SUCCESS("Successfully cleaned up Odoo quotations"))
