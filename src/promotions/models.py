from src.core.models import SoftDeletionModel, TimestampedModel
from django.contrib.postgres.fields import ArrayField

from django.db import models


class Promotions(TimestampedModel):
    title = models.CharField(max_length=200, null=False)
    message = models.CharField(max_length=200, null=False)
    company_id = models.ForeignKey(
        "user.Country", on_delete=models.DO_NOTHING, parent_link=True, null=True
    )
    trainings = ArrayField(
        models.IntegerField(),
        default=list,
    )
    schedule_date = models.DateTimeField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    refresh = models.BooleanField(default=False)

    def __str__(self):
        return f"Promotion: {self.title}"
