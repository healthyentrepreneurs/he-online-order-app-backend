from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import (
    CreateListScheduledPromotionNotificationView,
    ScheduledPromotionNotificationDeleteAPIView,
)  # ScheduledPromotionNotificationUpdateAPIView

urlpatterns = [
    path(
        "",
        CreateListScheduledPromotionNotificationView.as_view(),
        name="list-create-promotion",
    ),
    path(
        "<int:pk>",
        ScheduledPromotionNotificationDeleteAPIView.as_view(),
        name="delete-promotion",
    ),
]
