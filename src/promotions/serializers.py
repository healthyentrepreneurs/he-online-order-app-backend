from datetime import datetime, timedelta, timezone
from rest_framework import serializers
from src.user.models import Country
from src.trainings.models import Trainings
from .models import Promotions
from django.conf import settings
from django.utils.timezone import now, localtime
from django.utils.translation import gettext as _


class CreatePromotionSerializer(serializers.ModelSerializer):
    """Serializers for promotion notification creation."""

    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(max_length=255, required=True)
    message = serializers.CharField(max_length=600, required=True)
    country = serializers.PrimaryKeyRelatedField(
        source="company_id", queryset=Country.objects.all(), required=True
    )
    trainings = serializers.ListField(child=serializers.IntegerField(), required=False)

    schedule_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", required=True)
    created_at = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", read_only=True)
    refresh = serializers.BooleanField(required=False)

    @staticmethod
    def validate_schedule_date(schedule_date):
        n = localtime(now())
        schedule_date = localtime(schedule_date)
        if n > schedule_date:
            raise serializers.ValidationError(
                _("schedule date should be later than now")
            )

        return schedule_date

    def validate(self, data):
        validated_data = super().validate(data)
        title = validated_data.get("title", None)
        message = validated_data.get("message", None)
        country = validated_data.get("company_id", None)
        trainings = validated_data.get("trainings", None)
        schedule_date = validated_data.get("schedule_date", None)
        refresh = validated_data.get("refresh", None)

        # CreatePromotionSerializer.scheduled_date_validation(schedule_date)
        return {
            "title": title,
            "message": message,
            "company_id": country,
            "trainings": trainings,
            "schedule_date": schedule_date,
            "refresh": refresh,
        }

    @staticmethod
    def validate_trainings(trainings):
        try:
            invalid_trainings = [
                int(val) for val in trainings if val not in taining_ids()
            ]
            if invalid_trainings and len(invalid_trainings) > 0:
                raise serializers.ValidationError(
                    _("please use valid training template ids")
                )
        except ValueError:
            raise serializers.ValidationError(
                _("trainings should be a list of integers")
            )
        return trainings

    class Meta:
        model = Promotions
        fields = [
            "id",
            "title",
            "message",
            "trainings",
            "country",
            "company_id",
            "schedule_date",
            "created_at",
            "refresh",
        ]

    def create(self, validated_data):
        return Promotions.objects.create(**validated_data)


def taining_ids():
    return (obj.odoo_id for obj in Trainings.objects.all())


class ScheduledPromotionNotificationSerializer(serializers.ModelSerializer):
    """Serializers for promotion notification retrieve and list."""

    title = serializers.CharField(read_only=True)
    message = serializers.CharField(read_only=True)
    country = serializers.SerializerMethodField()
    trainings = serializers.ListField(child=serializers.IntegerField(), required=False)

    schedule_date = serializers.DateTimeField(
        # format="%Y-%m-%dT%H:%M:%S",
        read_only=True
    )
    created_at = serializers.DateTimeField(
        # format="%Y-%m-%dT%H:%M:%S",
        read_only=True
    )

    class Meta:
        model = Promotions
        fields = [
            "id",
            "title",
            "message",
            "trainings",
            "country",
            "company_id",
            "schedule_date",
            "created_at",
            "refresh",
        ]

    def get_country(self, obj):
        country = obj.company_id
        return {"name": country.name, "odoo_id": country.odoo_id, "id": country.id}


class ScheduledPromotionNotificationUpdateSerializers(serializers.Serializer):
    """Serializers for promotion notification update."""

    title = serializers.CharField(max_length=255, required=False)
    message = serializers.CharField(max_length=600, required=False)
    country = serializers.PrimaryKeyRelatedField(
        source="company_id", queryset=Country.objects.all(), required=False
    )
    trainings = serializers.ListField(child=serializers.IntegerField(), required=False)

    schedule_date = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S", required=False
    )
    refresh = serializers.BooleanField(required=False)

    @staticmethod
    def validate_schedule_date(schedule_date):
        n = localtime(now())
        schedule_date = localtime(schedule_date)
        if n > schedule_date:
            raise serializers.ValidationError(
                _("schedule date should be later than now")
            )

        return schedule_date

    @staticmethod
    def validate_trainings(trainings):
        try:
            invalid_trainings = [
                int(val) for val in trainings if val not in taining_ids()
            ]
            if invalid_trainings and len(invalid_trainings) > 0:
                raise serializers.ValidationError(
                    _("please use valid training template ids")
                )
        except ValueError:
            raise serializers.ValidationError(
                _("trainings should be a list of integers")
            )
        return trainings

    class Meta:
        model = Promotions
        fields = [
            "id",
            "title",
            "message",
            "trainings",
            "country",
            "company_id",
            "schedule_date",
            "refresh",
        ]
