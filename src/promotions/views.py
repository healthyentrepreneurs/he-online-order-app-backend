from datetime import datetime

from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)

from django_q.models import Schedule
from django_q.tasks import schedule

from rest_framework.permissions import (
    IsAdminUser,
)
from rest_framework.response import Response
from rest_framework import status

from .serializers import (
    CreatePromotionSerializer,
    Promotions,
    ScheduledPromotionNotificationSerializer,
    ScheduledPromotionNotificationUpdateSerializers,
)


class CreateListScheduledPromotionNotificationView(ListCreateAPIView):
    """
    post:
    Creates a scheduled notification for a given usergroup.
    """

    permission_classes = (IsAdminUser,)
    serializer_class = ScheduledPromotionNotificationSerializer
    queryset = Promotions.objects.all()

    def post(self, request):
        data = request.data

        serializer = CreatePromotionSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        serializer.save()
        create_tasks(
            serializer.validated_data["schedule_date"],
            serializer.validated_data.get("trainings", []),
            serializer.validated_data["title"],
            serializer.validated_data["message"],
            serializer.validated_data["refresh"],
        )
        return Response(
            {"message": "Promotion created successfully"},
            status=status.HTTP_200_OK,
        )


class ScheduledPromotionNotificationDeleteAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Promotions.objects.all()
    permission_classes = (IsAdminUser,)
    serializer_class = ScheduledPromotionNotificationSerializer
    model = Promotions

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        super().destroy(request, *args, **kwargs)
        kill_tasks(instance.trainings, instance.title)
        return Response(
            {"message": "Promotion successfully deleted"}, status=status.HTTP_200_OK
        )

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = ScheduledPromotionNotificationUpdateSerializers(data=request.data)
        if serializer.is_valid(raise_exception=True):
            for key, value in serializer.data.items():
                setattr(instance, key, value)
            instance.save()
            kill_tasks(instance.trainings, instance.title)
            create_tasks(
                instance.schedule_date,
                instance.trainings,
                instance.title,
                instance.message,
                instance.refresh,
            )
            return Response(
                {
                    "success": True,
                    "message": "Promotion Updated successfully.",
                },
                status=status.HTTP_200_OK,
            )

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def create_tasks(schedule_date, training_ids, title, message, refresh):
    schedules = []
    print("\n\n\n\n Schhedule date--1----->>", schedule_date, type(schedule_date))
    current_datetime = datetime.now()
    stamp = current_datetime.strftime("%Y-%m-%d %H:%M:%S")
    if training_ids:
        for training_id in training_ids:
            training_id = int(training_id)
            stamp = training_id
            sche = Schedule.objects.filter(
                name=f"send_promotion_notification:{stamp}:{title}"
            ).first()
            if sche:
                sche.delete()
            sche = create_schedule(stamp, title, message, refresh, schedule_date)
            schedules.append(sche)
    else:
        kill_tasks([], title)
        sche = create_schedule(stamp, title, message, refresh, schedule_date)
        schedules.append(sche)
    print("\n\n\n Scheduled promotion notificaiton created---=>", sche, schedules)

    return schedules


def create_schedule(stamp, title, message, refresh, schedule_date):
    return schedule(
        "src.utils.promotions.send_promotion_notificaiton",
        stamp,
        title,
        message,
        refresh,
        name=f"send_promotion_notification:{stamp}:{title}",
        schedule_type=Schedule.ONCE,
        next_run=schedule_date,
        repeats=1,
    )


def kill_tasks(training_ids, title):
    deleted_schedules = []
    sche = None
    if training_ids:
        for training_id in training_ids:
            training_id = int(training_id)
            sche = Schedule.objects.filter(
                name=f"send_promotion_notification:{training_id}:{title}"
            ).all()
    else:
        sche = Schedule.objects.filter(name__contains=title).all()
    if sche:
        deleted_schedules.extend(list(sche))
        sche.delete()
    print("\n\n\n Scheduled promotion notificaiton deleted---=>", deleted_schedules)
    return deleted_schedules
