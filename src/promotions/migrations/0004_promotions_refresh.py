# Generated by Django 2.2.10 on 2021-09-18 06:17

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("promotions", "0003_auto_20210915_2118"),
    ]

    operations = [
        migrations.AddField(
            model_name="promotions",
            name="refresh",
            field=models.BooleanField(default=False),
        ),
    ]
