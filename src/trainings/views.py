import logging, json
from drf_yasg.utils import swagger_auto_schema

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from src.core.filters import OdooSearchFilter, OdooParameterFilter
from src.core.pagination import OdooPagination
from src.djangodoo.models import OdooModel
from src.djangodoo.authentication import OdooAuthentication, OdooSessionAuthentication
from src.djangodoo.views import OdooObjectViewSet
from django.shortcuts import get_list_or_404, get_object_or_404
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

from src.trainings.tasks import sync_trainings as sync


from .models import Trainings, FreeProduct
from src.products.models import Category, Product
from .serializers import (
    TrainingsSerializer,
    TrainingProductsSerializer,
    TrainingProductsUpdateSerializer,
    SyncTrainingsSerializer,
    Country,
)
from fcm_django.api.rest_framework import FCMDevice
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from src.utils.tasks import send_notifications
from src.utils.helpers import Helpers


logger = logging.getLogger(__name__)
User = get_user_model()


class TrainingsViewSet(OdooObjectViewSet):
    _model = Trainings

    http_method_names = ["get", "head", "put"]
    queryset = Trainings.objects.all()
    pagination_class = OdooPagination
    filter_backends = [OdooParameterFilter, OdooSearchFilter]
    filter_fields = ["str@id", "datetime@x_name", "datetime@display_name"]
    search_fields = ["x_name", "id", "display_name"]
    authentication_classes = [OdooAuthentication, OdooSessionAuthentication]

    action_mapper = {
        "list": TrainingsSerializer,
        "retrieve": TrainingProductsSerializer,
        "update": TrainingProductsUpdateSerializer,
        "sync_trainings": SyncTrainingsSerializer,
    }

    def get_serializer_class(self):
        return self.action_mapper[self.action]

    def get_odoo_client(self):
        return (
            self.request.user.odoo_client
            or settings.odoo[self.request.user.company_id.odoo_id]
        )

    @swagger_auto_schema(method="get", query_serializer=SyncTrainingsSerializer)
    @action(
        methods=["get"],
        detail=False,
        permission_classes=[IsAdminUser],
    )
    def sync_trainings(self, request):
        self.filter_backends = None
        self.pagination_class = None
        redis_cache = settings.redis_cache
        last_run = request.GET.get("last_run", None)

        data = Helpers.get_cached_data(redis_cache, "sync_trainings") or {}
        running = data.get("running", False)

        if last_run and last_run.lower().strip() != "false":
            data = data if data else "No sync run yet"
            return Response(
                {"message": "Last trainings sync run", "data": data},
                status=status.HTTP_200_OK,
            )

        if running:
            return Response(
                {"message": "Sync already running", "data": data},
                status=status.HTTP_200_OK,
            )
        task = sync.delay()
        redis_cache.set(
            "sync_trainings",
            bytes(json.dumps({"running": True, "task_id": task.id}), "utf-8"),
        )

        return Response(
            {"message": "Sync running in background", "task_id": task.task_id},
            status=status.HTTP_200_OK,
        )

    def update(self, request, pk=None):
        try:
            data = request.data

            client = self.get_odoo_client()
            record = client.get_by_ids(self._model._odoo_model, [pk])
            if not record:
                return Response(
                    {"error": _(f"invalid training id {pk}")},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            serializer = self.get_serializer_class()(data=dict(data))

            if serializer.is_valid():
                free_products = serializer.validated_data.get("free_product_ids", [])
                free_product_ids = [
                    int(product["odoo_id"]) for product in free_products
                ]
                if len(free_products) > 0:
                    product_details = Trainings.fetch_by_ids(
                        self.request.user.company_id.odoo_id,
                        free_product_ids,
                        client=client,
                    )
                    [
                        product.update({"name": details["name"]})
                        for product in free_products
                        for details in product_details
                        if details["id"] == product["odoo_id"]
                    ]

                    FreeProduct.objects.filter(training=pk).all().delete()
                    [FreeProduct.objects.create(**item) for item in free_products]
                    client.write(
                        self._model._odoo_model,
                        pk,
                        {"free_products": [(6, 0, free_product_ids)]},
                    )

                devices = FCMDevice.objects.filter(user__training=pk)
                send_notifications(
                    devices,
                    title=_("Products list updated"),
                    body=_(
                        "Products have been updated. Press here to refresh application."
                    ),
                    free_products=free_product_ids,
                )

                return Response(
                    {
                        "success": "Products updated successfully for training",
                        "data": serializer.validated_data,
                    },
                    status=status.HTTP_200_OK,
                )
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(Helpers.stacktrace(e, "-----user group update error"))
