from django.db import models
from src.core.models import TimestampedModel, SoftDeletionModel
from src.djangodoo.models import OdooModel
from src.products.models import Product
from django.conf import settings
from src.djangodoo.utils import switch_country
from src.user.models import Country
from rest_framework import serializers
import jsonfield


class FreeProduct(TimestampedModel):
    expiry_date = models.DateTimeField(null=True)
    min_order_value = models.IntegerField(default=0)
    odoo_id = models.IntegerField(null=False, unique=False)
    name = models.CharField(max_length=1200, null=False)
    training = models.IntegerField(null=False, unique=False)

    class Meta:
        unique_together = (
            "odoo_id",
            "training",
        )


class Trainings(TimestampedModel, SoftDeletionModel, OdooModel):
    _odoo_model = "he.training.template"
    _odoo_fields = [
        "__last_update",
        "id",
        "create_date",
        "create_uid",
        "display_name",
        "write_date",
        "company_id",
        "name",
        # "template_id",
        "product_ids",
        # "trainer_id",
        "free_products",
        "active",
        "description",
    ]
    name = models.CharField(max_length=100)
    temp_product_ids = jsonfield.JSONField(default=[])

    @classmethod
    def products_fetch(cls, company_id, limit=None, offset=0, **kwargs):
        domain = [["company_id", "=", company_id]]
        client = switch_country(company_id)
        product_fields = ["name", "id", "code"]
        return Product.odoo_fetch(
            domain, client, limit, offset, fields=product_fields, **kwargs
        )

    @classmethod
    def fetch_by_ids(cls, company_id, ids, client=None):
        client = client or switch_country(company_id)
        product_fields = ["name", "id", "code"]
        return Product.fetch_by_ids(
            "product.product", ids, product_fields, client=client
        )

    @classmethod
    def product_ids(cls, id, company_id):
        fields = ["product_ids"]
        return cls.odoo_get(id, fields=fields, client=switch_country(company_id))

    @classmethod
    def fetch_free_products(cls, id, company_id):
        # switch_country(company_id)
        fields = ["free_products"]
        return cls.odoo_get(id, fields=fields, client=switch_country(company_id))
