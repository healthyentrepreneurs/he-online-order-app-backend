from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import TrainingsViewSet

router = routers.DefaultRouter()
router.register(r"", TrainingsViewSet)

urlpatterns = [
    path(r"", include(router.urls)),
]
