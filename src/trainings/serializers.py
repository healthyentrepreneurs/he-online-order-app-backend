from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from src.products.models import Product
from src.products.serializers import ProductSerializer
from src.trainings.models import Trainings, FreeProduct
from src.user.models import Country

from datetime import datetime
from django.conf import settings
from datetime import timezone, timedelta
from django.utils.timezone import now, localtime
from django.utils.translation import gettext as _


class FreeProductSerializer(serializers.Serializer):
    expiry_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", required=True)
    min_order_value = serializers.IntegerField(required=False)
    odoo_id = serializers.IntegerField(required=True)
    name = serializers.CharField(read_only=True)
    training = serializers.IntegerField(required=True)

    @staticmethod
    def validate_expiry_date(expiry_date):
        print(datetime.now().day, expiry_date.day)
        if datetime.now().date() > expiry_date.date():
            raise serializers.ValidationError(
                {"error": _("expiry_date has to be later than today")}
            )
        return expiry_date


class FreeProductUpdateSerializer(FreeProductSerializer):
    expiry_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", required=False)
    odoo_id = serializers.IntegerField(required=False)
    name = serializers.CharField(read_only=True)
    training = serializers.IntegerField(required=False)


class TrainingsSerializer(serializers.Serializer):
    created_at = serializers.DateTimeField(source="create_date", required=False)
    updated_at = serializers.DateTimeField(source="write_date", required=False)
    name = serializers.CharField(required=False)
    training_id = serializers.IntegerField(source="id", required=False)
    company = serializers.SerializerMethodField(required=False)
    display_name = serializers.CharField(required=False)
    free_products = serializers.SerializerMethodField(required=False)
    product_ids = serializers.JSONField(required=False)

    class Meta:
        model = Trainings
        fields = "__all__"

    def get_company(self, obj):
        company = obj["company_id"]
        if isinstance(company, list):
            print(company)
            return {"id": company[0], "name": company[1]}
        return {"id": company.id, "name": company.name}

    def get_free_products(self, obj):
        training = None
        if hasattr(obj, "id"):
            training = obj.id
        elif self.context.get("view"):
            training = obj["id"]

        serializer = FreeProductSerializer(
            FreeProduct.objects.filter(training=training).all(),
            many=True,
        )
        return serializer.data


class SyncTrainingsSerializer(serializers.Serializer):
    last_run = serializers.BooleanField(
        required=False, help_text="Flag to fetch data from last run"
    )


class TrainingProductsSerializer(TrainingsSerializer):
    products = serializers.SerializerMethodField(required=False)

    def get_products(self, obj):
        try:
            _id = obj["company_id"][0]
        except TypeError:
            _id = obj.company_id.id

        return Trainings.products_fetch(_id)


class TrainingProductsUpdateSerializer(TrainingProductsSerializer):
    free_product_ids = serializers.ListField(
        child=FreeProductSerializer(), required=False, write_only=True
    )
