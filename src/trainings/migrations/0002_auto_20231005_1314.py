# Generated by Django 2.2.10 on 2023-10-05 10:14

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):
    dependencies = [
        ("trainings", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="trainings",
            name="blacklisted_ids",
        ),
        migrations.AddField(
            model_name="trainings",
            name="temp_product_ids",
            field=jsonfield.fields.JSONField(default=[]),
        ),
    ]
