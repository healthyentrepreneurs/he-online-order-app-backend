import json
from celery import shared_task
from datetime import datetime
from django.conf import settings
from src.utils.helpers import Helpers
from celery_progress.backend import ProgressRecorder


from src.utils.sync_users import sync_company_trainings


@shared_task(bind=True, name="sync_trainings")
def sync_trainings(self):
    """Syncs che users from odoo to application."""
    redis_cache = settings.redis_cache

    try:
        progress_recorder = ProgressRecorder(self)
        sync_company_trainings(progress_recorder=progress_recorder)
    except Exception as err:
        print(Helpers.stacktrace(err, "-----odoo list error"))
        redis_cache.delete("sync_trainings")
        last_run = {
            "last_sync_time": str(datetime.now()),
            "success": False,
            "error": str(err),
            "running": False,
        }
        print("\n\n\nlast run----->", last_run)
        redis_cache.set("sync_trainings", bytes(json.dumps(last_run), "utf-8"))
