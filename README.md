# HE Application
This repository contains the implementation of the Health entrepreneurs application.

## Brief Description
An application that will be used by CHE to for sales of medicines

## Getting Started

These instructions below will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
- git
- python 3.7
- postgres 9.6
- virtualenv/venv

### Installing

1. Create a new virtual environment.
    If using vanilla [virtualenv](https://virtualenv.pypa.io/en/latest/):
    ```
    $ virtualenv venv
    $ source venv/bin/activate
    ```

2. Clone this repository using git to a local directory where you want to run the project.

3. In the root directory of the project, install the project dependencies stored in the requirements.txt file.
    ```
    $ pip install -r requirements.txt
    ```
4. Log into your postgres server, create a database called hehe, create a user, password and grant privileges to the database.
    ```
    sudo -u postgres psql

    postgres=# create database hedb;

    postgres=# create user heuser with encrypted password 'youpassword';

    postgres=# grant all privileges on database hedb to heuser;
    ```
5. Copy the `.env_example` file, rename it to `.env` and edit it to include your own environment variables.

6. Run `source .env` to apply the environment variables

7. Run the migrations with:
    ```
    $ python manage.py migrate
    ```
8. Load fixtures with the command
    ```
    python manage.py loaddata src/*/fixtures/*.json
    ```
9.  Start the development server:
    ```
    $ python manage.py runserver
    ```
    You can now access the application at http://localhost:8000 to register, login and go over the various API methods.

10.  Start the background server:
    ```
    $ python manage.py qcluster

11. backfill userwith clusters and db with clusters and districts
    $ python manage.py backfilluserclusters
    ```

### And coding style tests

This project uses Flake8 as the code style to ensure code syntax matches through out development

```
$ tox 
```

 ### Monitoring celery tasks
 - run `celery -A config flower --port=5566`
 - go to `localhost:5566`


## Deployment
-
## Built With

* [Django](https://www.djangoproject.com/) - The web framework used
