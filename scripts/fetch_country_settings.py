import os
import sys

import psycopg2

DATABASE_URL = os.getenv("DATABASE_URL")
conn = psycopg2.connect(DATABASE_URL)


conn = None

try:
    print("Backing Up company settings---------------->>")

    conn = psycopg2.connect(DATABASE_URL)

    cursor = conn.cursor()

    outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(
        "select * from user_country"
    )

    with open("company_settings.csv", "w") as f:
        cursor.copy_expert(outputquery, f)
    print(
        "Company settings backed up---------------->>",
        cursor.execute("select * from user_country"),
    )


except psycopg2.DatabaseError as e:
    print(f"Error {e}")
    sys.exit(1)

finally:
    if conn:
        conn.close()
    if cursor:
        cursor.close()
