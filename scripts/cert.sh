# generate csr
openssl req -new -newkey rsa:2048 -nodes -out certs/star_heorderapp_online.csr -keyout certs/star_heorderapp_online.key -subj "/C=UG/ST=Kampala/L=Kampala/O=Healthy Entrepreneurs Ltd./OU=IT/CN=*.heorderapp.online"

# generate public key from key
openssl rsa -in certs/star_heorderapp_online.key -pubout>certs/star_heorderapp_online.pub

# self sign certificate
openssl x509 -req -days 365 -in certs/star_heorderapp_online.csr -signkey certs/star_heorderapp_online.key -out certs/star_heorderapp_online_public.crt
