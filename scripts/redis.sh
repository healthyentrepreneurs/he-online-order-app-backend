#!/bin/bash
if [ ! -d redis-stable/src ]; then
   curl -O http://download.redis.io/redis-stable.tar.gz
   tar xvzf redis-stable.tar.gz
   rm redis-stable.tar.gz
fi

# cd redis-stable
# make
# src/redis-server

pip install -U "redis-cli"

a=$(redis-cli -p 6379 PING)
if [ "$a" = "PONG" ]
then
    echo 'Already running'
else
    b=$(systemctl start redis)
    echo $b
fi
