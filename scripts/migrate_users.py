import psycopg2

import os, sys

import django
from django.conf import settings
from psycopg2.extras import execute_values


DATABASE_URL = os.getenv("DATABASE_URL")


def client(company_id):
    try:
        client = settings.odoo[company_id]
    except KeyError:
        client = None
    return client


id_tracker = {}

conn = None


def migrate_users():
    from src.user.models import Country, User

    try:
        print("migrating users---------------->>")

        conn = psycopg2.connect(DATABASE_URL)
        cursor = conn.cursor()

        countries_data = list(Country.objects.values_list("odoo_id", "country_code"))

        update_data = []

        update_query = """UPDATE user_user 
                            SET odoo_id = update_payload.odoo_id 
                            FROM (VALUES %s) AS update_payload (odoo_id, phone) 
                            WHERE user_user.phone_number = update_payload.phone"""

        phone_number = (
            lambda phone, country_code: User.objects.normalize_phone(
                phone, country_code
            )
            if phone
            else None
        )

        # odoo_phone_format = lambda phone: f"{phone[:4]} {phone[4:7]} {phone[7:]}"
        odoo_phone_format = lambda phone: f"0{phone[4:7]}{phone[7:]}"
        print("\n\n\n\n\n\n", countries_data)

        for country in countries_data:
            company_id, country_code = country
            if odoo_client := client(company_id):
                country_users = User.objects.filter(
                    company_id_id=company_id, is_staff=False
                )

                new_data = User.odoo_fetch(
                    [
                        "&",
                        [
                            "phone",
                            "in",
                            [
                                odoo_phone_format(str(usr.phone_number))
                                for usr in country_users
                            ],
                        ],
                        ["company_id", "=", company_id],
                    ],
                    client=odoo_client,
                    fields=["id", "phone"],
                )

                update_data.extend(
                    [
                        (i["id"], phone_number(i["phone"], country_code))
                        for i in new_data
                    ]
                )

        print("\n\n\n\nn----update_data", len(update_data))
        cursor.execute("UPDATE user_user SET odoo_id = null;")
        execute_values(cursor, update_query, update_data)

        # Commit the changes
        conn.commit()

    except psycopg2.DatabaseError as e:
        print(f"Error {e}")
        sys.exit(1)

    finally:
        if conn:
            conn.close()
        if cursor:
            cursor.close()


if __name__ == "__main__":
    current_file_directory = os.path.dirname(os.path.abspath(__file__))
    sys.path.insert(0, current_file_directory)
    django.setup()
    migrate_users()
