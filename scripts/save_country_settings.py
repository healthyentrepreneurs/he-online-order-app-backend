import os
import json
import sys

import csv

csv.field_size_limit(sys.maxsize)

import psycopg2

DATABASE_URL = os.getenv("DATABASE_URL")
conn = psycopg2.connect(DATABASE_URL)


conn = None

try:
    print("Saving company settings---------------->>")

    conn = psycopg2.connect(DATABASE_URL)

    cursor = conn.cursor()
    update_query = """ UPDATE user_country
            SET phone_number_prefix = %s ,
            minimum_threshold = %s ,
            minimum_order_value = %s
            WHERE id = %s"""
    updated_rows = 0

    with open("company_settings.csv", "r") as backup_csv:
        lines = csv.reader(backup_csv)
        next(lines)

        for line in lines:
            print(
                (line[15], line[13], line[14], line[0]),
                "-------------------------------\n\n",
            )
            cursor.execute(update_query, (line[15], line[13], line[14], line[0]))
            updated_rows += cursor.rowcount
            conn.commit()
    cursor.execute("select * from user_country;")
    os.remove("company_settings.csv")

    print(
        f"Company settings maintained with release-----update {updated_rows} rows---->",
        cursor.fetchall(),
    )


except psycopg2.DatabaseError as e:
    print(f"Error {e}")
    sys.exit(1)

finally:
    if conn:
        conn.close()
    if cursor:
        cursor.close()
