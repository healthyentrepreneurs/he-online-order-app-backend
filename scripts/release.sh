bash scripts/cert.sh
python scripts/fetch_country_settings.py
python manage.py migrate
python manage.py collectstatic --no-input
# python manage.py loaddata src/*/fixtures/*.json
python scripts/save_country_settings.py
python manage.py load_products
python manage.py update_companies
python manage.py sync_company_trainings

echo "Release Finished"