import psycopg2
import os, sys

import django
import random


from psycopg2.extras import execute_values

import psycopg2.extensions
import logging

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)
logging.basicConfig(level=logging.DEBUG)


DATABASE_URL = os.getenv("DATABASE_URL")


def generate_random_number():
    return random.randint(10, 999)


table_mapper = [
    {
        "table": "user_user",
        "f_key": "user_user_company_id_id_ddb37cf1_fk_user_country_id",
        "field": "company_id_id",
    },
    {
        "table": "user_country_payment_methods",
        "f_key": "user_country_payment_country_id_5b87e286_fk_user_coun",
        "field": "country_id",
    },
    {
        "table": "districts_districts",
        "f_key": "districts_districts_company_id_id_99e469c8_fk_user_country_id",
        "field": "company_id_id",
    },
    {
        "table": "clusters_clusters",
        "f_key": "clusters_clusters_company_id_id_6ab8f37a_fk_user_country_id",
        "field": "company_id_id",
    },
    {
        "f_key": "payment_payment_company_id_id_7e7b129a_fk_user_country_id",
        "table": "payment_payment",
        "field": "company_id_id",
    },
    {
        "f_key": "promotions_promotions_company_id_id_a529eabd_fk_user_country_id",
        "table": "promotions_promotions",
        "field": "company_id_id",
    },
]

# [('clusters_clusters_company_id_id_6ab8f37a_fk_user_country_id', 'clusters_clusters'), ('districts_districts_company_id_id_99e469c8_fk_user_country_id', 'districts_districts'), ('payment_payment_company_id_id_7e7b129a_fk_user_country_id', 'payment_payment'), ('promotions_promotions_company_id_id_a529eabd_fk_user_country_id', 'promotions_promotions'), ('user_country_payment_country_id_5b87e286_fk_user_coun', 'user_country_payment_methods'), ('user_groups_promotio_company_id_id_adf871c7_fk_user_coun', 'user_groups_promotionalmessage'), ('user_user_company_id_id_ddb37cf1_fk_user_country_id', 'user_user'), ('clusters_clusters_company_id_fk', 'clusters_clusters')]


drop_fk_constraint = (
    lambda table, f_key: f"""
    ALTER TABLE {table}
    DROP CONSTRAINT {f_key};
"""
)
add_fk_constraint = (
    lambda table, f_key, field: f"""
    ALTER TABLE {table}
    ADD CONSTRAINT {f_key}
    FOREIGN KEY ({field}) REFERENCES user_country(id);
"""
)


def handle_country_constraints(create=False):
    print(f"\n\n\n\n\n\n----handle_country_constraints----{create}")
    conn = psycopg2.connect(DATABASE_URL)
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    print(
        f"\n\n\n\n\n\n----conn--{conn}----cursor-{cursor}----create is True?{create is True}"
    )
    if create is True:
        print(f"\n\n\n\n\n\n----handle_country_constraints--before--{create}")
        cursor.execute("ALTER TABLE user_country ADD PRIMARY KEY (id);")
        cursor.execute(
            "ALTER TABLE user_country ADD CONSTRAINT user_country_odoo_id_key UNIQUE (odoo_id);"
        )
        print(f"\n\n\n\n\n\n----handle_country_constraints--create--{create}")
    else:
        print(f"\n\n\n\n\n\n----handle_country_constraints--before-222-{create}")

        cursor.execute(
            "ALTER TABLE user_country DROP CONSTRAINT IF EXISTS user_country_pkey CASCADE;"
        )
        cursor.execute(
            "ALTER TABLE user_country DROP CONSTRAINT user_country_odoo_id_key CASCADE;"
        )
        print(f"\n\n\n\n\n\n----handle_country_constraints- no-create--{create}")

    conn.commit()
    cursor.close()
    conn.close()
    print(f"\n\n\n\n\n\n----handle_country_constraints--done--{conn}")


tables_to_drop = ["user_groups_promotionalmessage"]


update_company_id_in_table = (
    lambda table, field, new_id, old_id: f"UPDATE {table} SET {field} = {new_id} WHERE {field} = {old_id};"
)

current_data = "SELECT * FROM user_country;"


id_tracker = {}

conn = None


def match_records(new_data, countries):
    conn = psycopg2.connect(DATABASE_URL)
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()

    grouped = []
    exists = []
    for record in new_data:
        if x := [country for country in countries if country[8] == record["name"]]:
            print(
                f"\n\n\n\n\n\n----match_records----{record['name']}---{x[0][0]}-{x[0][4]}-"
            )
            grouped.append((x[0], record))
            exists.append(x[0])

    if ids := [record[0] for record in countries if record not in exists]:
        ids = ", ".join(map(str, ids))
        print(f"\n\n\n\n\n\n----ids to delete----{ids} -----")

        cursor.execute(f"DELETE FROM user_country WHERE id in ({ids});")
    conn.commit()
    cursor.close()
    conn.close()

    print(
        f"\n\n\n\n\n\n----match_records---2222drop---\n\n\n\n\n\===++++++-{[x[0] for x in exists]}--"
    )
    return grouped


def update_tracker():
    from src.user.models import Country

    conn = psycopg2.connect(DATABASE_URL)
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()

    cursor.execute("DELETE FROM user_country_payment_methods;")

    cursor.execute(current_data)
    countries_data = cursor.fetchall()
    print(f"\n\n\n\n\n\n-------{[c[0] for c in countries_data]}")
    new_data = Country.odoo_fetch([])

    grouped = match_records(new_data, countries_data)
    covered = []

    for country in new_data:
        if old_record := [data[0] for data in grouped if data[1] == country]:
            print(f"\n\n\n\n\n\n----new_rcord hrer----{country['name']}")
            id_tracker[old_record[0][0]] = country["id"]
            covered.append(country)

        if country not in covered:
            cursor.execute(
                """
                INSERT INTO user_country (created_at, updated_at, payment_method_id, payment_term_id, access_id_id, minimum_threshold, minimum_order_value, phone_number_prefix, last_update, id, name, odoo_id, currency, country_code, company_name, currency_id,trainings, company_defaults, payment_terms) 
                VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,1, 1, null, 15, 0,+256, null, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                RETURNING ID;""",
                (
                    country["id"],
                    country["name"],
                    country["id"],
                    country["currency_id"][1],
                    country["country_code"],
                    country["name"],
                    country["currency_id"][0],
                    "{}",
                    "{}",
                    "{}",
                ),
            )
    conn.commit()
    cursor.close()
    conn.close()
    return id_tracker


def update_user_country():
    conn = psycopg2.connect(DATABASE_URL)
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    for item in table_mapper:
        update_data = []
        for k, v in id_tracker.items():
            print(
                f"\n\n\n\nn\++++=====before====results---{k}---{v}---> SELECT * FROM {item['table']} WHERE {item['field']} = {k};"
            )
            cursor.execute(
                f"SELECT id FROM {item['table']} WHERE {item['field']} = {k};"
            )
            results = cursor.fetchall()
            print(f"\n\n\n\nn\++++=========results---{len(results)}")
            update_data.extend([(v, result[0]) for result in results])

        print(f"\n\n\n\n\n\n----update_data----{len(update_data)}")
        update_query = (
            lambda table, field: f"""UPDATE {table} 
                        SET {field} = update_payload.new_val 
                        FROM (VALUES %s) AS update_payload (new_val, id) 
                        WHERE {table}.id = update_payload.id"""
        )
        execute_values(cursor, update_query(item["table"], item["field"]), update_data)
        conn.commit()
    cursor.close()
    conn.close()


def update_country_pk(id_tracker):
    conn = psycopg2.connect(DATABASE_URL)
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()

    update_query = """
    UPDATE user_country
    SET
        id = CASE
    """

    for k, v in sorted(id_tracker.items()):
        update_query += f"""
            WHEN id = {k} THEN {v}
        """
    update_query += """
        ELSE id
      END,
    odoo_id = CASE
    """
    for k, v in sorted(id_tracker.items()):
        update_query += f"""
            WHEN id = {k} THEN {v}
        """
    update_query += f"""
        ELSE odoo_id
      END
    WHERE id in ({', '.join(map(str,sorted(id_tracker.keys())))});
    """
    print(update_query)
    cursor.execute(update_query)
    conn.commit()

    cursor.execute("select id, odoo_id from user_country;")
    print(f"\n\n\n\n\n\n----update_country_pk----{cursor.fetchall()}")
    conn.commit()
    cursor.close()
    conn.close()


def update_records():
    conn = psycopg2.connect(DATABASE_URL)
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()

    for item in table_mapper:
        print(f"\n\n\n\n\n\n----addded_fk_const----{item}")
        if item["table"] in ["user_country_payment_methods"]:
            cursor.execute(
                f"ALTER TABLE {item['table']} ALTER COLUMN {item['field']} SET NOT NULL;"
            )
        cursor.execute(add_fk_constraint(item["table"], item["f_key"], item["field"]))
    conn.commit()
    cursor.close()
    conn.close()


def drop_constraints():
    print(f"\n\n\n\n\n\n----drop_constraints----")

    conn = psycopg2.connect(DATABASE_URL)
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    for item in table_mapper:
        print(f"\n\n\n\n\n\n----dropped_fk_const----{item}")
        if item["table"] in ["user_country_payment_methods"]:
            cursor.execute(
                f"ALTER TABLE {item['table']} ALTER COLUMN {item['field']} DROP NOT NULL;"
            )
    conn.commit()
    cursor.close()
    conn.close()


def migrate_countries():
    from time import sleep

    print("migrating company ids primary keys---------------->>")
    try:
        id_tracker = update_tracker()

        print(f"\n\n\n\n\n\n----id_tracker--hereee--{id_tracker}")
        handle_country_constraints()

        update_user_country()

        print(f"\n\n\n\n\n\n----id_tracker----{id_tracker}")

        drop_constraints()
        update_country_pk(id_tracker)
        sleep(10)
        handle_country_constraints(create=True)
        update_records()

    except psycopg2.DatabaseError as e:
        print(f"Error {e}")
        sys.exit(1)


if __name__ == "__main__":
    current_file_directory = os.path.dirname(os.path.abspath(__file__))
    sys.path.insert(0, current_file_directory)

    django.setup()
    migrate_countries()

# def drop_related_constraints():
#     try:
#         conn = psycopg2.connect(DATABASE_URL)
#         conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
#         cursor = conn.cursor()
#         for table in tables_to_drop:
#             cursor.execute(f"DROP TABLE IF EXISTS {table};")
#         cursor.execute("""
#             SELECT conname AS constraint_name, conrelid::regclass AS table_name
#             FROM pg_constraint
#             WHERE confrelid = 'user_country'::regclass;
#         """)

#         constraints_info = cursor.fetchall()
#         print(f"\n\n\n\n\n\n----constraints_info----{constraints_info}")

#         # Drop constraints for the specified table
#         for constraint_name, table in constraints_info:
#             print(f"\n\n\n\n\n\n----{constraint_name}----{table}")
#             cursor.execute(f'ALTER TABLE {table} DROP CONSTRAINT {constraint_name};')
#         conn.commit()

#         # Return the constraints information
#         return constraints_info
#     except psycopg2.DatabaseError as e:
#         print(f"Error {e}")
#         sys.exit(1)

#     finally:
#         if conn:
#             conn.close()
#         if cursor:
#             cursor.close()

# def create_related_constraints(constraints_info):
#     try:
#         conn = psycopg2.connect(DATABASE_URL)
#         conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
#         cursor = conn.cursor()

#         for constraint_name, table_name in constraints_info:
#             field = "company_id_id"
#             if table_name == "user_country_payment_methods":
#                 field = "country_id"
#             update = f"ALTER TABLE {table_name} ADD CONSTRAINT {constraint_name} FOREIGN KEY ({field}) REFERENCES user_country (id);"
#             print(f"\n\n\n\n\n\n----{constraint_name}--222--{table_name}", update)
#             cursor.execute(update)

#         conn.commit()
#     except psycopg2.DatabaseError as e:
#         print(f"Error {e}")
#         sys.exit(1)

#     finally:
#         if conn:
#             conn.close()
#         if cursor:
#             cursor.close()
