#!/usr/bin/env bash
pip install -U "ansible<2.10"
set -e
if [ -z "$1" ]; then
    if [ -f ./hosts ]; then
        HOSTS=hosts
    else
        echo "usage: $0 <system>"
        exit
    fi
else
    HOSTS=$1
fi

KEY_FILE=$2
if [ -n "${KEY_FILE-}" ]; then
    KEY_FILE=$2
else
    echo 'no server key selected '
    exit 1
fi

RUNNER_ROOT=$3
if [ -n "${RUNNER_ROOT-}" ]; then
    RUNNER_ROOT=$3
fi

if [ -n "${ENV_FILE-}" ]; then
    echo 'env file loaded'
else
    echo 'no environment variable file path selected '
    exit 1
fi
echo $RUNNER_ROOT
ansible-playbook  site.yml -i $HOSTS --extra-vars="ansible_ssh_private_key_file=$KEY_FILE runner_root_dir=$RUNNER_ROOT" --verbose
