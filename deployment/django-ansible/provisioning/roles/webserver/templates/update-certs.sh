#!/usr/bin/env bash
mkdir -p /tmp/letsencrypt/
letsencrypt certonly --webroot --webroot-path /tmp/letsencrypt -d {{ domain }} {% if use_www %}-d www.{{ domain }}{% endif %}  -d admin.heorderapp.online --keep-until-expiring -n --expand
sudo systemctl reload nginx
