from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework.documentation import include_docs_urls
from rest_framework.renderers import JSONOpenAPIRenderer
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(title="HE API", default_version="v1"),
    public=True,
    permission_classes=[],
)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api-auth/", include("rest_framework.urls", namespace="rest")),
    path(
        "api/user/",
        include(("src.user.urls", "user_api"), namespace="user_api"),
        name="users",
    ),
    url(
        r"^schema(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    path("", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("docs", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
    path(
        "api/country/",
        include(("src.user.country", "countries"), namespace="country_api"),
        name="country",
    ),
    path(
        "api/telnyx/",
        include(("src.user.telnyx", "telnyx"), namespace="telnyx_api"),
        name="telnyx",
    ),
    path(
        "api/products/",
        include(("src.products.urls", "products"), namespace="products-api"),
        name="product",
    ),
    path(
        "api/orders/",
        include(("src.orders.urls", "orders"), namespace="orders-api"),
        name="order",
    ),
    path(
        "api/payments/",
        include(("src.payment.urls", "payment"), namespace="payment-api"),
        name="payment",
    ),
    path(
        "api/trainings/",
        include(("src.trainings.urls", "trainings"), namespace="trainings-api"),
        name="trainings",
    ),
    path(
        "api/clusters/",
        include(("src.clusters.urls", "clusters"), namespace="clusters-api"),
        name="clusters",
    ),
    path(
        "api/districts/",
        include(("src.districts.urls", "districts"), namespace="districts-api"),
        name="districts",
    ),
    path(
        "api/promotions/",
        include(("src.promotions.urls", "promotions"), namespace="promotions-api"),
        name="promotions",
    ),
    path(
        "api/accounting/",
        include(("src.accounting.urls", "accounting"), namespace="invoices-api"),
        name="accounting",
    ),
    re_path(r"api/celery-progress/", include("celery_progress.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("debug/", include(debug_toolbar.urls)),
    ] + urlpatterns
