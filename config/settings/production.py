from .common import *

import dj_database_url
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

DEBUG = False

DATABASES["default"] = dj_database_url.config(
    default=os.environ.get("DATABASE_URL", None)
)

CSRF_COOKIE_DOMAIN = "heorderapp.online"

csrf_host_list = os.environ.get("ALLOWED_HOSTS")
CSRF_TRUSTED_ORIGINS = csrf_host_list.split(",") if csrf_host_list else []

sentry_sdk.init(
    dsn="https://8207a792f06940e598457d23d7228161@o472953.ingest.sentry.io/5507280",
    integrations=[DjangoIntegration()],
    traces_sample_rate=0.5,
    send_default_pii=True,
    environment="production",
)
