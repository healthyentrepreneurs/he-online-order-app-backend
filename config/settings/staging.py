from .common import *
import dj_database_url
import django_heroku
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

DEBUG = os.environ.get("DEBUG", False)

django_heroku.settings(locals(), test_runner=False)
DATABASES["default"] = dj_database_url.config(
    default=os.environ.get("DATABASE_URL", None)
)

csrf_host_list = os.environ.get("CSRF_TRUSTED_ORIGINS")
CSRF_TRUSTED_ORIGINS = csrf_host_list.split(",") if csrf_host_list else []

CORS_ALLOW_ALL_ORIGINS = os.environ.get("CORS_ALLOW_ALL_ORIGINS", True)


sentry_sdk.init(
    dsn="https://8207a792f06940e598457d23d7228161@o472953.ingest.sentry.io/5507280",
    integrations=[DjangoIntegration()],
    traces_sample_rate=0.5,
    send_default_pii=True,
    environment="staging",
)
