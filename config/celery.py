from __future__ import absolute_import, unicode_literals
import os

from celery import Celery


# Set the default Django settings module for the 'celery' program.
# "sample_app" is name of the root app
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.common")
url = os.environ.get("REDISCLOUD_URL")
print("\n\n\n\n redis celery before  connecting---->", url)
if url:
    url = f"{url}/0"
    print("\n\n\n\n redis celery connecting---->", url)
    app = Celery("celery_app", broker=url, backend=url)
else:
    print("\n\n\n\n redis celery connecting-locally or prod--->", url)
    app = Celery(
        "celery_app",
        broker="redis://localhost:6379/0",
        backend="redis://localhost:6379/0",
    )

# Load task modules from all registered Django apps.
app.autodiscover_tasks()
